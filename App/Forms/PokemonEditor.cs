﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Utilities;
using Utilities.Structures;

namespace App
{
    public partial class PokemonEditor : Form
    {
        private List<MoveField> MovesList = new List<MoveField>();
        private List<ushort> RegionalNumbers = new List<ushort>();
        private List<string> EggMovesList = new List<string>();
        private string WildItemCommonValue = "";
        private string WildItemUncommonValue = "";
        private string WildItemRareValue = "";
        private string IncenseValue = "";
        private List<Evolutions> Evolutions = new List<Evolutions>();
        private string[] FormNames = new string[0];
        private Bitmap IconBitmap;
        private bool IconChanged = false;

        public PokemonEditor()
        {
            InitializeComponent();
        }

        private void PokemonEditor_Load(object sender, EventArgs e)
        {
            OpenIconFileDialog.InitialDirectory = Global.Directory;
            SaveIconFileDialog.InitialDirectory = Global.Directory;
            PokemonList.DataSource = Global.Pokemon;
            Type1Selector.Items.AddRange(Global.Types.GetAll());
            Type2Selector.Items.AddRange(Global.Types.GetAll());
            MainAbilityInput.Items.AddRange(Helpers.GetAllAbilityConstantNames().ToArray());
            SecondaryAbilityInput.Items.AddRange(Helpers.GetAllAbilityConstantNames().ToArray());
            HiddenAbilityInput.Items.AddRange(Helpers.GetAllAbilityConstantNames().ToArray());
            var GenderRates = Enum.GetValues(typeof(GenderRate)).Cast<GenderRate>().ToArray();
            var Colors = Enum.GetValues(typeof(Utilities.Structures.Color)).Cast<Utilities.Structures.Color>().ToArray();
            var Habitats = Enum.GetValues(typeof(Habitat)).Cast<Habitat>().ToArray();
            var EggGroupsv16 = Enum.GetValues(typeof(EggGroupsv16)).Cast<EggGroupsv16>().ToArray();
            var EggGroupsv15 = Enum.GetValues(typeof(EggGroupsv15)).Cast<EggGroupsv15>().ToArray();
            var GrowthRates = Enum.GetValues(typeof(GrowthRate)).Cast<GrowthRate>().ToArray();
            GenderRateInput.Items.AddRange((from gr in GenderRates select gr.ToString()).ToArray<string>());
            ColorInput.Items.AddRange((from cl in Colors select cl.ToString()).ToArray<string>());
            HabitatSelector.Items.AddRange((from h in Habitats select h.ToString()).ToArray<string>());
            GrowthRateInput.Items.AddRange((from gr in GrowthRates select gr.ToString()).ToArray<string>());
            if (Global.v16Detected)
            {
                CompatibilitySelector.Items.AddRange((from c in EggGroupsv16 select c.ToString()).ToArray<string>());
            }
            else
            {
                CompatibilitySelector.Items.AddRange((from c in EggGroupsv15 select c.ToString()).ToArray<string>());
            }
            GrowthRateInput.BackColor = System.Drawing.Color.White;
            EffortPointsInput.BackColor = System.Drawing.Color.White;
            MainAbilityInput.BackColor = System.Drawing.Color.White;
            GenderRateInput.BackColor = System.Drawing.Color.White;
            UpdateFields();
        }

        #region "Form Controls"
        private void SecondaryType_CheckedChanged(object sender, EventArgs e)
        {
            Type2Selector.Enabled = SecondaryType.Checked;
        }

        private void SecondaryAbility_CheckedChanged(object sender, EventArgs e)
        {
            SecondaryAbilityInput.Enabled = SecondaryAbility.Checked;
        }

        private void HiddenAbility_CheckedChanged(object sender, EventArgs e)
        {
            HiddenAbilityInput.Enabled = HiddenAbility.Checked;
        }

        private void PokemonList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(PokemonList.SelectedItem == null)
            {
                return;
            }
            this.IconChanged = false;
            UpdateFields();
        }

        private void AddItemButton_Click(object sender, EventArgs e)
        {
            if (PokemonList.Items.Count == 65525)
            {
                MessageBox.Show("You cannot add more than 65,525 slots!");
                return;
            }
            Global.Pokemon.Insert(PokemonList.SelectedIndex, new Pokemon(true));
            UpdateFields();
        }

        private void DeleteItem_Click(object sender, EventArgs e)
        {
            if(PokemonList.SelectedItem == null || PokemonList.Items.Count == 0)
            {
                return;
            }
            if (MessageBox.Show(
    "Do you want to delete this slot?", "Confirmation",
    MessageBoxButtons.YesNo, MessageBoxIcon.Question,
    MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
            {
                Global.Pokemon.Remove((Pokemon)PokemonList.SelectedItem);
                UpdateFields();
            }
        }

        private void MoveUpButton_Click(object sender, EventArgs e)
        {
            if(PokemonList.SelectedIndex == 0)
            {
                return;
            }
            int index = PokemonList.SelectedIndex;
            Pokemon pkmn_curr = (Pokemon)PokemonList.SelectedItem;
            Pokemon pkmn_prev = (Pokemon)PokemonList.Items[index - 1];
            Global.Pokemon[index - 1] = pkmn_curr;
            Global.Pokemon[index] = pkmn_prev;

            Global.Pokemon[index - 1] = pkmn_curr;
            Global.Pokemon[index] = pkmn_prev;
            PokemonList.Refresh();
            --PokemonList.SelectedIndex;
        }

        private void MoveDownButton_Click(object sender, EventArgs e)
        {
            if(PokemonList.SelectedIndex == Global.Pokemon.Count)
            {
                return;
            }
            int index = PokemonList.SelectedIndex;
            Pokemon pkmn_curr = (Pokemon)PokemonList.SelectedItem;
            Pokemon pkmn_next = (Pokemon)PokemonList.Items[index + 1];
            Global.Pokemon[index] = pkmn_next;
            Global.Pokemon[index + 1] = pkmn_curr;
            PokemonList.Refresh();
            ++PokemonList.SelectedIndex;
        }

        private void GenderRateInput_TextChanged(object sender, EventArgs e)
        {
            if (!GenderRateInput.AutoCompleteCustomSource.Contains(GenderRateInput.Text))
            {
                GenderRateInput.BackColor = System.Drawing.Color.Red;
            }
            else
            {
                GenderRateInput.BackColor = System.Drawing.Color.White;
            }
        }

        private void GrowthRateInput_TextChanged(object sender, EventArgs e)
        {
            if (!GrowthRateInput.AutoCompleteCustomSource.Contains(GrowthRateInput.Text))
            {
                GrowthRateInput.BackColor = System.Drawing.Color.Red;
            }
            else
            {
                GrowthRateInput.BackColor = System.Drawing.Color.White;
            }
        }

        private void MainAbilityInput_TextChanged(object sender, EventArgs e)
        {
            if (!MainAbilityInput.AutoCompleteCustomSource.Contains(MainAbilityInput.Text))
            {
                MainAbilityInput.BackColor = System.Drawing.Color.Red;
            }
            else
            {
                MainAbilityInput.BackColor = System.Drawing.Color.White;
            }
        }

        private void PokemonList_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = String.Format("{0:D3}",PokemonList.Items.IndexOf((Pokemon)e.ListItem) + 1) + " - " + e.Value;
        }

        private void IconPicture_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            OpenIconFileDialog.FileName = "";
            if (OpenIconFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.IconChanged = true;
                DrawIcon(OpenIconFileDialog.FileName);
            }
            else
            {
                DrawIcon();
            }
            Cursor.Current = Cursors.Default;
        }

        private void DrawIcon(string path = null)
        {
            try
            {
                Bitmap bmp;
                if(path != null)
                {
                    bmp = new Bitmap(path);
                }
                else
                {
                    bmp = new Bitmap(Global.Directory + "/Graphics/Icons/icon" + String.Format("{0:D3}", PokemonList.SelectedIndex + 1) + ".png");
                }
                this.IconBitmap = bmp;
                int width = bmp.Width/2;
                int height = bmp.Height;
                Bitmap bmpImage = new Bitmap(bmp);
                Bitmap bmpCrop = bmpImage.Clone(new Rectangle(0, 0, width, height),
                bmpImage.PixelFormat);
                Bitmap bmpFinal = new Bitmap(bmpCrop, new Size(width / 2, height / 2));
                IconPicture.BackgroundImage = (Image)bmpFinal;
            }
            catch
            {
                IconPicture.BackgroundImage = null;
            }
        }

        private void ManageMoves_Click(object sender, EventArgs e)
        {
            ManageMoves moves_dialog = new ManageMoves(this.MovesList);
            moves_dialog.ShowDialog();
            if(moves_dialog.Changed)
            {
                this.MovesList.Clear();
                this.MovesList.AddRange(moves_dialog.MovesList);
            }
        }

        private void ManageEggMoves_Click(object sender, EventArgs e)
        {
            ManageEggMoves eggmoves_dialog = new ManageEggMoves(this.EggMovesList);
            eggmoves_dialog.ShowDialog();
            if (eggmoves_dialog.Changed)
            {
                this.EggMovesList.Clear();
                this.EggMovesList.AddRange(eggmoves_dialog.EggMovesList);
            }
        }

        private void ManageItems_Click(object sender, EventArgs e)
        {
            ManageItems items_dialog = new ManageItems(WildItemCommonValue, WildItemUncommonValue, WildItemRareValue, IncenseValue);
            items_dialog.ShowDialog();
            if (items_dialog.Changed)
            {
                this.WildItemCommonValue = items_dialog.CommonItemInput.Text;
                this.WildItemUncommonValue = items_dialog.UncommonItemInput.Text;
                this.WildItemRareValue = items_dialog.RareItemInput.Text;
                this.IncenseValue = items_dialog.IncenseInput.Text;
            }
        }

        #endregion

        private void UpdateFields()
        {
            Pokemon pkmn = (Pokemon)PokemonList.SelectedItem;
            CompatibilitySelector.SelectedItems.Clear();
            HabitatSelector.SelectedItem = Habitat.Unknown;
            this.MovesList = pkmn.Moves;
            this.RegionalNumbers = pkmn.RegionalNumbers;
            this.EggMovesList = pkmn.EggMoves;
            this.WildItemCommonValue = pkmn.WildItemCommon;
            this.WildItemUncommonValue = pkmn.WildItemUncommon;
            this.WildItemRareValue = pkmn.WildItemRare;
            this.IncenseValue = pkmn.Incense;
            this.Evolutions = pkmn.Evolutions;
            this.FormNames = pkmn.FormNames;

            NameInput.Text = pkmn.Name;
            InternalNameInput.Text = pkmn.InternalName;
            eYPosInput.Value = pkmn.BattlerEnemyY;
            YPosInput.Value = pkmn.BattlerPlayerY;
            APosInput.Value = pkmn.BattlerAltitude;

            HeightInput.Value = pkmn.Height;
            WeightInput.Value = pkmn.Weight;
            GenderRateInput.Text = pkmn.GenderRate.ToString();
            ColorInput.Text = pkmn.Color.ToString();
            HabitatSelector.SelectedItem = pkmn.Habitat.ToString();
            if (Global.v16Detected)
            {
                foreach (EggGroupsv16 eg in pkmn.Compatibilityv16)
                {
                    CompatibilitySelector.SelectedItems.Add(eg.ToString());
                }
            }
            else
            {
                foreach (EggGroupsv15 eg in pkmn.Compatibilityv15)
                {
                    CompatibilitySelector.SelectedItems.Add(eg.ToString());
                }
            }

            EffortPointsInput.Text = String.Join("", pkmn.EffortPoints);

            List<string> bs = new List<string>();
            foreach (int stat in pkmn.BaseStats)
            {
                bs.Add(String.Format("{0:D3}", stat));
            }

            BaseStatsInput.Text = String.Join("", bs.ToArray());
            BaseExpInput.Value = pkmn.BaseExp;
            GrowthRateInput.Text = pkmn.GrowthRate.ToString();
            RarenessInput.Value = pkmn.Rareness;
            StepsToHatchInput.Value = pkmn.StepsToHatch;
            HappinessInput.Value = pkmn.Happiness;
            KindInput.Text = pkmn.Kind;

            #region "types"
            Type1Selector.SelectedItem = pkmn.Type1;
            if (pkmn.Type2 != null)
            {
                SecondaryType.Checked = true;
                Type2Selector.SelectedItem = pkmn.Type2;
            }
            else
            {
                SecondaryType.Checked = false;
                Type2Selector.SelectedItem = null;
            }
            #endregion

            #region "abilities"
            MainAbilityInput.Text = pkmn.Abilities[0];
            if (pkmn.Abilities.Length == 2)
            {
                SecondaryAbility.Checked = true;
                SecondaryAbilityInput.Text = pkmn.Abilities[1];
            }
            else
            {
                SecondaryAbility.Checked = false;
                SecondaryAbilityInput.Text = "";
            }
            if (pkmn.HiddenAbility != null)
            {
                HiddenAbility.Checked = true;
                HiddenAbilityInput.Text = pkmn.HiddenAbility;
            }
            else
            {
                HiddenAbility.Checked = false;
                HiddenAbilityInput.Text = "";
            }
            #endregion

            PokedexEntryInput.Text = pkmn.Pokedex;
            PokemonList.Update();
            DrawIcon();

        }

        private void SaveItemButton_Click(object sender, EventArgs e)
        {
            Pokemon pkmn = GeneratePokemon();
            SaveIconFileDialog.FileName = "";
            if (this.IconChanged)
            {
                if (SaveIconFileDialog.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(SaveIconFileDialog.FileName))
                    {
                        System.GC.Collect();
                        System.GC.WaitForPendingFinalizers();
                        File.Delete(SaveIconFileDialog.FileName);
                    }
                    int width = Convert.ToInt32(IconBitmap.Width);
                    int height = Convert.ToInt32(IconBitmap.Height);
                    Bitmap bmp = new Bitmap(width, height);
                    bmp = IconBitmap.Clone(new Rectangle(0, 0, width, height), bmp.PixelFormat);
                    bmp.Save(SaveIconFileDialog.FileName, ImageFormat.Png);
                    this.IconChanged = false;
                }
            }
            MessageBox.Show(String.Format("{0}'s data has been updated.", pkmn.Name));
            for (int i = 0; i < Global.Pokemon.Count; i++)
            {
                if (i == PokemonList.SelectedIndex)
                {
                    Global.Pokemon[i] = pkmn;
                    break;
                }
            }
            PokemonList.Update();
        }

        private void ManageEvolutions_Click(object sender, EventArgs e)
        {
            ManageEvolutions evo_dialog = new ManageEvolutions(this.Evolutions);
            evo_dialog.ShowDialog();
            if (evo_dialog.Changed)
            {
                this.Evolutions.Clear();
                this.Evolutions.AddRange(evo_dialog.EvoList);
            }
        }

        private void PreviewButton_Click(object sender, EventArgs e)
        {
            Pokemon pkmn = GeneratePokemon();
            MessageBox.Show(pkmn.ToString());
        }

        private Pokemon GeneratePokemon()
        {
            Pokemon pkmn = (Pokemon)PokemonList.SelectedItem;
            pkmn.Name = NameInput.Text;
            pkmn.InternalName = InternalNameInput.Text;
            pkmn.Pokedex = PokedexEntryInput.Text;
            pkmn.BattlerEnemyY = (byte)eYPosInput.Value;
            pkmn.BattlerPlayerY = (byte)YPosInput.Value;
            pkmn.BattlerAltitude = (byte)APosInput.Value;
            GenderRate gr;
            Enum.TryParse(GenderRateInput.Text, true, out gr);
            pkmn.GenderRate = gr;
            pkmn.Height = HeightInput.Value;
            pkmn.Weight = WeightInput.Value;
            Utilities.Structures.Color cl;
            Enum.TryParse(ColorInput.Text, true, out cl);
            pkmn.Color = cl;
            Habitat hb;
            Enum.TryParse(HabitatSelector.SelectedItem.ToString(), true, out hb);
            pkmn.Habitat = hb;
            if (Global.v16Detected)
            {
                List<EggGroupsv16> egs = new List<EggGroupsv16>();
                foreach (object eg in CompatibilitySelector.SelectedItems)
                {
                    EggGroupsv16 es;
                    Enum.TryParse(eg.ToString(), true, out es);
                    egs.Add(es);
                }
                pkmn.Compatibilityv16 = egs;
            }
            else
            {
                List<EggGroupsv15> egs = new List<EggGroupsv15>();
                foreach (object eg in CompatibilitySelector.SelectedItems)
                {
                    EggGroupsv15 es;
                    Enum.TryParse(eg.ToString(), true, out es);
                    egs.Add(es);
                }
                pkmn.Compatibilityv15 = egs;
            }
            pkmn.Type1 = Type1Selector.Text;
            if (SecondaryType.Checked)
            {
                pkmn.Type2 = Type2Selector.Text;
            }
            string[] base_stats = Regex.Split(BaseStatsInput.Text, "[\\,\\.]");
            List<byte> bstats = new List<byte>();
            foreach (string stat in base_stats)
            {
                bstats.Add((byte)Convert.ToInt32(stat));
            }
            pkmn.BaseStats = bstats.ToArray();
            GrowthRate growth_rate;
            Enum.TryParse(GrowthRateInput.Text, true, out growth_rate);
            pkmn.GrowthRate = growth_rate;
            pkmn.BaseExp = (ushort)Convert.ToInt32(BaseExpInput.Value);

            string[] effort_points = Regex.Split(EffortPointsInput.Text, "[\\,\\.]");
            List<byte> e_points = new List<byte>();
            foreach (string stat in effort_points)
            {
                e_points.Add((byte)Convert.ToInt32(stat));
            }
            pkmn.EffortPoints = e_points.ToArray();
            pkmn.Rareness = (byte)RarenessInput.Value;
            pkmn.Happiness = (byte)HappinessInput.Value;
            pkmn.Moves = this.MovesList;
            pkmn.EggMoves = this.EggMovesList;
            pkmn.StepsToHatch = (ushort)StepsToHatchInput.Value;
            pkmn.Kind = KindInput.Text;
            pkmn.Evolutions = this.Evolutions;
            pkmn.Abilities[0] = MainAbilityInput.Text;
            if (SecondaryAbility.Checked)
            {
                pkmn.Abilities[1] = SecondaryAbilityInput.Text;
            }
            if (HiddenAbility.Checked)
            {
                pkmn.HiddenAbility = HiddenAbilityInput.Text;
            }
            pkmn.RegionalNumbers = this.RegionalNumbers;
            pkmn.FormNames = this.FormNames;
            pkmn.WildItemCommon = this.WildItemCommonValue;
            pkmn.WildItemUncommon = this.WildItemUncommonValue;
            pkmn.WildItemRare = this.WildItemRareValue;
            pkmn.Incense = this.IncenseValue;
            return pkmn;
        }

        private void RegionalNumbersButton_Click(object sender, EventArgs e)
        {
            ManageRegionalNumbers regional_dialog = new ManageRegionalNumbers(this.RegionalNumbers);
            regional_dialog.ShowDialog();
            if (regional_dialog.Changed)
            {
                this.RegionalNumbers.Clear();
                this.RegionalNumbers.AddRange(regional_dialog.RegionalNumbersList);
            }
        }

        private void FormNamesButton_Click(object sender, EventArgs e)
        {
            ManageFormNames forms_dialog = new ManageFormNames(this.FormNames);
            forms_dialog.ShowDialog();
            if (forms_dialog.Changed)
            {
                this.FormNames = forms_dialog.FormNamesList;
            }
        }

        private void PokemonEditor_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            System.Diagnostics.Process.Start("http://pokemonessentials.wikia.com/wiki/Defining_a_species");
        }
    }
}
