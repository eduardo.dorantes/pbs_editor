﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Utilities;
using Utilities.Structures;

namespace App.Forms
{
    public partial class AbilitiesEditor: Form
    {
        public AbilitiesEditor()
        {
            InitializeComponent();
        }

        private void AbilitiesEditor_Load(object sender, EventArgs e)
        {
            AbilitiesList.DataSource = Global.Abilities;
        }

        private void AbilitiesList_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = String.Format("{0:D3}", (AbilitiesList.Items.IndexOf(e.ListItem) + 1)) + " - " + e.Value;
        }

        private void AbilitiesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AbilitiesList.SelectedItem == null)
            {
                return;
            }
            UpdateFields();
        }

        private void UpdateFields()
        {
            Ability ability = (Ability)AbilitiesList.SelectedItem;
            ability.Description = Regex.Replace(ability.Description, "[\\\\]+\'", "'");
            ability.Description = Regex.Replace(ability.Description, "[\\\\]+\"", "\"");
            InternalNameInput.Text = ability.InternalName;
            DisplayNameInput.Text = ability.Name;
            DescriptionInput.Text = ability.Description;
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            if(AbilitiesList.Items.Count == 65525)
            {
                MessageBox.Show("You cannot add more than 65,525 slots!");
                return;
            }
            Global.Abilities.Insert(AbilitiesList.SelectedIndex, new Ability());
            UpdateFields();
        }

        private void RemoveButton_Click(object sender, EventArgs e)
        {
            if (AbilitiesList.SelectedItem == null || AbilitiesList.Items.Count == 0)
            {
                return;
            }
            if (MessageBox.Show(
                "Do you want to delete this slot?", "Confirmation",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
            {
                Global.Abilities.Remove((Ability)AbilitiesList.SelectedItem);
                UpdateFields();
            }
        }

        private void MoveUpButton_Click(object sender, EventArgs e)
        {
            if (AbilitiesList.SelectedIndex == 0)
            {
                return;
            }
            int index = AbilitiesList.SelectedIndex;
            Ability item_curr = (Ability)AbilitiesList.SelectedItem;
            Ability item_prev = (Ability)AbilitiesList.Items[index - 1];
            Global.Abilities[index - 1] = item_curr;
            Global.Abilities[index] = item_prev;
            Global.Abilities[index - 1] = item_curr;
            Global.Abilities[index] = item_prev;
            AbilitiesList.Refresh();
            --AbilitiesList.SelectedIndex;
        }

        private void MoveDownButton_Click(object sender, EventArgs e)
        {
            if (AbilitiesList.SelectedIndex == Global.Abilities.Count)
            {
                return;
            }
            int index = AbilitiesList.SelectedIndex;
            Ability item_curr = (Ability)AbilitiesList.SelectedItem;
            Ability item_next = (Ability)AbilitiesList.Items[index + 1];
            Global.Abilities[index] = item_next;
            Global.Abilities[index + 1] = item_curr;
            AbilitiesList.Refresh();
            ++AbilitiesList.SelectedIndex;
        }

        private void PreviewButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show((AbilitiesList.SelectedIndex + 1) + "," + GenerateAbility().ToString());
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            Ability ability = GenerateAbility();
            for(int i = 0; i < Global.Abilities.Count; i++)
            {
                if(i == AbilitiesList.SelectedIndex)
                {
                    Global.Abilities[i] = ability;
                }
            }
            MessageBox.Show(String.Format("{0}'s data has been updated.", ability.Name));
            AbilitiesList.Refresh();
        }

        private Ability GenerateAbility()
        {
            Ability ability = (Ability)AbilitiesList.SelectedItem;
            ability.InternalName = InternalNameInput.Text;
            ability.Name = DisplayNameInput.Text;
            ability.Description = DescriptionInput.Text;
            ability.Description = Regex.Replace(ability.Description, "'", "\'");
            ability.Description = Regex.Replace(ability.Description, "\"", "\\\"");
            return ability;
        }
    }
}
