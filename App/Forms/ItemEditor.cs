﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;
using Utilities;
using Utilities.Structures;

namespace App
{
    public partial class ItemEditor : Form
    {
        public Bitmap IconBitmap { get; private set; }
        public bool IconChanged { get; private set; }
        private int extra_pockets = 0;

        public ItemEditor()
        {
            InitializeComponent();
        }

        private void ItemEditor_Load(object sender, EventArgs e)
        {
            OpenIconDialog.InitialDirectory = Global.Directory;
            SaveIconDialog.InitialDirectory = Global.Directory;
            ItemsListBox.DataSource = Global.Items;
            cbxMove.Items.AddRange(Helpers.GetAllMoveNames().ToArray());
            txtPluralName.Enabled = Global.v16Detected;
            lblPluralName.Enabled = Global.v16Detected;
        }

        private void ItemsListBox_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = String.Format("{0:D3}", ItemsListBox.Items.IndexOf((Item)e.ListItem) + 1) + " - " + e.Value;
        }

        private void UpdateFields()
        {
            Item item = (Item)ItemsListBox.SelectedItem;
            txtInternalName.Text = item.InternalName;
            txtDisplayName.Text = item.Name;
            txtPluralName.Text = item.PluralName;
            txtDescription.Text = item.Description;
            lstInBattle.SelectedIndex = item.UsabilityInBattle % lstInBattle.Items.Count;
            lstOutsideBattle.SelectedIndex = item.UsabilityOutOfBattle % lstOutsideBattle.Items.Count;
            bool needExtra = (item.Pocket) >= lstPocket.Items.Count;
            if (needExtra)
            {
                int extra = (item.Pocket) - lstPocket.Items.Count;
                for (int i = 0; i < extra; i++)
                {
                    lstPocket.Items.Add("Custom " + ++this.extra_pockets);
                }
            }
            lstPocket.SelectedIndex = item.Pocket - 1;
            lstSpecialItem.SelectedIndex = item.SpecialItems % lstSpecialItem.Items.Count;
            numPrice.Value = item.Price;
            if(item.Move != null)
            {
                cbxMove.Text = item.Move;
            }
            DrawIcon();
        }

        private void ItemsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ItemsListBox.SelectedItem == null)
            {
                return;
            }
            this.IconChanged = false;
            UpdateFields();
        }

        private void AddItemButton_Click(object sender, EventArgs e)
        {
            if(ItemsListBox.Items.Count == 65525)
            {
                MessageBox.Show("You cannot add more than 65,525 slots!");
                return;
            }
            Global.Items.Insert(ItemsListBox.SelectedIndex, new Item(true));
            UpdateFields();
        }

        private void DeleteItemButton_Click(object sender, EventArgs e)
        {
            if(ItemsListBox.SelectedItem == null || ItemsListBox.Items.Count == 0)
            {
                return;
            }
            if (MessageBox.Show(
                "Do you want to delete this slot?", "Confirmation",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
            {
                Global.Items.Remove((Item)ItemsListBox.SelectedItem);
                UpdateFields();
            }
        }

        private void MoveUpButton_Click(object sender, EventArgs e)
        {
            if(ItemsListBox.SelectedIndex == 0)
            {
                return;
            }
            int index = ItemsListBox.SelectedIndex;
            Item item_curr = (Item)ItemsListBox.SelectedItem;
            Item item_prev = (Item)ItemsListBox.Items[index - 1];
            Global.Items[index - 1] = item_curr;
            Global.Items[index] = item_prev;
            Global.Items[index - 1] = item_curr;
            Global.Items[index] = item_prev;
            ItemsListBox.Refresh();
            --ItemsListBox.SelectedIndex;
        }

        private void MoveDownButton_Click(object sender, EventArgs e)
        {
            if (ItemsListBox.SelectedIndex == Global.Items.Count)
            {
                return;
            }
            int index = ItemsListBox.SelectedIndex;
            Item item_curr = (Item)ItemsListBox.SelectedItem;
            Item item_next = (Item)ItemsListBox.Items[index + 1];
            Global.Items[index] = item_next;
            Global.Items[index + 1] = item_curr;
            ItemsListBox.Refresh();
            ++ItemsListBox.SelectedIndex;
        }

        private void PreviewButton_Click(object sender, EventArgs e)
        {
            Item item = GenerateItem();
            MessageBox.Show(item.ToString());
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            Item item = GenerateItem();
            if (this.IconChanged)
            {
                if (SaveIconDialog.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(SaveIconDialog.FileName))
                    {
                        System.GC.Collect();
                        System.GC.WaitForPendingFinalizers();
                        File.Delete(SaveIconDialog.FileName);
                    }
                    int width = Convert.ToInt32(IconBitmap.Width);
                    int height = Convert.ToInt32(IconBitmap.Height);
                    Bitmap bmp = new Bitmap(width, height);
                    bmp = IconBitmap.Clone(new Rectangle(0, 0, width, height), bmp.PixelFormat);
                    bmp.Save(SaveIconDialog.FileName, ImageFormat.Png);
                    this.IconChanged = false;
                }
            }
            MessageBox.Show(String.Format("{0}'s data has been updated.", item.Name));
            for (int i = 0; i < Global.Items.Count; i++)
            {
                if(i == ItemsListBox.SelectedIndex)
                {
                    Global.Items[i] = item;
                    break;
                }
            }
            ItemsListBox.Update();
        }

        private Item GenerateItem()
        {
            Item item = (Item)ItemsListBox.SelectedItem;
            item.InternalName = txtInternalName.Text;
            item.Name = txtDisplayName.Text;
            item.PluralName = txtPluralName.Text;
            item.Pocket = (byte)(lstPocket.SelectedIndex + 1);
            item.Price = (int)numPrice.Value;
            if (cbxMove.Text != "" && item.Pocket == 4)
            {
                item.Move = cbxMove.Text;
            }
            item.SpecialItems = (byte)lstSpecialItem.SelectedIndex;
            item.UsabilityOutOfBattle = (byte)lstOutsideBattle.SelectedIndex;
            item.UsabilityInBattle = (byte)lstInBattle.SelectedIndex;
            item.Description = txtDescription.Text;
            return item;
        }

        private void lstPocket_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(lstPocket.SelectedIndex == 3)
            {
                cbxMove.Enabled = true;
            }
            else
            {
                cbxMove.Enabled = false;
            }
        }

        private void cbxMove_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstPocket.SelectedIndex == 3)
            {
                cbxMove.Enabled = true;
            }
            else
            {
                cbxMove.Enabled = false;
            }
        }

        private void DrawIcon(string cpath = null)
        {
            string icon = String.Format("{0:D3}", ItemsListBox.SelectedIndex + 1);
            string path = Global.Directory + "/Graphics/Icons/item" + icon + ".png";
            if(cpath != null)
            {
                path = cpath;
            }
            try
            {
                Bitmap bmp = new Bitmap(path);
                this.IconBitmap = bmp;
                int width = bmp.Width;
                int height = bmp.Height;
                if (height / width > 1)
                {
                    width = width / (height / width);
                }
                Bitmap bmpImage = new Bitmap(bmp);
                Bitmap bmpCrop = bmpImage.Clone(new Rectangle(0, 0, width, height),
                bmpImage.PixelFormat);
                Bitmap bmpFinal = new Bitmap(bmpCrop, new Size(width / 2, height / 2));
                IconBox.BackgroundImage = (Image)bmpFinal;
            }
            catch
            {
                IconBox.BackgroundImage = null;
            }
        }

        private void IconBox_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            string path = "";
            OpenIconDialog.FileName = path;
            if (OpenIconDialog.ShowDialog() == DialogResult.OK)
            {
                this.IconChanged = true;
                DrawIcon(OpenIconDialog.FileName);
            }
            else
            {
                DrawIcon();
            }
            Cursor.Current = Cursors.Default;
        }

        private void ItemEditor_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {
            System.Diagnostics.Process.Start("http://pokemonessentials.wikia.com/wiki/Defining_an_item");
        }
    }
}
