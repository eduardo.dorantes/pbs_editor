﻿namespace App.Forms
{
    partial class AbilitiesEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AbilitiesEditor));
            this.ListPanel = new System.Windows.Forms.Panel();
            this.ToolStrip = new System.Windows.Forms.ToolStrip();
            this.AddButton = new System.Windows.Forms.ToolStripButton();
            this.RemoveButton = new System.Windows.Forms.ToolStripButton();
            this.MoveUpButton = new System.Windows.Forms.ToolStripButton();
            this.MoveDownButton = new System.Windows.Forms.ToolStripButton();
            this.PreviewButton = new System.Windows.Forms.ToolStripButton();
            this.SaveButton = new System.Windows.Forms.ToolStripButton();
            this.AbilitiesList = new System.Windows.Forms.ListBox();
            this.InternalNameLabel = new System.Windows.Forms.Label();
            this.DisplayNameLabel = new System.Windows.Forms.Label();
            this.DescriptionLabel = new System.Windows.Forms.Label();
            this.InternalNameInput = new System.Windows.Forms.TextBox();
            this.DisplayNameInput = new System.Windows.Forms.TextBox();
            this.DescriptionInput = new System.Windows.Forms.TextBox();
            this.ListPanel.SuspendLayout();
            this.ToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // ListPanel
            // 
            this.ListPanel.Controls.Add(this.ToolStrip);
            this.ListPanel.Controls.Add(this.AbilitiesList);
            this.ListPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.ListPanel.Location = new System.Drawing.Point(0, 0);
            this.ListPanel.Name = "ListPanel";
            this.ListPanel.Size = new System.Drawing.Size(165, 263);
            this.ListPanel.TabIndex = 0;
            // 
            // ToolStrip
            // 
            this.ToolStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddButton,
            this.RemoveButton,
            this.MoveUpButton,
            this.MoveDownButton,
            this.PreviewButton,
            this.SaveButton});
            this.ToolStrip.Location = new System.Drawing.Point(0, 238);
            this.ToolStrip.Name = "ToolStrip";
            this.ToolStrip.Size = new System.Drawing.Size(165, 25);
            this.ToolStrip.TabIndex = 1;
            this.ToolStrip.Text = "toolStrip1";
            // 
            // AddButton
            // 
            this.AddButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.AddButton.Image = global::App.Properties.Resources.AddIcon;
            this.AddButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(23, 22);
            this.AddButton.Text = "Add Ability";
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // RemoveButton
            // 
            this.RemoveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.RemoveButton.Image = global::App.Properties.Resources.DeleteIcon;
            this.RemoveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RemoveButton.Name = "RemoveButton";
            this.RemoveButton.Size = new System.Drawing.Size(23, 22);
            this.RemoveButton.Text = "Remove Ability";
            this.RemoveButton.Click += new System.EventHandler(this.RemoveButton_Click);
            // 
            // MoveUpButton
            // 
            this.MoveUpButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.MoveUpButton.Image = global::App.Properties.Resources.UpIcon;
            this.MoveUpButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MoveUpButton.Name = "MoveUpButton";
            this.MoveUpButton.Size = new System.Drawing.Size(23, 22);
            this.MoveUpButton.Text = "Move Up";
            this.MoveUpButton.Click += new System.EventHandler(this.MoveUpButton_Click);
            // 
            // MoveDownButton
            // 
            this.MoveDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.MoveDownButton.Image = global::App.Properties.Resources.DownIcon;
            this.MoveDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MoveDownButton.Name = "MoveDownButton";
            this.MoveDownButton.Size = new System.Drawing.Size(23, 22);
            this.MoveDownButton.Text = "Move Down";
            this.MoveDownButton.Click += new System.EventHandler(this.MoveDownButton_Click);
            // 
            // PreviewButton
            // 
            this.PreviewButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PreviewButton.Image = global::App.Properties.Resources.PreviewIcon;
            this.PreviewButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PreviewButton.Name = "PreviewButton";
            this.PreviewButton.Size = new System.Drawing.Size(23, 22);
            this.PreviewButton.Text = "Preview";
            this.PreviewButton.Click += new System.EventHandler(this.PreviewButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.SaveButton.Image = global::App.Properties.Resources.OKIcon;
            this.SaveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(23, 22);
            this.SaveButton.Text = "Save Ability";
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // AbilitiesList
            // 
            this.AbilitiesList.DisplayMember = "Name";
            this.AbilitiesList.Dock = System.Windows.Forms.DockStyle.Top;
            this.AbilitiesList.FormattingEnabled = true;
            this.AbilitiesList.Location = new System.Drawing.Point(0, 0);
            this.AbilitiesList.Name = "AbilitiesList";
            this.AbilitiesList.Size = new System.Drawing.Size(165, 238);
            this.AbilitiesList.TabIndex = 0;
            this.AbilitiesList.SelectedIndexChanged += new System.EventHandler(this.AbilitiesList_SelectedIndexChanged);
            this.AbilitiesList.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.AbilitiesList_Format);
            // 
            // InternalNameLabel
            // 
            this.InternalNameLabel.AutoSize = true;
            this.InternalNameLabel.Location = new System.Drawing.Point(171, 15);
            this.InternalNameLabel.Name = "InternalNameLabel";
            this.InternalNameLabel.Size = new System.Drawing.Size(76, 13);
            this.InternalNameLabel.TabIndex = 1;
            this.InternalNameLabel.Text = "Internal Name:";
            // 
            // DisplayNameLabel
            // 
            this.DisplayNameLabel.AutoSize = true;
            this.DisplayNameLabel.Location = new System.Drawing.Point(171, 64);
            this.DisplayNameLabel.Name = "DisplayNameLabel";
            this.DisplayNameLabel.Size = new System.Drawing.Size(75, 13);
            this.DisplayNameLabel.TabIndex = 2;
            this.DisplayNameLabel.Text = "Display Name:";
            // 
            // DescriptionLabel
            // 
            this.DescriptionLabel.AutoSize = true;
            this.DescriptionLabel.Location = new System.Drawing.Point(172, 112);
            this.DescriptionLabel.Name = "DescriptionLabel";
            this.DescriptionLabel.Size = new System.Drawing.Size(63, 13);
            this.DescriptionLabel.TabIndex = 3;
            this.DescriptionLabel.Text = "Description:";
            // 
            // InternalNameInput
            // 
            this.InternalNameInput.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.InternalNameInput.Location = new System.Drawing.Point(187, 31);
            this.InternalNameInput.Name = "InternalNameInput";
            this.InternalNameInput.Size = new System.Drawing.Size(144, 20);
            this.InternalNameInput.TabIndex = 4;
            // 
            // DisplayNameInput
            // 
            this.DisplayNameInput.Location = new System.Drawing.Point(187, 80);
            this.DisplayNameInput.Name = "DisplayNameInput";
            this.DisplayNameInput.Size = new System.Drawing.Size(144, 20);
            this.DisplayNameInput.TabIndex = 5;
            // 
            // DescriptionInput
            // 
            this.DescriptionInput.Location = new System.Drawing.Point(187, 128);
            this.DescriptionInput.Multiline = true;
            this.DescriptionInput.Name = "DescriptionInput";
            this.DescriptionInput.Size = new System.Drawing.Size(213, 121);
            this.DescriptionInput.TabIndex = 6;
            // 
            // AbilitiesEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 263);
            this.Controls.Add(this.DescriptionInput);
            this.Controls.Add(this.DisplayNameInput);
            this.Controls.Add(this.InternalNameInput);
            this.Controls.Add(this.DescriptionLabel);
            this.Controls.Add(this.DisplayNameLabel);
            this.Controls.Add(this.InternalNameLabel);
            this.Controls.Add(this.ListPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AbilitiesEditor";
            this.ShowInTaskbar = false;
            this.Text = "Abilities Editor";
            this.Load += new System.EventHandler(this.AbilitiesEditor_Load);
            this.ListPanel.ResumeLayout(false);
            this.ListPanel.PerformLayout();
            this.ToolStrip.ResumeLayout(false);
            this.ToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel ListPanel;
        private System.Windows.Forms.ToolStrip ToolStrip;
        private System.Windows.Forms.ListBox AbilitiesList;
        private System.Windows.Forms.Label InternalNameLabel;
        private System.Windows.Forms.Label DisplayNameLabel;
        private System.Windows.Forms.Label DescriptionLabel;
        private System.Windows.Forms.TextBox InternalNameInput;
        private System.Windows.Forms.TextBox DisplayNameInput;
        private System.Windows.Forms.TextBox DescriptionInput;
        private System.Windows.Forms.ToolStripButton AddButton;
        private System.Windows.Forms.ToolStripButton RemoveButton;
        private System.Windows.Forms.ToolStripButton MoveUpButton;
        private System.Windows.Forms.ToolStripButton MoveDownButton;
        private System.Windows.Forms.ToolStripButton PreviewButton;
        private System.Windows.Forms.ToolStripButton SaveButton;
    }
}