﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace App
{
    public partial class ManageItems : Form
    {
        public bool Changed = false;
        private string WildItemCommon;
        private string WildItemUncommon;
        private string WildItemRare;
        private string Incense;

        public ManageItems(string c, string u, string r, string i)
        {
            InitializeComponent();
            WildItemCommon = c;
            WildItemUncommon = u;
            WildItemRare = r;
            Incense = i;
        }

        private void ManageItems_Load(object sender, EventArgs e)
        {
            List<string> Items = Utilities.Helpers.GetAllItemConstantNames();
            CommonItemInput.Items.AddRange(Items.ToArray());
            UncommonItemInput.Items.AddRange(Items.ToArray());
            RareItemInput.Items.AddRange(Items.ToArray());
            IncenseInput.Items.AddRange(Items.ToArray());
            CommonItemInput.Text = WildItemCommon;
            UncommonItemInput.Text = WildItemUncommon;
            RareItemInput.Text = WildItemRare;
            IncenseInput.Text = Incense;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            Changed = true;
            Close();
        }
    }
}
