﻿namespace App
{
    partial class ManageRegionalNumbers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManageRegionalNumbers));
            this.Panel = new System.Windows.Forms.Panel();
            this.RegionalNumbersData = new System.Windows.Forms.DataGridView();
            this.Numbers = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaveButton = new System.Windows.Forms.Button();
            this.Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RegionalNumbersData)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel
            // 
            this.Panel.BackColor = System.Drawing.Color.White;
            this.Panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel.Controls.Add(this.RegionalNumbersData);
            this.Panel.Location = new System.Drawing.Point(12, 12);
            this.Panel.Name = "Panel";
            this.Panel.Size = new System.Drawing.Size(164, 215);
            this.Panel.TabIndex = 0;
            // 
            // RegionalNumbersData
            // 
            this.RegionalNumbersData.BackgroundColor = System.Drawing.Color.White;
            this.RegionalNumbersData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RegionalNumbersData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.RegionalNumbersData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Numbers});
            this.RegionalNumbersData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RegionalNumbersData.Location = new System.Drawing.Point(0, 0);
            this.RegionalNumbersData.Name = "RegionalNumbersData";
            this.RegionalNumbersData.Size = new System.Drawing.Size(162, 213);
            this.RegionalNumbersData.TabIndex = 0;
            // 
            // Numbers
            // 
            dataGridViewCellStyle1.Format = "N0";
            dataGridViewCellStyle1.NullValue = null;
            this.Numbers.DefaultCellStyle = dataGridViewCellStyle1;
            this.Numbers.HeaderText = "Numbers";
            this.Numbers.Name = "Numbers";
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(12, 233);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 1;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // ManageRegionalNumbers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(190, 264);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.Panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ManageRegionalNumbers";
            this.ShowInTaskbar = false;
            this.Text = "Regional Numbers Table";
            this.Load += new System.EventHandler(this.ManageRegionalNumbers_Load);
            this.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RegionalNumbersData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Panel;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.DataGridView RegionalNumbersData;
        private System.Windows.Forms.DataGridViewTextBoxColumn Numbers;
    }
}