﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Utilities;
using Utilities.Structures;

namespace App
{
    public partial class ManageEggMoves : Form
    {
        public List<string> EggMovesList = new List<string>();
        public bool Changed = false;

        public ManageEggMoves(List<string> moves)
        {
            InitializeComponent();
            if(moves != null)
            {
                this.EggMovesList.AddRange(moves);
            }
        }

        private void ManageEggMoves_Load(object sender, EventArgs e)
        {
            List<string> moves = Helpers.GetAllMoveNames();
            Moves.Items.AddRange(moves.ToArray());
            if(EggMovesList.Count > 0)
            {
                foreach (string move in this.EggMovesList)
                {
                    EggMovesDataTable.Rows.Add(move);
                }
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            List<string> moves = new List<string>();
            foreach (DataGridViewRow row in EggMovesDataTable.Rows)
            {
                if (row.Cells[0].Value == null)
                {
                    continue;
                }
                moves.Add(row.Cells[0].Value.ToString());
            }
            EggMovesList = moves;
            Changed = true;
            Close();
        }
    }
}
