﻿namespace App
{
    partial class ManageItems
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManageItems));
            this.CommonItemLabel = new System.Windows.Forms.Label();
            this.UncommonItemLabel = new System.Windows.Forms.Label();
            this.RareItemLabel = new System.Windows.Forms.Label();
            this.IncenseLabel = new System.Windows.Forms.Label();
            this.CommonItemInput = new System.Windows.Forms.ComboBox();
            this.UncommonItemInput = new System.Windows.Forms.ComboBox();
            this.RareItemInput = new System.Windows.Forms.ComboBox();
            this.IncenseInput = new System.Windows.Forms.ComboBox();
            this.SaveButton = new System.Windows.Forms.Button();
            this.Panel = new System.Windows.Forms.Panel();
            this.Panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // CommonItemLabel
            // 
            this.CommonItemLabel.AutoSize = true;
            this.CommonItemLabel.Location = new System.Drawing.Point(39, 16);
            this.CommonItemLabel.Name = "CommonItemLabel";
            this.CommonItemLabel.Size = new System.Drawing.Size(74, 13);
            this.CommonItemLabel.TabIndex = 0;
            this.CommonItemLabel.Text = "Common Item:";
            // 
            // UncommonItemLabel
            // 
            this.UncommonItemLabel.AutoSize = true;
            this.UncommonItemLabel.Location = new System.Drawing.Point(26, 63);
            this.UncommonItemLabel.Name = "UncommonItemLabel";
            this.UncommonItemLabel.Size = new System.Drawing.Size(87, 13);
            this.UncommonItemLabel.TabIndex = 1;
            this.UncommonItemLabel.Text = "Uncommon Item:";
            // 
            // RareItemLabel
            // 
            this.RareItemLabel.AutoSize = true;
            this.RareItemLabel.Location = new System.Drawing.Point(57, 109);
            this.RareItemLabel.Name = "RareItemLabel";
            this.RareItemLabel.Size = new System.Drawing.Size(56, 13);
            this.RareItemLabel.TabIndex = 2;
            this.RareItemLabel.Text = "Rare Item:";
            // 
            // IncenseLabel
            // 
            this.IncenseLabel.AutoSize = true;
            this.IncenseLabel.Location = new System.Drawing.Point(14, 156);
            this.IncenseLabel.Name = "IncenseLabel";
            this.IncenseLabel.Size = new System.Drawing.Size(99, 13);
            this.IncenseLabel.TabIndex = 3;
            this.IncenseLabel.Text = "Incense (Breeding):";
            // 
            // CommonItemInput
            // 
            this.CommonItemInput.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CommonItemInput.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CommonItemInput.FormattingEnabled = true;
            this.CommonItemInput.Location = new System.Drawing.Point(119, 13);
            this.CommonItemInput.Name = "CommonItemInput";
            this.CommonItemInput.Size = new System.Drawing.Size(121, 21);
            this.CommonItemInput.TabIndex = 4;
            // 
            // UncommonItemInput
            // 
            this.UncommonItemInput.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.UncommonItemInput.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.UncommonItemInput.FormattingEnabled = true;
            this.UncommonItemInput.Location = new System.Drawing.Point(119, 60);
            this.UncommonItemInput.Name = "UncommonItemInput";
            this.UncommonItemInput.Size = new System.Drawing.Size(121, 21);
            this.UncommonItemInput.TabIndex = 5;
            // 
            // RareItemInput
            // 
            this.RareItemInput.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.RareItemInput.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.RareItemInput.FormattingEnabled = true;
            this.RareItemInput.Location = new System.Drawing.Point(119, 106);
            this.RareItemInput.Name = "RareItemInput";
            this.RareItemInput.Size = new System.Drawing.Size(121, 21);
            this.RareItemInput.TabIndex = 6;
            // 
            // IncenseInput
            // 
            this.IncenseInput.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.IncenseInput.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.IncenseInput.FormattingEnabled = true;
            this.IncenseInput.Location = new System.Drawing.Point(119, 153);
            this.IncenseInput.Name = "IncenseInput";
            this.IncenseInput.Size = new System.Drawing.Size(121, 21);
            this.IncenseInput.TabIndex = 7;
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(20, 226);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 8;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // Panel
            // 
            this.Panel.BackColor = System.Drawing.Color.White;
            this.Panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel.Controls.Add(this.CommonItemInput);
            this.Panel.Controls.Add(this.CommonItemLabel);
            this.Panel.Controls.Add(this.IncenseInput);
            this.Panel.Controls.Add(this.UncommonItemLabel);
            this.Panel.Controls.Add(this.RareItemInput);
            this.Panel.Controls.Add(this.RareItemLabel);
            this.Panel.Controls.Add(this.UncommonItemInput);
            this.Panel.Controls.Add(this.IncenseLabel);
            this.Panel.Location = new System.Drawing.Point(12, 12);
            this.Panel.Name = "Panel";
            this.Panel.Size = new System.Drawing.Size(259, 193);
            this.Panel.TabIndex = 9;
            // 
            // ManageItems
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(287, 261);
            this.Controls.Add(this.Panel);
            this.Controls.Add(this.SaveButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ManageItems";
            this.ShowInTaskbar = false;
            this.Text = "Items Table";
            this.Load += new System.EventHandler(this.ManageItems_Load);
            this.Panel.ResumeLayout(false);
            this.Panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label CommonItemLabel;
        private System.Windows.Forms.Label UncommonItemLabel;
        private System.Windows.Forms.Label RareItemLabel;
        private System.Windows.Forms.Label IncenseLabel;
        private System.Windows.Forms.Button SaveButton;
        public System.Windows.Forms.ComboBox CommonItemInput;
        public System.Windows.Forms.ComboBox UncommonItemInput;
        public System.Windows.Forms.ComboBox RareItemInput;
        public System.Windows.Forms.ComboBox IncenseInput;
        private System.Windows.Forms.Panel Panel;
    }
}