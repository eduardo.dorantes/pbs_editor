﻿namespace App
{
    partial class ManageEggMoves
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManageEggMoves));
            this.Panel = new System.Windows.Forms.Panel();
            this.EggMovesDataTable = new System.Windows.Forms.DataGridView();
            this.Moves = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.SaveButton = new System.Windows.Forms.Button();
            this.Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EggMovesDataTable)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel
            // 
            this.Panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel.Controls.Add(this.EggMovesDataTable);
            this.Panel.Location = new System.Drawing.Point(12, 12);
            this.Panel.Name = "Panel";
            this.Panel.Size = new System.Drawing.Size(247, 238);
            this.Panel.TabIndex = 0;
            // 
            // EggMovesDataTable
            // 
            this.EggMovesDataTable.BackgroundColor = System.Drawing.Color.White;
            this.EggMovesDataTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.EggMovesDataTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.EggMovesDataTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Moves});
            this.EggMovesDataTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EggMovesDataTable.Location = new System.Drawing.Point(0, 0);
            this.EggMovesDataTable.Name = "EggMovesDataTable";
            this.EggMovesDataTable.Size = new System.Drawing.Size(245, 236);
            this.EggMovesDataTable.TabIndex = 0;
            // 
            // Moves
            // 
            this.Moves.HeaderText = "Move Name";
            this.Moves.Name = "Moves";
            this.Moves.Width = 180;
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(12, 256);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 1;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // ManageEggMoves
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(272, 290);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.Panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ManageEggMoves";
            this.ShowInTaskbar = false;
            this.Text = "Egg Moves Table";
            this.Load += new System.EventHandler(this.ManageEggMoves_Load);
            this.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.EggMovesDataTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Panel;
        private System.Windows.Forms.DataGridView EggMovesDataTable;
        private System.Windows.Forms.DataGridViewComboBoxColumn Moves;
        private System.Windows.Forms.Button SaveButton;
    }
}