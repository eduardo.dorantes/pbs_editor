﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace App
{
    public partial class ManageFormNames : Form
    {
        public string[] FormNamesList = new string[0];
        public bool Changed = false;

        public ManageFormNames(string[] forms)
        {
            InitializeComponent();
            if(forms != null)
            {
                FormNamesList = forms;
            }
        }

        private void ManageFormNames_Load(object sender, EventArgs e)
        {
            if(FormNamesList != null && FormNamesList.Length > 0)
            {
                foreach(string form in FormNamesList)
                {
                    FormNamesData.Rows.Add(form);
                }
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            List<string> forms = new List<string>();
            foreach(DataGridViewRow row in FormNamesData.Rows)
            {
                if(row.Cells[0].Value == null)
                {
                    continue;
                }
                forms.Add(row.Cells[0].Value.ToString());
            }
            FormNamesList = forms.ToArray();
            Changed = true;
            Close();
        }
    }
}
