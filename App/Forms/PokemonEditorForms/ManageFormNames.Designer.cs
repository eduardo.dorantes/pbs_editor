﻿namespace App
{
    partial class ManageFormNames
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManageFormNames));
            this.Panel = new System.Windows.Forms.Panel();
            this.FormNamesData = new System.Windows.Forms.DataGridView();
            this.Names = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaveButton = new System.Windows.Forms.Button();
            this.Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FormNamesData)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel
            // 
            this.Panel.BackColor = System.Drawing.Color.White;
            this.Panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel.Controls.Add(this.FormNamesData);
            this.Panel.Location = new System.Drawing.Point(12, 12);
            this.Panel.Name = "Panel";
            this.Panel.Size = new System.Drawing.Size(242, 206);
            this.Panel.TabIndex = 0;
            // 
            // FormNamesData
            // 
            this.FormNamesData.BackgroundColor = System.Drawing.Color.White;
            this.FormNamesData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.FormNamesData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.FormNamesData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Names});
            this.FormNamesData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FormNamesData.Location = new System.Drawing.Point(0, 0);
            this.FormNamesData.Name = "FormNamesData";
            this.FormNamesData.Size = new System.Drawing.Size(240, 204);
            this.FormNamesData.TabIndex = 0;
            // 
            // Names
            // 
            this.Names.HeaderText = "Form Name";
            this.Names.Name = "Names";
            this.Names.Width = 170;
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(12, 226);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 1;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // ManageFormNames
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(271, 261);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.Panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ManageFormNames";
            this.Text = "Form Names Table";
            this.Load += new System.EventHandler(this.ManageFormNames_Load);
            this.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FormNamesData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Panel;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.DataGridView FormNamesData;
        private System.Windows.Forms.DataGridViewTextBoxColumn Names;
    }
}