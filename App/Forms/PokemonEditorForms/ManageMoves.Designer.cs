﻿namespace App
{
    partial class ManageMoves
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManageMoves));
            this.PokemonMovesTable = new System.Windows.Forms.DataGridView();
            this.TablePanel = new System.Windows.Forms.Panel();
            this.SaveButton = new System.Windows.Forms.Button();
            this.Level = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Movements = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.PokemonMovesTable)).BeginInit();
            this.TablePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PokemonMovesTable
            // 
            this.PokemonMovesTable.BackgroundColor = System.Drawing.Color.White;
            this.PokemonMovesTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PokemonMovesTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PokemonMovesTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Level,
            this.Movements});
            this.PokemonMovesTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PokemonMovesTable.Location = new System.Drawing.Point(0, 0);
            this.PokemonMovesTable.Name = "PokemonMovesTable";
            this.PokemonMovesTable.Size = new System.Drawing.Size(298, 282);
            this.PokemonMovesTable.TabIndex = 0;
            // 
            // TablePanel
            // 
            this.TablePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TablePanel.Controls.Add(this.PokemonMovesTable);
            this.TablePanel.Location = new System.Drawing.Point(12, 12);
            this.TablePanel.Name = "TablePanel";
            this.TablePanel.Size = new System.Drawing.Size(300, 284);
            this.TablePanel.TabIndex = 1;
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(12, 307);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 2;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // Level
            // 
            dataGridViewCellStyle1.Format = "N0";
            dataGridViewCellStyle1.NullValue = "0";
            this.Level.DefaultCellStyle = dataGridViewCellStyle1;
            this.Level.HeaderText = "Level";
            this.Level.MaxInputLength = 3;
            this.Level.Name = "Level";
            this.Level.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Level.Width = 50;
            // 
            // Movements
            // 
            this.Movements.HeaderText = "Move";
            this.Movements.Name = "Movements";
            this.Movements.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Movements.Width = 180;
            // 
            // ManageMoves
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 342);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.TablePanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ManageMoves";
            this.ShowInTaskbar = false;
            this.Text = "Moves Table";
            this.Load += new System.EventHandler(this.ManageMoves_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PokemonMovesTable)).EndInit();
            this.TablePanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView PokemonMovesTable;
        private System.Windows.Forms.Panel TablePanel;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn Level;
        private System.Windows.Forms.DataGridViewComboBoxColumn Movements;
    }
}