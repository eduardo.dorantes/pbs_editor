﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace App
{
    public partial class ManageRegionalNumbers : Form
    {
        public List<ushort> RegionalNumbersList = new List<ushort>();
        public bool Changed = false;

        public ManageRegionalNumbers(List<ushort> list)
        {
            InitializeComponent();
            if(list != null)
            {
                RegionalNumbersList.AddRange(list);
            }
        }

        private void ManageRegionalNumbers_Load(object sender, EventArgs e)
        {
            if(RegionalNumbersList.Count > 0)
            {
                foreach(int i in RegionalNumbersList)
                {
                    Console.WriteLine(i);
                    RegionalNumbersData.Rows.Add(i.ToString());
                }
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            List<ushort> list = new List<ushort>();
            foreach(DataGridViewRow row in RegionalNumbersData.Rows)
            {
                if(row.Cells[0].Value == null)
                {
                    continue;
                }
                list.Add((ushort)Convert.ToInt32(row.Cells[0].Value.ToString()));
            }
            RegionalNumbersList.Clear();
            RegionalNumbersList.AddRange(list);
            Changed = true;
            Close();
        }
    }
}
