﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Utilities.Structures;

namespace App
{
    public partial class ManageMoves : Form
    {
        public List<MoveField> MovesList = new List<MoveField>();
        public bool Changed = false;

        public ManageMoves(List<MoveField> moves)
        {
            InitializeComponent();
            if(moves != null)
            {
                MovesList.AddRange(moves);
            }
        }

        private void ManageMoves_Load(object sender, EventArgs e)
        {
            List<string> Moves = Utilities.Helpers.GetAllMoveNames();
            Movements.Items.AddRange(Moves.ToArray());
            if(MovesList.Count > 0)
            {
                foreach (MoveField move in MovesList)
                {
                    PokemonMovesTable.Rows.Add(move.Level, move.Name);
                }
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            List<MoveField> Moves = new List<MoveField>();
            foreach(DataGridViewRow row in PokemonMovesTable.Rows)
            {
                if(row.Cells[0].Value == null || row.Cells[1].Value == null)
                {
                    continue;
                }
                Moves.Add(new MoveField((byte)Convert.ToInt32(row.Cells[0].Value.ToString()), row.Cells[1].Value.ToString()));
            }
            MovesList = Moves;
            Changed = true;
            Close();
        }
    }
}
