﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Utilities;
using Utilities.Structures;

namespace App
{
    public partial class ManageEvolutions : Form
    {
        public List<Evolutions> EvoList = new List<Evolutions>();
        public bool Changed = false;
        private List<string> Items = Helpers.GetAllItemConstantNames();
        private List<string> Moves = Helpers.GetAllMoveNames();

        public ManageEvolutions(List<Evolutions> evos)
        {
            InitializeComponent();
            if(evos != null)
            {
                EvoList.AddRange(evos);
            }
        }

        private void Value_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            string titleText = EvoDataTable.Columns[2].HeaderText;
            if (titleText.Equals("Parameter"))
            {
                TextBox autoText = e.Control as TextBox;
                if (autoText != null)
                {
                    autoText.AutoCompleteMode = AutoCompleteMode.Suggest;
                    autoText.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    AutoCompleteStringCollection DataCollection = new AutoCompleteStringCollection();
                    addItems(DataCollection);
                    autoText.AutoCompleteCustomSource = DataCollection;
                }
            }
        }

        public void addItems(AutoCompleteStringCollection col)
        {
            foreach(string item in Species.Items)
            {
                col.Add(item);
            }
            //col.AddRange((from item in Items where item != null select item).ToArray<string>());
            //col.AddRange(Moves.ToArray());
            //col.AddRange(Items.ToArray());
        }

        private void ManageEvolutions_Load(object sender, EventArgs e)
        {
            List<string> species = Helpers.GetAllPokemonConstantNames();
            EvolutionMethods[] methods = Enum.GetValues(typeof(EvolutionMethods)).Cast<EvolutionMethods>().ToArray();
            
            Species.Items.AddRange(species.ToArray());
            
            foreach(EvolutionMethods m in methods)
            {
                Method.Items.Add(m.ToString());
            }
            if (!Global.v16Detected)
            {
                string[] v15ex = new string[] { "LevelDay", "LevelNight", "LevelDarkInParty", "LevelRain", "HappinessMoveType" };
                foreach(string i in v15ex)
                {
                    Method.Items.Remove(i);
                }
            }
            if (EvoList.Count > 0)
            {
                foreach (Evolutions evo in EvoList)
                {
                    EvoDataTable.Rows.Add(evo.Pokemon, evo.Method.ToString(), evo.Value);
                }
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            List<Evolutions> evos = new List<Evolutions>();
            foreach (DataGridViewRow row in EvoDataTable.Rows)
            {
                if (row.Cells[0].Value == null || row.Cells[1].Value == null || row.Cells[2].Value == null)
                {
                    continue;
                }
                EvolutionMethods meth;
                Enum.TryParse(row.Cells[1].Value.ToString(), true, out meth);
                evos.Add(new Evolutions(row.Cells[0].Value.ToString(), meth, row.Cells[2].Value.ToString()));
            }
            EvoList = evos;
            Changed = true;
            Close();
        }
    }
}
