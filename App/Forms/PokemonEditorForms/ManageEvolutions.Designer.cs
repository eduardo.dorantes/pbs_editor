﻿namespace App
{
    partial class ManageEvolutions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManageEvolutions));
            this.Panel = new System.Windows.Forms.Panel();
            this.EvoDataTable = new System.Windows.Forms.DataGridView();
            this.Species = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Method = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaveButton = new System.Windows.Forms.Button();
            this.Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EvoDataTable)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel
            // 
            this.Panel.BackColor = System.Drawing.Color.White;
            this.Panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel.Controls.Add(this.EvoDataTable);
            this.Panel.Location = new System.Drawing.Point(12, 12);
            this.Panel.Name = "Panel";
            this.Panel.Size = new System.Drawing.Size(516, 221);
            this.Panel.TabIndex = 0;
            // 
            // EvoDataTable
            // 
            this.EvoDataTable.BackgroundColor = System.Drawing.Color.White;
            this.EvoDataTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.EvoDataTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.EvoDataTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Species,
            this.Method,
            this.Value});
            this.EvoDataTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EvoDataTable.Location = new System.Drawing.Point(0, 0);
            this.EvoDataTable.Name = "EvoDataTable";
            this.EvoDataTable.Size = new System.Drawing.Size(514, 219);
            this.EvoDataTable.TabIndex = 0;
            // 
            // Species
            // 
            this.Species.HeaderText = "Pokémon";
            this.Species.Name = "Species";
            this.Species.Width = 150;
            // 
            // Method
            // 
            this.Method.HeaderText = "Evolution Method";
            this.Method.Name = "Method";
            this.Method.Width = 150;
            // 
            // Value
            // 
            this.Value.HeaderText = "Parameter";
            this.Value.Name = "Value";
            this.Value.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Value.Width = 150;
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(13, 250);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 1;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // ManageEvolutions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 284);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.Panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ManageEvolutions";
            this.ShowInTaskbar = false;
            this.Text = "Evolutions Table";
            this.Load += new System.EventHandler(this.ManageEvolutions_Load);
            this.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.EvoDataTable)).EndInit();
            this.ResumeLayout(false);

        }

        private void EvoDataTable_EditingControlShowing1(object sender, System.Windows.Forms.DataGridViewEditingControlShowingEventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private void EvoDataTable_EditingControlShowing(object sender, System.Windows.Forms.DataGridViewEditingControlShowingEventArgs e)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.Panel Panel;
        private System.Windows.Forms.DataGridView EvoDataTable;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.DataGridViewComboBoxColumn Species;
        private System.Windows.Forms.DataGridViewComboBoxColumn Method;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
    }
}