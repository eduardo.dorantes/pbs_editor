﻿namespace App.Forms
{
    partial class MovesEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MovesEditor));
            this.MovesListPanel = new System.Windows.Forms.Panel();
            this.MovesList = new System.Windows.Forms.ListBox();
            this.InternalNameInput = new System.Windows.Forms.TextBox();
            this.DisplayNameInput = new System.Windows.Forms.TextBox();
            this.FunctionCodeInput = new System.Windows.Forms.TextBox();
            this.BasePowerInput = new System.Windows.Forms.NumericUpDown();
            this.TypeSelector = new System.Windows.Forms.ComboBox();
            this.DamageCategorySelector = new System.Windows.Forms.ComboBox();
            this.AccuracyInput = new System.Windows.Forms.NumericUpDown();
            this.TotalPPInput = new System.Windows.Forms.NumericUpDown();
            this.AdditionalEffectInput = new System.Windows.Forms.NumericUpDown();
            this.TargetSelector = new System.Windows.Forms.ListBox();
            this.PriorityInput = new System.Windows.Forms.NumericUpDown();
            this.FlagsSelector = new System.Windows.Forms.ListBox();
            this.DescriptionInput = new System.Windows.Forms.TextBox();
            this.InternalNameLabel = new System.Windows.Forms.Label();
            this.DisplayNameLabel = new System.Windows.Forms.Label();
            this.FunctionCodeLabel = new System.Windows.Forms.Label();
            this.BasePowerLabel = new System.Windows.Forms.Label();
            this.TypeLabel = new System.Windows.Forms.Label();
            this.DamageCategoryLabel = new System.Windows.Forms.Label();
            this.AccuracyLabel = new System.Windows.Forms.Label();
            this.TotalPPLabel = new System.Windows.Forms.Label();
            this.AdditionalEffectLabel = new System.Windows.Forms.Label();
            this.TargetLabel = new System.Windows.Forms.Label();
            this.PriorityLabel = new System.Windows.Forms.Label();
            this.FlagsLabel = new System.Windows.Forms.Label();
            this.DescriptionLabel = new System.Windows.Forms.Label();
            this.MovesListPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BasePowerInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccuracyInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalPPInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AdditionalEffectInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PriorityInput)).BeginInit();
            this.SuspendLayout();
            // 
            // MovesListPanel
            // 
            this.MovesListPanel.Controls.Add(this.MovesList);
            this.MovesListPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.MovesListPanel.Location = new System.Drawing.Point(0, 0);
            this.MovesListPanel.Name = "MovesListPanel";
            this.MovesListPanel.Size = new System.Drawing.Size(200, 410);
            this.MovesListPanel.TabIndex = 0;
            // 
            // MovesList
            // 
            this.MovesList.DisplayMember = "Name";
            this.MovesList.Dock = System.Windows.Forms.DockStyle.Top;
            this.MovesList.FormattingEnabled = true;
            this.MovesList.Location = new System.Drawing.Point(0, 0);
            this.MovesList.Name = "MovesList";
            this.MovesList.Size = new System.Drawing.Size(200, 381);
            this.MovesList.TabIndex = 0;
            this.MovesList.SelectedIndexChanged += new System.EventHandler(this.MovesList_SelectedIndexChanged);
            this.MovesList.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.MovesList_Format);
            // 
            // InternalNameInput
            // 
            this.InternalNameInput.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.InternalNameInput.Location = new System.Drawing.Point(325, 33);
            this.InternalNameInput.Name = "InternalNameInput";
            this.InternalNameInput.Size = new System.Drawing.Size(120, 20);
            this.InternalNameInput.TabIndex = 1;
            // 
            // DisplayNameInput
            // 
            this.DisplayNameInput.Location = new System.Drawing.Point(325, 59);
            this.DisplayNameInput.Name = "DisplayNameInput";
            this.DisplayNameInput.Size = new System.Drawing.Size(120, 20);
            this.DisplayNameInput.TabIndex = 2;
            // 
            // FunctionCodeInput
            // 
            this.FunctionCodeInput.Location = new System.Drawing.Point(325, 85);
            this.FunctionCodeInput.Name = "FunctionCodeInput";
            this.FunctionCodeInput.Size = new System.Drawing.Size(120, 20);
            this.FunctionCodeInput.TabIndex = 3;
            // 
            // BasePowerInput
            // 
            this.BasePowerInput.Location = new System.Drawing.Point(325, 111);
            this.BasePowerInput.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.BasePowerInput.Name = "BasePowerInput";
            this.BasePowerInput.Size = new System.Drawing.Size(120, 20);
            this.BasePowerInput.TabIndex = 4;
            // 
            // TypeSelector
            // 
            this.TypeSelector.FormattingEnabled = true;
            this.TypeSelector.Location = new System.Drawing.Point(325, 138);
            this.TypeSelector.Name = "TypeSelector";
            this.TypeSelector.Size = new System.Drawing.Size(121, 21);
            this.TypeSelector.TabIndex = 5;
            // 
            // DamageCategorySelector
            // 
            this.DamageCategorySelector.FormattingEnabled = true;
            this.DamageCategorySelector.Location = new System.Drawing.Point(325, 165);
            this.DamageCategorySelector.Name = "DamageCategorySelector";
            this.DamageCategorySelector.Size = new System.Drawing.Size(121, 21);
            this.DamageCategorySelector.TabIndex = 6;
            // 
            // AccuracyInput
            // 
            this.AccuracyInput.Location = new System.Drawing.Point(325, 192);
            this.AccuracyInput.Name = "AccuracyInput";
            this.AccuracyInput.Size = new System.Drawing.Size(120, 20);
            this.AccuracyInput.TabIndex = 7;
            // 
            // TotalPPInput
            // 
            this.TotalPPInput.Location = new System.Drawing.Point(325, 218);
            this.TotalPPInput.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.TotalPPInput.Name = "TotalPPInput";
            this.TotalPPInput.Size = new System.Drawing.Size(120, 20);
            this.TotalPPInput.TabIndex = 8;
            // 
            // AdditionalEffectInput
            // 
            this.AdditionalEffectInput.Location = new System.Drawing.Point(325, 244);
            this.AdditionalEffectInput.Name = "AdditionalEffectInput";
            this.AdditionalEffectInput.Size = new System.Drawing.Size(120, 20);
            this.AdditionalEffectInput.TabIndex = 9;
            // 
            // TargetSelector
            // 
            this.TargetSelector.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TargetSelector.FormattingEnabled = true;
            this.TargetSelector.ItemHeight = 12;
            this.TargetSelector.Items.AddRange(new object[] {
            "Single Pokémon other than the user",
            "No target (i.e. Counter, Metal Burst, Mirror Coat, Curse)",
            "Single opposing Pokémon selected at random",
            "All opposing Pokémon",
            "All Pokémon other than the user",
            "User Itself",
            "User\'s side (e.g. Light Screen, Mist)",
            "Both sides (e.g. Sunny Day, Trick Room)",
            "Opposing side (i.e. Spikes, Toxic Spikes, Stealth Rocks)",
            "User\'s partner (i.e. Helping Hand)",
            "Single Pokémon on user\'s side (i.e. Acupressure)",
            "Single opposing Pokémon (i.e. Me First)",
            "Single opposing Pokémon directly opposite of user"});
            this.TargetSelector.Location = new System.Drawing.Point(520, 40);
            this.TargetSelector.Name = "TargetSelector";
            this.TargetSelector.Size = new System.Drawing.Size(260, 112);
            this.TargetSelector.TabIndex = 10;
            // 
            // PriorityInput
            // 
            this.PriorityInput.Location = new System.Drawing.Point(325, 270);
            this.PriorityInput.Maximum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.PriorityInput.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            -2147483648});
            this.PriorityInput.Name = "PriorityInput";
            this.PriorityInput.Size = new System.Drawing.Size(120, 20);
            this.PriorityInput.TabIndex = 11;
            // 
            // FlagsSelector
            // 
            this.FlagsSelector.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FlagsSelector.FormattingEnabled = true;
            this.FlagsSelector.HorizontalScrollbar = true;
            this.FlagsSelector.ItemHeight = 12;
            this.FlagsSelector.Items.AddRange(new object[] {
            "The move makes physical contact with the target.",
            "The target can use Protect or Detect to protect itself from the move.",
            "The target can use Magic Coat to redirect the effect of the move.",
            "The target can use Snatch to steal the effect of the move.",
            "The move can be copied by Mirror Move.",
            "The move has a 10% chance of making the opponent flinch if the user is holding a " +
                "King\'s Rock/Razor Fang.",
            "If the user is frozen, the move will thaw it out before it is used.",
            "The move has a high critical hit rate.",
            "The move is a biting move",
            "The move is a punching move",
            "The move is a sound-based move.",
            "The move is a powder-based move",
            "The move is a pulse-based move",
            "The move is a bomb-based move"});
            this.FlagsSelector.Location = new System.Drawing.Point(520, 177);
            this.FlagsSelector.Name = "FlagsSelector";
            this.FlagsSelector.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.FlagsSelector.Size = new System.Drawing.Size(260, 112);
            this.FlagsSelector.TabIndex = 12;
            // 
            // DescriptionInput
            // 
            this.DescriptionInput.Location = new System.Drawing.Point(238, 335);
            this.DescriptionInput.Multiline = true;
            this.DescriptionInput.Name = "DescriptionInput";
            this.DescriptionInput.Size = new System.Drawing.Size(542, 46);
            this.DescriptionInput.TabIndex = 13;
            // 
            // InternalNameLabel
            // 
            this.InternalNameLabel.AutoSize = true;
            this.InternalNameLabel.Location = new System.Drawing.Point(234, 36);
            this.InternalNameLabel.Name = "InternalNameLabel";
            this.InternalNameLabel.Size = new System.Drawing.Size(76, 13);
            this.InternalNameLabel.TabIndex = 14;
            this.InternalNameLabel.Text = "Internal Name:";
            // 
            // DisplayNameLabel
            // 
            this.DisplayNameLabel.AutoSize = true;
            this.DisplayNameLabel.Location = new System.Drawing.Point(234, 62);
            this.DisplayNameLabel.Name = "DisplayNameLabel";
            this.DisplayNameLabel.Size = new System.Drawing.Size(75, 13);
            this.DisplayNameLabel.TabIndex = 15;
            this.DisplayNameLabel.Text = "Display Name:";
            // 
            // FunctionCodeLabel
            // 
            this.FunctionCodeLabel.AutoSize = true;
            this.FunctionCodeLabel.Location = new System.Drawing.Point(230, 88);
            this.FunctionCodeLabel.Name = "FunctionCodeLabel";
            this.FunctionCodeLabel.Size = new System.Drawing.Size(79, 13);
            this.FunctionCodeLabel.TabIndex = 16;
            this.FunctionCodeLabel.Text = "Function Code:";
            // 
            // BasePowerLabel
            // 
            this.BasePowerLabel.AutoSize = true;
            this.BasePowerLabel.Location = new System.Drawing.Point(242, 113);
            this.BasePowerLabel.Name = "BasePowerLabel";
            this.BasePowerLabel.Size = new System.Drawing.Size(67, 13);
            this.BasePowerLabel.TabIndex = 17;
            this.BasePowerLabel.Text = "Base Power:";
            // 
            // TypeLabel
            // 
            this.TypeLabel.AutoSize = true;
            this.TypeLabel.Location = new System.Drawing.Point(275, 140);
            this.TypeLabel.Name = "TypeLabel";
            this.TypeLabel.Size = new System.Drawing.Size(34, 13);
            this.TypeLabel.TabIndex = 18;
            this.TypeLabel.Text = "Type:";
            // 
            // DamageCategoryLabel
            // 
            this.DamageCategoryLabel.AutoSize = true;
            this.DamageCategoryLabel.Location = new System.Drawing.Point(215, 168);
            this.DamageCategoryLabel.Name = "DamageCategoryLabel";
            this.DamageCategoryLabel.Size = new System.Drawing.Size(95, 13);
            this.DamageCategoryLabel.TabIndex = 19;
            this.DamageCategoryLabel.Text = "Damage Category:";
            // 
            // AccuracyLabel
            // 
            this.AccuracyLabel.AutoSize = true;
            this.AccuracyLabel.Location = new System.Drawing.Point(254, 194);
            this.AccuracyLabel.Name = "AccuracyLabel";
            this.AccuracyLabel.Size = new System.Drawing.Size(55, 13);
            this.AccuracyLabel.TabIndex = 20;
            this.AccuracyLabel.Text = "Accuracy:";
            // 
            // TotalPPLabel
            // 
            this.TotalPPLabel.AutoSize = true;
            this.TotalPPLabel.Location = new System.Drawing.Point(258, 220);
            this.TotalPPLabel.Name = "TotalPPLabel";
            this.TotalPPLabel.Size = new System.Drawing.Size(51, 13);
            this.TotalPPLabel.TabIndex = 21;
            this.TotalPPLabel.Text = "Total PP:";
            // 
            // AdditionalEffectLabel
            // 
            this.AdditionalEffectLabel.AutoSize = true;
            this.AdditionalEffectLabel.Location = new System.Drawing.Point(224, 246);
            this.AdditionalEffectLabel.Name = "AdditionalEffectLabel";
            this.AdditionalEffectLabel.Size = new System.Drawing.Size(87, 13);
            this.AdditionalEffectLabel.TabIndex = 22;
            this.AdditionalEffectLabel.Text = "Additional Effect:";
            // 
            // TargetLabel
            // 
            this.TargetLabel.AutoSize = true;
            this.TargetLabel.Location = new System.Drawing.Point(473, 40);
            this.TargetLabel.Name = "TargetLabel";
            this.TargetLabel.Size = new System.Drawing.Size(41, 13);
            this.TargetLabel.TabIndex = 23;
            this.TargetLabel.Text = "Target:";
            // 
            // PriorityLabel
            // 
            this.PriorityLabel.AutoSize = true;
            this.PriorityLabel.Location = new System.Drawing.Point(268, 272);
            this.PriorityLabel.Name = "PriorityLabel";
            this.PriorityLabel.Size = new System.Drawing.Size(41, 13);
            this.PriorityLabel.TabIndex = 24;
            this.PriorityLabel.Text = "Priority:";
            // 
            // FlagsLabel
            // 
            this.FlagsLabel.AutoSize = true;
            this.FlagsLabel.Location = new System.Drawing.Point(479, 177);
            this.FlagsLabel.Name = "FlagsLabel";
            this.FlagsLabel.Size = new System.Drawing.Size(35, 13);
            this.FlagsLabel.TabIndex = 25;
            this.FlagsLabel.Text = "Flags:";
            // 
            // DescriptionLabel
            // 
            this.DescriptionLabel.AutoSize = true;
            this.DescriptionLabel.Location = new System.Drawing.Point(235, 319);
            this.DescriptionLabel.Name = "DescriptionLabel";
            this.DescriptionLabel.Size = new System.Drawing.Size(63, 13);
            this.DescriptionLabel.TabIndex = 26;
            this.DescriptionLabel.Text = "Description:";
            // 
            // MovesEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 410);
            this.Controls.Add(this.DescriptionLabel);
            this.Controls.Add(this.FlagsLabel);
            this.Controls.Add(this.PriorityLabel);
            this.Controls.Add(this.TargetLabel);
            this.Controls.Add(this.AdditionalEffectLabel);
            this.Controls.Add(this.TotalPPLabel);
            this.Controls.Add(this.AccuracyLabel);
            this.Controls.Add(this.DamageCategoryLabel);
            this.Controls.Add(this.TypeLabel);
            this.Controls.Add(this.BasePowerLabel);
            this.Controls.Add(this.FunctionCodeLabel);
            this.Controls.Add(this.DisplayNameLabel);
            this.Controls.Add(this.InternalNameLabel);
            this.Controls.Add(this.DescriptionInput);
            this.Controls.Add(this.FlagsSelector);
            this.Controls.Add(this.PriorityInput);
            this.Controls.Add(this.TargetSelector);
            this.Controls.Add(this.AdditionalEffectInput);
            this.Controls.Add(this.TotalPPInput);
            this.Controls.Add(this.AccuracyInput);
            this.Controls.Add(this.DamageCategorySelector);
            this.Controls.Add(this.TypeSelector);
            this.Controls.Add(this.BasePowerInput);
            this.Controls.Add(this.FunctionCodeInput);
            this.Controls.Add(this.DisplayNameInput);
            this.Controls.Add(this.InternalNameInput);
            this.Controls.Add(this.MovesListPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MovesEditor";
            this.ShowInTaskbar = false;
            this.Text = "Moves Editor";
            this.Load += new System.EventHandler(this.MovesEditor_Load);
            this.MovesListPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BasePowerInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccuracyInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalPPInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AdditionalEffectInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PriorityInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel MovesListPanel;
        private System.Windows.Forms.ListBox MovesList;
        private System.Windows.Forms.TextBox InternalNameInput;
        private System.Windows.Forms.TextBox DisplayNameInput;
        private System.Windows.Forms.TextBox FunctionCodeInput;
        private System.Windows.Forms.NumericUpDown BasePowerInput;
        private System.Windows.Forms.ComboBox TypeSelector;
        private System.Windows.Forms.ComboBox DamageCategorySelector;
        private System.Windows.Forms.NumericUpDown AccuracyInput;
        private System.Windows.Forms.NumericUpDown TotalPPInput;
        private System.Windows.Forms.NumericUpDown AdditionalEffectInput;
        private System.Windows.Forms.ListBox TargetSelector;
        private System.Windows.Forms.NumericUpDown PriorityInput;
        private System.Windows.Forms.ListBox FlagsSelector;
        private System.Windows.Forms.TextBox DescriptionInput;
        private System.Windows.Forms.Label InternalNameLabel;
        private System.Windows.Forms.Label DisplayNameLabel;
        private System.Windows.Forms.Label FunctionCodeLabel;
        private System.Windows.Forms.Label BasePowerLabel;
        private System.Windows.Forms.Label TypeLabel;
        private System.Windows.Forms.Label DamageCategoryLabel;
        private System.Windows.Forms.Label AccuracyLabel;
        private System.Windows.Forms.Label TotalPPLabel;
        private System.Windows.Forms.Label AdditionalEffectLabel;
        private System.Windows.Forms.Label TargetLabel;
        private System.Windows.Forms.Label PriorityLabel;
        private System.Windows.Forms.Label FlagsLabel;
        private System.Windows.Forms.Label DescriptionLabel;
    }
}