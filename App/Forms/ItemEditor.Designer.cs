﻿namespace App
{
    partial class ItemEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemEditor));
            this.ItemsListBox = new System.Windows.Forms.ListBox();
            this.ItemListPanel = new System.Windows.Forms.Panel();
            this.ItemListToolStrip = new System.Windows.Forms.ToolStrip();
            this.AddItemButton = new System.Windows.Forms.ToolStripButton();
            this.DeleteItemButton = new System.Windows.Forms.ToolStripButton();
            this.MoveUpButton = new System.Windows.Forms.ToolStripButton();
            this.MoveDownButton = new System.Windows.Forms.ToolStripButton();
            this.PreviewButton = new System.Windows.Forms.ToolStripButton();
            this.OKButton = new System.Windows.Forms.ToolStripButton();
            this.txtInternalName = new System.Windows.Forms.TextBox();
            this.txtDisplayName = new System.Windows.Forms.TextBox();
            this.txtPluralName = new System.Windows.Forms.TextBox();
            this.lstPocket = new System.Windows.Forms.ListBox();
            this.numPrice = new System.Windows.Forms.NumericUpDown();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lstOutsideBattle = new System.Windows.Forms.ListBox();
            this.lstInBattle = new System.Windows.Forms.ListBox();
            this.lstSpecialItem = new System.Windows.Forms.ListBox();
            this.cbxMove = new System.Windows.Forms.ComboBox();
            this.lblInternalName = new System.Windows.Forms.Label();
            this.lblDisplayName = new System.Windows.Forms.Label();
            this.lblPluralName = new System.Windows.Forms.Label();
            this.lblPocket = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.lblMove = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblOutsideBattle = new System.Windows.Forms.Label();
            this.lblInBattle = new System.Windows.Forms.Label();
            this.lblSpecialItem = new System.Windows.Forms.Label();
            this.IconBox = new System.Windows.Forms.PictureBox();
            this.ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.OpenIconDialog = new System.Windows.Forms.OpenFileDialog();
            this.SaveIconDialog = new System.Windows.Forms.SaveFileDialog();
            this.ItemListPanel.SuspendLayout();
            this.ItemListToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IconBox)).BeginInit();
            this.SuspendLayout();
            // 
            // ItemsListBox
            // 
            this.ItemsListBox.DisplayMember = "Name";
            this.ItemsListBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.ItemsListBox.FormattingEnabled = true;
            this.ItemsListBox.Location = new System.Drawing.Point(0, 0);
            this.ItemsListBox.Name = "ItemsListBox";
            this.ItemsListBox.Size = new System.Drawing.Size(161, 368);
            this.ItemsListBox.TabIndex = 0;
            this.ItemsListBox.SelectedIndexChanged += new System.EventHandler(this.ItemsListBox_SelectedIndexChanged);
            this.ItemsListBox.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.ItemsListBox_Format);
            // 
            // ItemListPanel
            // 
            this.ItemListPanel.Controls.Add(this.ItemListToolStrip);
            this.ItemListPanel.Controls.Add(this.ItemsListBox);
            this.ItemListPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.ItemListPanel.Location = new System.Drawing.Point(0, 0);
            this.ItemListPanel.Name = "ItemListPanel";
            this.ItemListPanel.Size = new System.Drawing.Size(161, 394);
            this.ItemListPanel.TabIndex = 1;
            // 
            // ItemListToolStrip
            // 
            this.ItemListToolStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ItemListToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ItemListToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddItemButton,
            this.DeleteItemButton,
            this.MoveUpButton,
            this.MoveDownButton,
            this.PreviewButton,
            this.OKButton});
            this.ItemListToolStrip.Location = new System.Drawing.Point(0, 369);
            this.ItemListToolStrip.Name = "ItemListToolStrip";
            this.ItemListToolStrip.Size = new System.Drawing.Size(161, 25);
            this.ItemListToolStrip.TabIndex = 1;
            this.ItemListToolStrip.Text = "toolStrip1";
            // 
            // AddItemButton
            // 
            this.AddItemButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.AddItemButton.Image = Properties.Resources.AddIcon;
            this.AddItemButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AddItemButton.Name = "AddItemButton";
            this.AddItemButton.Size = new System.Drawing.Size(23, 22);
            this.AddItemButton.Text = "Add Item";
            this.AddItemButton.ToolTipText = "Inserts a new item at the current intex.";
            this.AddItemButton.Click += new System.EventHandler(this.AddItemButton_Click);
            // 
            // DeleteItemButton
            // 
            this.DeleteItemButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.DeleteItemButton.Image = global::App.Properties.Resources.DeleteIcon;
            this.DeleteItemButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.DeleteItemButton.Name = "DeleteItemButton";
            this.DeleteItemButton.Size = new System.Drawing.Size(23, 22);
            this.DeleteItemButton.Text = "Delete Item";
            this.DeleteItemButton.ToolTipText = "Removes the current item.";
            this.DeleteItemButton.Click += new System.EventHandler(this.DeleteItemButton_Click);
            // 
            // MoveUpButton
            // 
            this.MoveUpButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.MoveUpButton.Image = global::App.Properties.Resources.UpIcon;
            this.MoveUpButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MoveUpButton.Name = "MoveUpButton";
            this.MoveUpButton.Size = new System.Drawing.Size(23, 22);
            this.MoveUpButton.Text = "Move Up";
            this.MoveUpButton.Click += new System.EventHandler(this.MoveUpButton_Click);
            // 
            // MoveDownButton
            // 
            this.MoveDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.MoveDownButton.Image = global::App.Properties.Resources.DownIcon;
            this.MoveDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MoveDownButton.Name = "MoveDownButton";
            this.MoveDownButton.Size = new System.Drawing.Size(23, 22);
            this.MoveDownButton.Text = "Move Down";
            this.MoveDownButton.Click += new System.EventHandler(this.MoveDownButton_Click);
            // 
            // PreviewButton
            // 
            this.PreviewButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PreviewButton.Image = global::App.Properties.Resources.PreviewIcon;
            this.PreviewButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PreviewButton.Name = "PreviewButton";
            this.PreviewButton.Size = new System.Drawing.Size(23, 22);
            this.PreviewButton.Text = "Preview";
            this.PreviewButton.ToolTipText = "Shows the current item in string format.";
            this.PreviewButton.Click += new System.EventHandler(this.PreviewButton_Click);
            // 
            // OKButton
            // 
            this.OKButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OKButton.Image = global::App.Properties.Resources.OKIcon;
            this.OKButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(23, 22);
            this.OKButton.Text = "Save Item";
            this.OKButton.ToolTipText = "Sets the current item\'s data.";
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // txtInternalName
            // 
            this.txtInternalName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtInternalName.Location = new System.Drawing.Point(296, 26);
            this.txtInternalName.MaxLength = 255;
            this.txtInternalName.Name = "txtInternalName";
            this.txtInternalName.Size = new System.Drawing.Size(120, 20);
            this.txtInternalName.TabIndex = 2;
            this.txtInternalName.TabStop = false;
            this.ToolTip.SetToolTip(this.txtInternalName, "Uppercase, is used as a constant.");
            // 
            // txtDisplayName
            // 
            this.txtDisplayName.Location = new System.Drawing.Point(297, 65);
            this.txtDisplayName.MaxLength = 255;
            this.txtDisplayName.Name = "txtDisplayName";
            this.txtDisplayName.Size = new System.Drawing.Size(119, 20);
            this.txtDisplayName.TabIndex = 3;
            this.txtDisplayName.TabStop = false;
            this.ToolTip.SetToolTip(this.txtDisplayName, "Name displayed in the game.");
            // 
            // txtPluralName
            // 
            this.txtPluralName.Enabled = false;
            this.txtPluralName.Location = new System.Drawing.Point(298, 107);
            this.txtPluralName.MaxLength = 255;
            this.txtPluralName.Name = "txtPluralName";
            this.txtPluralName.Size = new System.Drawing.Size(118, 20);
            this.txtPluralName.TabIndex = 4;
            this.txtPluralName.TabStop = false;
            this.ToolTip.SetToolTip(this.txtPluralName, "Name displayed when the player has more than 1. Only in v16+");
            // 
            // lstPocket
            // 
            this.lstPocket.FormattingEnabled = true;
            this.lstPocket.Items.AddRange(new object[] {
            "Items",
            "Medicine",
            "Poké Balls",
            "TMs & HMs",
            "Berries",
            "Mail",
            "Battle Items",
            "Key Items"});
            this.lstPocket.Location = new System.Drawing.Point(296, 151);
            this.lstPocket.Name = "lstPocket";
            this.lstPocket.Size = new System.Drawing.Size(120, 95);
            this.lstPocket.TabIndex = 5;
            this.lstPocket.TabStop = false;
            this.lstPocket.SelectedIndexChanged += new System.EventHandler(this.lstPocket_SelectedIndexChanged);
            // 
            // numPrice
            // 
            this.numPrice.Location = new System.Drawing.Point(296, 269);
            this.numPrice.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numPrice.Name = "numPrice";
            this.numPrice.Size = new System.Drawing.Size(120, 20);
            this.numPrice.TabIndex = 6;
            this.numPrice.TabStop = false;
            this.ToolTip.SetToolTip(this.numPrice, "Item\'s price in shops. If it\'s equal to 0 then the player can\'t sell it.");
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(295, 348);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(594, 34);
            this.txtDescription.TabIndex = 7;
            this.txtDescription.TabStop = false;
            // 
            // lstOutsideBattle
            // 
            this.lstOutsideBattle.FormattingEnabled = true;
            this.lstOutsideBattle.Items.AddRange(new object[] {
            "The item cannot be used outside of battle.",
            "The item can be used on a Pokémon, and disappears after use.",
            "The item can be used out of battle, but it isn\'t used on a Pokémon.",
            "The item is a TM. It teaches a move to a Pokémon, and disappears after use.",
            "The item is a HM. It teaches a move to a Pokémon, but does not disappear after us" +
                "e.",
            "The item can be used on a Pokémon (like 1), but it does not disappear after use."});
            this.lstOutsideBattle.Location = new System.Drawing.Point(449, 45);
            this.lstOutsideBattle.Name = "lstOutsideBattle";
            this.lstOutsideBattle.Size = new System.Drawing.Size(440, 69);
            this.lstOutsideBattle.TabIndex = 8;
            this.lstOutsideBattle.TabStop = false;
            // 
            // lstInBattle
            // 
            this.lstInBattle.FormattingEnabled = true;
            this.lstInBattle.Items.AddRange(new object[] {
            "The item cannot be used in battle.",
            "The item can be used on one of your party Pokémon, and disappears after use.",
            "The item is a Poké Ball, is used on the active Pokémon you are choosing a command" +
                " for.",
            "The item can be used on a Pokémon (like 1), but does not disappear after use.",
            "The item can be used directly, but does not disappear after use."});
            this.lstInBattle.Location = new System.Drawing.Point(449, 145);
            this.lstInBattle.Name = "lstInBattle";
            this.lstInBattle.Size = new System.Drawing.Size(440, 69);
            this.lstInBattle.TabIndex = 9;
            this.lstInBattle.TabStop = false;
            // 
            // lstSpecialItem
            // 
            this.lstSpecialItem.FormattingEnabled = true;
            this.lstSpecialItem.Items.AddRange(new object[] {
            "The item is none of the items below.",
            "The item is a Mail item.",
            "The item is a Mail item, and the images of Pokémon in your party appear on the Ma" +
                "il.",
            "The item is a Snag Ball.",
            "The item is a Poké Ball item.",
            "The item is a berry that can be planted.",
            "The item is a Key Item."});
            this.lstSpecialItem.Location = new System.Drawing.Point(449, 249);
            this.lstSpecialItem.Name = "lstSpecialItem";
            this.lstSpecialItem.Size = new System.Drawing.Size(440, 69);
            this.lstSpecialItem.TabIndex = 10;
            this.lstSpecialItem.TabStop = false;
            // 
            // cbxMove
            // 
            this.cbxMove.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbxMove.FormattingEnabled = true;
            this.cbxMove.Location = new System.Drawing.Point(295, 309);
            this.cbxMove.Name = "cbxMove";
            this.cbxMove.Size = new System.Drawing.Size(121, 21);
            this.cbxMove.TabIndex = 11;
            this.cbxMove.TabStop = false;
            this.ToolTip.SetToolTip(this.cbxMove, "The move this TM/HM teaches.");
            this.cbxMove.SelectedIndexChanged += new System.EventHandler(this.cbxMove_SelectedIndexChanged);
            // 
            // lblInternalName
            // 
            this.lblInternalName.AutoSize = true;
            this.lblInternalName.Location = new System.Drawing.Point(209, 30);
            this.lblInternalName.Name = "lblInternalName";
            this.lblInternalName.Size = new System.Drawing.Size(76, 13);
            this.lblInternalName.TabIndex = 12;
            this.lblInternalName.Text = "Internal Name:";
            // 
            // lblDisplayName
            // 
            this.lblDisplayName.AutoSize = true;
            this.lblDisplayName.Location = new System.Drawing.Point(209, 68);
            this.lblDisplayName.Name = "lblDisplayName";
            this.lblDisplayName.Size = new System.Drawing.Size(75, 13);
            this.lblDisplayName.TabIndex = 13;
            this.lblDisplayName.Text = "Display Name:";
            // 
            // lblPluralName
            // 
            this.lblPluralName.AutoSize = true;
            this.lblPluralName.Enabled = false;
            this.lblPluralName.Location = new System.Drawing.Point(180, 110);
            this.lblPluralName.Name = "lblPluralName";
            this.lblPluralName.Size = new System.Drawing.Size(104, 13);
            this.lblPluralName.TabIndex = 14;
            this.lblPluralName.Text = "Plural Display Name:";
            // 
            // lblPocket
            // 
            this.lblPocket.AutoSize = true;
            this.lblPocket.Location = new System.Drawing.Point(240, 151);
            this.lblPocket.Name = "lblPocket";
            this.lblPocket.Size = new System.Drawing.Size(44, 13);
            this.lblPocket.TabIndex = 15;
            this.lblPocket.Text = "Pocket:";
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(250, 271);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(34, 13);
            this.lblPrice.TabIndex = 16;
            this.lblPrice.Text = "Price:";
            // 
            // lblMove
            // 
            this.lblMove.AutoSize = true;
            this.lblMove.Location = new System.Drawing.Point(206, 312);
            this.lblMove.Name = "lblMove";
            this.lblMove.Size = new System.Drawing.Size(78, 13);
            this.lblMove.TabIndex = 17;
            this.lblMove.Text = "TM/HM Move:";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(221, 351);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(63, 13);
            this.lblDescription.TabIndex = 18;
            this.lblDescription.Text = "Description:";
            // 
            // lblOutsideBattle
            // 
            this.lblOutsideBattle.AutoSize = true;
            this.lblOutsideBattle.Location = new System.Drawing.Point(444, 29);
            this.lblOutsideBattle.Name = "lblOutsideBattle";
            this.lblOutsideBattle.Size = new System.Drawing.Size(118, 13);
            this.lblOutsideBattle.TabIndex = 19;
            this.lblOutsideBattle.Text = "Usability Outside Battle:";
            // 
            // lblInBattle
            // 
            this.lblInBattle.AutoSize = true;
            this.lblInBattle.Location = new System.Drawing.Point(444, 129);
            this.lblInBattle.Name = "lblInBattle";
            this.lblInBattle.Size = new System.Drawing.Size(91, 13);
            this.lblInBattle.TabIndex = 20;
            this.lblInBattle.Text = "Usability In Battle:";
            // 
            // lblSpecialItem
            // 
            this.lblSpecialItem.AutoSize = true;
            this.lblSpecialItem.Location = new System.Drawing.Point(444, 233);
            this.lblSpecialItem.Name = "lblSpecialItem";
            this.lblSpecialItem.Size = new System.Drawing.Size(68, 13);
            this.lblSpecialItem.TabIndex = 21;
            this.lblSpecialItem.Text = "Special Item:";
            // 
            // IconBox
            // 
            this.IconBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.IconBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IconBox.Location = new System.Drawing.Point(853, 5);
            this.IconBox.Name = "IconBox";
            this.IconBox.Size = new System.Drawing.Size(36, 34);
            this.IconBox.TabIndex = 22;
            this.IconBox.TabStop = false;
            this.IconBox.Click += new System.EventHandler(this.IconBox_Click);
            // 
            // OpenIconDialog
            // 
            this.OpenIconDialog.FileName = "openFileDialog1";
            this.OpenIconDialog.Filter = "PNG Image|*.png";
            this.OpenIconDialog.RestoreDirectory = true;
            // 
            // SaveIconDialog
            // 
            this.SaveIconDialog.Filter = "PNG Image|*.png";
            this.SaveIconDialog.RestoreDirectory = true;
            // 
            // ItemEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(901, 394);
            this.Controls.Add(this.IconBox);
            this.Controls.Add(this.lblSpecialItem);
            this.Controls.Add(this.lblInBattle);
            this.Controls.Add(this.lblOutsideBattle);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblMove);
            this.Controls.Add(this.lblPrice);
            this.Controls.Add(this.lblPocket);
            this.Controls.Add(this.lblPluralName);
            this.Controls.Add(this.lblDisplayName);
            this.Controls.Add(this.lblInternalName);
            this.Controls.Add(this.cbxMove);
            this.Controls.Add(this.lstSpecialItem);
            this.Controls.Add(this.lstInBattle);
            this.Controls.Add(this.lstOutsideBattle);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.numPrice);
            this.Controls.Add(this.lstPocket);
            this.Controls.Add(this.txtPluralName);
            this.Controls.Add(this.txtDisplayName);
            this.Controls.Add(this.txtInternalName);
            this.Controls.Add(this.ItemListPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ItemEditor";
            this.Text = "Item Editor";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.ItemEditor_HelpButtonClicked);
            this.Load += new System.EventHandler(this.ItemEditor_Load);
            this.ItemListPanel.ResumeLayout(false);
            this.ItemListPanel.PerformLayout();
            this.ItemListToolStrip.ResumeLayout(false);
            this.ItemListToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IconBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox ItemsListBox;
        private System.Windows.Forms.Panel ItemListPanel;
        private System.Windows.Forms.ToolStrip ItemListToolStrip;
        private System.Windows.Forms.TextBox txtInternalName;
        private System.Windows.Forms.TextBox txtDisplayName;
        private System.Windows.Forms.TextBox txtPluralName;
        private System.Windows.Forms.ListBox lstPocket;
        private System.Windows.Forms.NumericUpDown numPrice;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.ListBox lstOutsideBattle;
        private System.Windows.Forms.ListBox lstInBattle;
        private System.Windows.Forms.ListBox lstSpecialItem;
        private System.Windows.Forms.ComboBox cbxMove;
        private System.Windows.Forms.Label lblInternalName;
        private System.Windows.Forms.Label lblDisplayName;
        private System.Windows.Forms.Label lblPluralName;
        private System.Windows.Forms.Label lblPocket;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblMove;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblOutsideBattle;
        private System.Windows.Forms.Label lblInBattle;
        private System.Windows.Forms.Label lblSpecialItem;
        private System.Windows.Forms.ToolStripButton AddItemButton;
        private System.Windows.Forms.ToolStripButton DeleteItemButton;
        private System.Windows.Forms.ToolStripButton MoveUpButton;
        private System.Windows.Forms.ToolStripButton MoveDownButton;
        private System.Windows.Forms.ToolStripButton PreviewButton;
        private System.Windows.Forms.ToolStripButton OKButton;
        private System.Windows.Forms.PictureBox IconBox;
        private System.Windows.Forms.ToolTip ToolTip;
        private System.Windows.Forms.OpenFileDialog OpenIconDialog;
        private System.Windows.Forms.SaveFileDialog SaveIconDialog;
    }
}