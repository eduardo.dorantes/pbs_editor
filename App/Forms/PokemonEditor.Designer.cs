﻿namespace App
{
    partial class PokemonEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PokemonEditor));
            this.ListPanel = new System.Windows.Forms.Panel();
            this.ListTools = new System.Windows.Forms.ToolStrip();
            this.AddItemButton = new System.Windows.Forms.ToolStripButton();
            this.DeleteItem = new System.Windows.Forms.ToolStripButton();
            this.MoveUpButton = new System.Windows.Forms.ToolStripButton();
            this.MoveDownButton = new System.Windows.Forms.ToolStripButton();
            this.PreviewButton = new System.Windows.Forms.ToolStripButton();
            this.SaveItemButton = new System.Windows.Forms.ToolStripButton();
            this.PokemonList = new System.Windows.Forms.ListBox();
            this.NameLabel = new System.Windows.Forms.Label();
            this.InternalNameLabel = new System.Windows.Forms.Label();
            this.Type1Label = new System.Windows.Forms.Label();
            this.Type2Label = new System.Windows.Forms.Label();
            this.NameInput = new System.Windows.Forms.TextBox();
            this.InternalNameInput = new System.Windows.Forms.TextBox();
            this.Type1Selector = new System.Windows.Forms.ListBox();
            this.Type2Selector = new System.Windows.Forms.ListBox();
            this.SecondaryType = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SecondaryAbilityLabel = new System.Windows.Forms.Label();
            this.SecondaryAbility = new System.Windows.Forms.CheckBox();
            this.HiddenAbilityLabel = new System.Windows.Forms.Label();
            this.HiddenAbility = new System.Windows.Forms.CheckBox();
            this.AbilitiesBox = new System.Windows.Forms.GroupBox();
            this.HiddenAbilityInput = new System.Windows.Forms.ComboBox();
            this.SecondaryAbilityInput = new System.Windows.Forms.ComboBox();
            this.MainAbilityInput = new System.Windows.Forms.ComboBox();
            this.BasicBox = new System.Windows.Forms.GroupBox();
            this.GenderRateInput = new System.Windows.Forms.ComboBox();
            this.GenderRateLabel = new System.Windows.Forms.Label();
            this.PokedexnEvoBox = new System.Windows.Forms.GroupBox();
            this.FormNamesButton = new System.Windows.Forms.Button();
            this.RegionalNumbersButton = new System.Windows.Forms.Button();
            this.KindInput = new System.Windows.Forms.TextBox();
            this.KindLabel = new System.Windows.Forms.Label();
            this.CompatibilitySelector = new System.Windows.Forms.ListBox();
            this.CompatibilityLabel = new System.Windows.Forms.Label();
            this.HabitatSelector = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.PokedexEntryInput = new System.Windows.Forms.TextBox();
            this.PokedexEntryLabel = new System.Windows.Forms.Label();
            this.MovesBox = new System.Windows.Forms.GroupBox();
            this.ManageEvolutions = new System.Windows.Forms.Button();
            this.ManageItems = new System.Windows.Forms.Button();
            this.ManageEggMoves = new System.Windows.Forms.Button();
            this.ManageMoves = new System.Windows.Forms.Button();
            this.OtherBox = new System.Windows.Forms.GroupBox();
            this.GrowthRateInput = new System.Windows.Forms.ComboBox();
            this.HappinessInput = new System.Windows.Forms.NumericUpDown();
            this.StepsToHatchInput = new System.Windows.Forms.NumericUpDown();
            this.RarenessInput = new System.Windows.Forms.NumericUpDown();
            this.StepsToHatchLabel = new System.Windows.Forms.Label();
            this.HappinessLabel = new System.Windows.Forms.Label();
            this.RarenessLabel = new System.Windows.Forms.Label();
            this.GrowthRateLabel = new System.Windows.Forms.Label();
            this.BattlePosBox = new System.Windows.Forms.GroupBox();
            this.APosInput = new System.Windows.Forms.NumericUpDown();
            this.APosLabel = new System.Windows.Forms.Label();
            this.YPosInput = new System.Windows.Forms.NumericUpDown();
            this.eYPosInput = new System.Windows.Forms.NumericUpDown();
            this.pYLabel = new System.Windows.Forms.Label();
            this.eYLabel = new System.Windows.Forms.Label();
            this.IdentityBox = new System.Windows.Forms.GroupBox();
            this.ColorInput = new System.Windows.Forms.ComboBox();
            this.WeightInput = new System.Windows.Forms.NumericUpDown();
            this.HeightInput = new System.Windows.Forms.NumericUpDown();
            this.WeightLabel = new System.Windows.Forms.Label();
            this.HeightLabel = new System.Windows.Forms.Label();
            this.ColorLabel = new System.Windows.Forms.Label();
            this.BaseValuesBox = new System.Windows.Forms.GroupBox();
            this.BaseStatsInput = new System.Windows.Forms.MaskedTextBox();
            this.EffortPointsInput = new System.Windows.Forms.MaskedTextBox();
            this.BaseExpInput = new System.Windows.Forms.NumericUpDown();
            this.BaseExpLabel = new System.Windows.Forms.Label();
            this.BaseStatsLabel = new System.Windows.Forms.Label();
            this.EPLabel = new System.Windows.Forms.Label();
            this.IconBox = new System.Windows.Forms.GroupBox();
            this.IconPicture = new System.Windows.Forms.PictureBox();
            this.OpenIconFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.SaveIconFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.ListPanel.SuspendLayout();
            this.ListTools.SuspendLayout();
            this.AbilitiesBox.SuspendLayout();
            this.BasicBox.SuspendLayout();
            this.PokedexnEvoBox.SuspendLayout();
            this.MovesBox.SuspendLayout();
            this.OtherBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HappinessInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StepsToHatchInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RarenessInput)).BeginInit();
            this.BattlePosBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.APosInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YPosInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eYPosInput)).BeginInit();
            this.IdentityBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WeightInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeightInput)).BeginInit();
            this.BaseValuesBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BaseExpInput)).BeginInit();
            this.IconBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IconPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // ListPanel
            // 
            this.ListPanel.Controls.Add(this.ListTools);
            this.ListPanel.Controls.Add(this.PokemonList);
            this.ListPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.ListPanel.Location = new System.Drawing.Point(0, 0);
            this.ListPanel.Name = "ListPanel";
            this.ListPanel.Size = new System.Drawing.Size(158, 419);
            this.ListPanel.TabIndex = 0;
            // 
            // ListTools
            // 
            this.ListTools.Dock = System.Windows.Forms.DockStyle.None;
            this.ListTools.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.ListTools.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddItemButton,
            this.DeleteItem,
            this.MoveUpButton,
            this.MoveDownButton,
            this.PreviewButton,
            this.SaveItemButton});
            this.ListTools.Location = new System.Drawing.Point(0, 394);
            this.ListTools.Name = "ListTools";
            this.ListTools.Size = new System.Drawing.Size(141, 25);
            this.ListTools.TabIndex = 2;
            this.ListTools.Text = "toolStrip1";
            // 
            // AddItemButton
            // 
            this.AddItemButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.AddItemButton.Image = global::App.Properties.Resources.AddIcon;
            this.AddItemButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AddItemButton.Name = "AddItemButton";
            this.AddItemButton.Size = new System.Drawing.Size(23, 22);
            this.AddItemButton.Text = "Add Item";
            this.AddItemButton.ToolTipText = "Inserts a new field in the current position.";
            this.AddItemButton.Click += new System.EventHandler(this.AddItemButton_Click);
            // 
            // DeleteItem
            // 
            this.DeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.DeleteItem.Image = global::App.Properties.Resources.DeleteIcon;
            this.DeleteItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.DeleteItem.Name = "DeleteItem";
            this.DeleteItem.Size = new System.Drawing.Size(23, 22);
            this.DeleteItem.Text = "Delete Item";
            this.DeleteItem.ToolTipText = "Removes the current item.";
            this.DeleteItem.Click += new System.EventHandler(this.DeleteItem_Click);
            // 
            // MoveUpButton
            // 
            this.MoveUpButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.MoveUpButton.Image = global::App.Properties.Resources.UpIcon;
            this.MoveUpButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MoveUpButton.Name = "MoveUpButton";
            this.MoveUpButton.Size = new System.Drawing.Size(23, 22);
            this.MoveUpButton.Text = "Move Up";
            this.MoveUpButton.Click += new System.EventHandler(this.MoveUpButton_Click);
            // 
            // MoveDownButton
            // 
            this.MoveDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.MoveDownButton.Image = global::App.Properties.Resources.DownIcon;
            this.MoveDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.MoveDownButton.Name = "MoveDownButton";
            this.MoveDownButton.Size = new System.Drawing.Size(23, 22);
            this.MoveDownButton.Text = "Move Down";
            this.MoveDownButton.Click += new System.EventHandler(this.MoveDownButton_Click);
            // 
            // PreviewButton
            // 
            this.PreviewButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.PreviewButton.Image = global::App.Properties.Resources.PreviewIcon;
            this.PreviewButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PreviewButton.Name = "PreviewButton";
            this.PreviewButton.Size = new System.Drawing.Size(23, 22);
            this.PreviewButton.Text = "Preview";
            this.PreviewButton.ToolTipText = "Preview the current Pokémon\'s data in string format.";
            this.PreviewButton.Click += new System.EventHandler(this.PreviewButton_Click);
            // 
            // SaveItemButton
            // 
            this.SaveItemButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.SaveItemButton.Image = global::App.Properties.Resources.OKIcon;
            this.SaveItemButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SaveItemButton.Name = "SaveItemButton";
            this.SaveItemButton.Size = new System.Drawing.Size(23, 22);
            this.SaveItemButton.Text = "Save Item";
            this.SaveItemButton.ToolTipText = "Sets the changes in the current Pokémon\'s object.";
            this.SaveItemButton.Click += new System.EventHandler(this.SaveItemButton_Click);
            // 
            // PokemonList
            // 
            this.PokemonList.DisplayMember = "Name";
            this.PokemonList.FormattingEnabled = true;
            this.PokemonList.Location = new System.Drawing.Point(0, 0);
            this.PokemonList.Name = "PokemonList";
            this.PokemonList.Size = new System.Drawing.Size(158, 394);
            this.PokemonList.TabIndex = 1;
            this.PokemonList.SelectedIndexChanged += new System.EventHandler(this.PokemonList_SelectedIndexChanged);
            this.PokemonList.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.PokemonList_Format);
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(11, 59);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(75, 13);
            this.NameLabel.TabIndex = 1;
            this.NameLabel.Text = "Display Name:";
            // 
            // InternalNameLabel
            // 
            this.InternalNameLabel.AutoSize = true;
            this.InternalNameLabel.Location = new System.Drawing.Point(10, 30);
            this.InternalNameLabel.Name = "InternalNameLabel";
            this.InternalNameLabel.Size = new System.Drawing.Size(76, 13);
            this.InternalNameLabel.TabIndex = 2;
            this.InternalNameLabel.Text = "Internal Name:";
            // 
            // Type1Label
            // 
            this.Type1Label.AutoSize = true;
            this.Type1Label.Location = new System.Drawing.Point(15, 83);
            this.Type1Label.Name = "Type1Label";
            this.Type1Label.Size = new System.Drawing.Size(71, 13);
            this.Type1Label.TabIndex = 3;
            this.Type1Label.Text = "Primary Type:";
            // 
            // Type2Label
            // 
            this.Type2Label.AutoSize = true;
            this.Type2Label.Location = new System.Drawing.Point(23, 134);
            this.Type2Label.Name = "Type2Label";
            this.Type2Label.Size = new System.Drawing.Size(59, 13);
            this.Type2Label.TabIndex = 4;
            this.Type2Label.Text = "Sec. Type:";
            // 
            // NameInput
            // 
            this.NameInput.Location = new System.Drawing.Point(98, 56);
            this.NameInput.MaxLength = 255;
            this.NameInput.Name = "NameInput";
            this.NameInput.Size = new System.Drawing.Size(100, 20);
            this.NameInput.TabIndex = 5;
            this.NameInput.TabStop = false;
            this.ToolTip.SetToolTip(this.NameInput, "This value is the Pokémon\'s name as-is.");
            // 
            // InternalNameInput
            // 
            this.InternalNameInput.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.InternalNameInput.Location = new System.Drawing.Point(98, 27);
            this.InternalNameInput.MaxLength = 255;
            this.InternalNameInput.Name = "InternalNameInput";
            this.InternalNameInput.Size = new System.Drawing.Size(100, 20);
            this.InternalNameInput.TabIndex = 6;
            this.InternalNameInput.TabStop = false;
            this.ToolTip.SetToolTip(this.InternalNameInput, "Uppercase, this is used as a constant.");
            // 
            // Type1Selector
            // 
            this.Type1Selector.FormattingEnabled = true;
            this.Type1Selector.Location = new System.Drawing.Point(98, 84);
            this.Type1Selector.Name = "Type1Selector";
            this.Type1Selector.Size = new System.Drawing.Size(100, 43);
            this.Type1Selector.TabIndex = 7;
            this.Type1Selector.TabStop = false;
            // 
            // Type2Selector
            // 
            this.Type2Selector.Enabled = false;
            this.Type2Selector.FormattingEnabled = true;
            this.Type2Selector.Location = new System.Drawing.Point(98, 134);
            this.Type2Selector.Name = "Type2Selector";
            this.Type2Selector.Size = new System.Drawing.Size(100, 43);
            this.Type2Selector.TabIndex = 8;
            this.Type2Selector.TabStop = false;
            // 
            // SecondaryType
            // 
            this.SecondaryType.AutoSize = true;
            this.SecondaryType.Location = new System.Drawing.Point(27, 160);
            this.SecondaryType.Name = "SecondaryType";
            this.SecondaryType.Size = new System.Drawing.Size(59, 17);
            this.SecondaryType.TabIndex = 9;
            this.SecondaryType.TabStop = false;
            this.SecondaryType.Text = "Enable";
            this.SecondaryType.UseVisualStyleBackColor = true;
            this.SecondaryType.CheckedChanged += new System.EventHandler(this.SecondaryType_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Main Ability:";
            // 
            // SecondaryAbilityLabel
            // 
            this.SecondaryAbilityLabel.AutoSize = true;
            this.SecondaryAbilityLabel.Location = new System.Drawing.Point(6, 56);
            this.SecondaryAbilityLabel.Name = "SecondaryAbilityLabel";
            this.SecondaryAbilityLabel.Size = new System.Drawing.Size(91, 13);
            this.SecondaryAbilityLabel.TabIndex = 12;
            this.SecondaryAbilityLabel.Text = "Secondary Ability:";
            // 
            // SecondaryAbility
            // 
            this.SecondaryAbility.AutoSize = true;
            this.SecondaryAbility.Location = new System.Drawing.Point(246, 54);
            this.SecondaryAbility.Name = "SecondaryAbility";
            this.SecondaryAbility.Size = new System.Drawing.Size(59, 17);
            this.SecondaryAbility.TabIndex = 14;
            this.SecondaryAbility.TabStop = false;
            this.SecondaryAbility.Text = "Enable";
            this.SecondaryAbility.UseVisualStyleBackColor = true;
            this.SecondaryAbility.CheckedChanged += new System.EventHandler(this.SecondaryAbility_CheckedChanged);
            // 
            // HiddenAbilityLabel
            // 
            this.HiddenAbilityLabel.AutoSize = true;
            this.HiddenAbilityLabel.Location = new System.Drawing.Point(23, 84);
            this.HiddenAbilityLabel.Name = "HiddenAbilityLabel";
            this.HiddenAbilityLabel.Size = new System.Drawing.Size(74, 13);
            this.HiddenAbilityLabel.TabIndex = 15;
            this.HiddenAbilityLabel.Text = "Hidden Ability:";
            // 
            // HiddenAbility
            // 
            this.HiddenAbility.AutoSize = true;
            this.HiddenAbility.Location = new System.Drawing.Point(246, 83);
            this.HiddenAbility.Name = "HiddenAbility";
            this.HiddenAbility.Size = new System.Drawing.Size(59, 17);
            this.HiddenAbility.TabIndex = 17;
            this.HiddenAbility.TabStop = false;
            this.HiddenAbility.Text = "Enable";
            this.HiddenAbility.UseVisualStyleBackColor = true;
            this.HiddenAbility.CheckedChanged += new System.EventHandler(this.HiddenAbility_CheckedChanged);
            // 
            // AbilitiesBox
            // 
            this.AbilitiesBox.Controls.Add(this.HiddenAbilityInput);
            this.AbilitiesBox.Controls.Add(this.SecondaryAbilityInput);
            this.AbilitiesBox.Controls.Add(this.MainAbilityInput);
            this.AbilitiesBox.Controls.Add(this.HiddenAbilityLabel);
            this.AbilitiesBox.Controls.Add(this.HiddenAbility);
            this.AbilitiesBox.Controls.Add(this.label1);
            this.AbilitiesBox.Controls.Add(this.SecondaryAbilityLabel);
            this.AbilitiesBox.Controls.Add(this.SecondaryAbility);
            this.AbilitiesBox.Location = new System.Drawing.Point(389, 6);
            this.AbilitiesBox.Name = "AbilitiesBox";
            this.AbilitiesBox.Size = new System.Drawing.Size(311, 117);
            this.AbilitiesBox.TabIndex = 18;
            this.AbilitiesBox.TabStop = false;
            this.AbilitiesBox.Text = "Abilities";
            // 
            // HiddenAbilityInput
            // 
            this.HiddenAbilityInput.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.HiddenAbilityInput.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.HiddenAbilityInput.Enabled = false;
            this.HiddenAbilityInput.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.HiddenAbilityInput.FormattingEnabled = true;
            this.HiddenAbilityInput.Location = new System.Drawing.Point(111, 81);
            this.HiddenAbilityInput.Name = "HiddenAbilityInput";
            this.HiddenAbilityInput.Size = new System.Drawing.Size(121, 21);
            this.HiddenAbilityInput.TabIndex = 20;
            this.HiddenAbilityInput.TabStop = false;
            // 
            // SecondaryAbilityInput
            // 
            this.SecondaryAbilityInput.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.SecondaryAbilityInput.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.SecondaryAbilityInput.Enabled = false;
            this.SecondaryAbilityInput.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.SecondaryAbilityInput.FormattingEnabled = true;
            this.SecondaryAbilityInput.Location = new System.Drawing.Point(111, 53);
            this.SecondaryAbilityInput.Name = "SecondaryAbilityInput";
            this.SecondaryAbilityInput.Size = new System.Drawing.Size(121, 21);
            this.SecondaryAbilityInput.TabIndex = 19;
            this.SecondaryAbilityInput.TabStop = false;
            // 
            // MainAbilityInput
            // 
            this.MainAbilityInput.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.MainAbilityInput.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.MainAbilityInput.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.MainAbilityInput.FormattingEnabled = true;
            this.MainAbilityInput.Location = new System.Drawing.Point(111, 26);
            this.MainAbilityInput.Name = "MainAbilityInput";
            this.MainAbilityInput.Size = new System.Drawing.Size(121, 21);
            this.MainAbilityInput.TabIndex = 18;
            this.MainAbilityInput.TabStop = false;
            // 
            // BasicBox
            // 
            this.BasicBox.Controls.Add(this.GenderRateInput);
            this.BasicBox.Controls.Add(this.GenderRateLabel);
            this.BasicBox.Controls.Add(this.Type2Selector);
            this.BasicBox.Controls.Add(this.NameLabel);
            this.BasicBox.Controls.Add(this.SecondaryType);
            this.BasicBox.Controls.Add(this.InternalNameLabel);
            this.BasicBox.Controls.Add(this.Type1Label);
            this.BasicBox.Controls.Add(this.Type1Selector);
            this.BasicBox.Controls.Add(this.Type2Label);
            this.BasicBox.Controls.Add(this.InternalNameInput);
            this.BasicBox.Controls.Add(this.NameInput);
            this.BasicBox.Location = new System.Drawing.Point(168, 6);
            this.BasicBox.Name = "BasicBox";
            this.BasicBox.Size = new System.Drawing.Size(215, 221);
            this.BasicBox.TabIndex = 19;
            this.BasicBox.TabStop = false;
            this.BasicBox.Text = "Basic";
            // 
            // GenderRateInput
            // 
            this.GenderRateInput.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.GenderRateInput.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.GenderRateInput.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.GenderRateInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GenderRateInput.FormattingEnabled = true;
            this.GenderRateInput.Location = new System.Drawing.Point(98, 183);
            this.GenderRateInput.Name = "GenderRateInput";
            this.GenderRateInput.Size = new System.Drawing.Size(111, 20);
            this.GenderRateInput.TabIndex = 11;
            this.GenderRateInput.TabStop = false;
            // 
            // GenderRateLabel
            // 
            this.GenderRateLabel.AutoSize = true;
            this.GenderRateLabel.Location = new System.Drawing.Point(11, 185);
            this.GenderRateLabel.Name = "GenderRateLabel";
            this.GenderRateLabel.Size = new System.Drawing.Size(71, 13);
            this.GenderRateLabel.TabIndex = 10;
            this.GenderRateLabel.Text = "Gender Rate:";
            // 
            // PokedexnEvoBox
            // 
            this.PokedexnEvoBox.Controls.Add(this.FormNamesButton);
            this.PokedexnEvoBox.Controls.Add(this.RegionalNumbersButton);
            this.PokedexnEvoBox.Controls.Add(this.KindInput);
            this.PokedexnEvoBox.Controls.Add(this.KindLabel);
            this.PokedexnEvoBox.Controls.Add(this.CompatibilitySelector);
            this.PokedexnEvoBox.Controls.Add(this.CompatibilityLabel);
            this.PokedexnEvoBox.Controls.Add(this.HabitatSelector);
            this.PokedexnEvoBox.Controls.Add(this.label6);
            this.PokedexnEvoBox.Controls.Add(this.PokedexEntryInput);
            this.PokedexnEvoBox.Controls.Add(this.PokedexEntryLabel);
            this.PokedexnEvoBox.Location = new System.Drawing.Point(168, 313);
            this.PokedexnEvoBox.Name = "PokedexnEvoBox";
            this.PokedexnEvoBox.Size = new System.Drawing.Size(644, 100);
            this.PokedexnEvoBox.TabIndex = 20;
            this.PokedexnEvoBox.TabStop = false;
            this.PokedexnEvoBox.Text = "Pokédex";
            // 
            // FormNamesButton
            // 
            this.FormNamesButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.FormNamesButton.Location = new System.Drawing.Point(159, 10);
            this.FormNamesButton.Name = "FormNamesButton";
            this.FormNamesButton.Size = new System.Drawing.Size(95, 23);
            this.FormNamesButton.TabIndex = 11;
            this.FormNamesButton.TabStop = false;
            this.FormNamesButton.Text = "Form Names";
            this.FormNamesButton.UseVisualStyleBackColor = true;
            this.FormNamesButton.Click += new System.EventHandler(this.FormNamesButton_Click);
            // 
            // RegionalNumbersButton
            // 
            this.RegionalNumbersButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.RegionalNumbersButton.Location = new System.Drawing.Point(253, 10);
            this.RegionalNumbersButton.Name = "RegionalNumbersButton";
            this.RegionalNumbersButton.Size = new System.Drawing.Size(103, 23);
            this.RegionalNumbersButton.TabIndex = 10;
            this.RegionalNumbersButton.TabStop = false;
            this.RegionalNumbersButton.Text = "Regional Numbers";
            this.RegionalNumbersButton.UseVisualStyleBackColor = true;
            this.RegionalNumbersButton.Click += new System.EventHandler(this.RegionalNumbersButton_Click);
            // 
            // KindInput
            // 
            this.KindInput.Location = new System.Drawing.Point(538, 12);
            this.KindInput.MaxLength = 13;
            this.KindInput.Name = "KindInput";
            this.KindInput.Size = new System.Drawing.Size(94, 20);
            this.KindInput.TabIndex = 7;
            this.KindInput.TabStop = false;
            // 
            // KindLabel
            // 
            this.KindLabel.AutoSize = true;
            this.KindLabel.Location = new System.Drawing.Point(501, 15);
            this.KindLabel.Name = "KindLabel";
            this.KindLabel.Size = new System.Drawing.Size(31, 13);
            this.KindLabel.TabIndex = 6;
            this.KindLabel.Text = "Kind:";
            // 
            // CompatibilitySelector
            // 
            this.CompatibilitySelector.FormattingEnabled = true;
            this.CompatibilitySelector.Location = new System.Drawing.Point(512, 51);
            this.CompatibilitySelector.Name = "CompatibilitySelector";
            this.CompatibilitySelector.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.CompatibilitySelector.Size = new System.Drawing.Size(120, 43);
            this.CompatibilitySelector.TabIndex = 5;
            this.CompatibilitySelector.TabStop = false;
            // 
            // CompatibilityLabel
            // 
            this.CompatibilityLabel.AutoSize = true;
            this.CompatibilityLabel.Location = new System.Drawing.Point(509, 35);
            this.CompatibilityLabel.Name = "CompatibilityLabel";
            this.CompatibilityLabel.Size = new System.Drawing.Size(68, 13);
            this.CompatibilityLabel.TabIndex = 4;
            this.CompatibilityLabel.Text = "Compatibility:";
            // 
            // HabitatSelector
            // 
            this.HabitatSelector.FormattingEnabled = true;
            this.HabitatSelector.Location = new System.Drawing.Point(376, 51);
            this.HabitatSelector.Name = "HabitatSelector";
            this.HabitatSelector.Size = new System.Drawing.Size(120, 43);
            this.HabitatSelector.TabIndex = 3;
            this.HabitatSelector.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(373, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Habitat:";
            // 
            // PokedexEntryInput
            // 
            this.PokedexEntryInput.Location = new System.Drawing.Point(17, 35);
            this.PokedexEntryInput.Multiline = true;
            this.PokedexEntryInput.Name = "PokedexEntryInput";
            this.PokedexEntryInput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.PokedexEntryInput.Size = new System.Drawing.Size(339, 59);
            this.PokedexEntryInput.TabIndex = 1;
            this.PokedexEntryInput.TabStop = false;
            // 
            // PokedexEntryLabel
            // 
            this.PokedexEntryLabel.AutoSize = true;
            this.PokedexEntryLabel.Location = new System.Drawing.Point(14, 19);
            this.PokedexEntryLabel.Name = "PokedexEntryLabel";
            this.PokedexEntryLabel.Size = new System.Drawing.Size(34, 13);
            this.PokedexEntryLabel.TabIndex = 0;
            this.PokedexEntryLabel.Text = "Entry:";
            // 
            // MovesBox
            // 
            this.MovesBox.Controls.Add(this.ManageEvolutions);
            this.MovesBox.Controls.Add(this.ManageItems);
            this.MovesBox.Controls.Add(this.ManageEggMoves);
            this.MovesBox.Controls.Add(this.ManageMoves);
            this.MovesBox.Location = new System.Drawing.Point(706, 6);
            this.MovesBox.Name = "MovesBox";
            this.MovesBox.Size = new System.Drawing.Size(106, 117);
            this.MovesBox.TabIndex = 22;
            this.MovesBox.TabStop = false;
            this.MovesBox.Text = "Tables";
            // 
            // ManageEvolutions
            // 
            this.ManageEvolutions.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ManageEvolutions.Location = new System.Drawing.Point(17, 85);
            this.ManageEvolutions.Name = "ManageEvolutions";
            this.ManageEvolutions.Size = new System.Drawing.Size(71, 23);
            this.ManageEvolutions.TabIndex = 8;
            this.ManageEvolutions.TabStop = false;
            this.ManageEvolutions.Text = "Evolutions";
            this.ManageEvolutions.UseVisualStyleBackColor = true;
            this.ManageEvolutions.Click += new System.EventHandler(this.ManageEvolutions_Click);
            // 
            // ManageItems
            // 
            this.ManageItems.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ManageItems.Location = new System.Drawing.Point(17, 63);
            this.ManageItems.Name = "ManageItems";
            this.ManageItems.Size = new System.Drawing.Size(71, 23);
            this.ManageItems.TabIndex = 2;
            this.ManageItems.TabStop = false;
            this.ManageItems.Text = "Items";
            this.ManageItems.UseVisualStyleBackColor = true;
            this.ManageItems.Click += new System.EventHandler(this.ManageItems_Click);
            // 
            // ManageEggMoves
            // 
            this.ManageEggMoves.AutoSize = true;
            this.ManageEggMoves.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ManageEggMoves.Location = new System.Drawing.Point(17, 41);
            this.ManageEggMoves.Name = "ManageEggMoves";
            this.ManageEggMoves.Size = new System.Drawing.Size(71, 23);
            this.ManageEggMoves.TabIndex = 1;
            this.ManageEggMoves.TabStop = false;
            this.ManageEggMoves.Text = "Egg Moves";
            this.ManageEggMoves.UseVisualStyleBackColor = true;
            this.ManageEggMoves.Click += new System.EventHandler(this.ManageEggMoves_Click);
            // 
            // ManageMoves
            // 
            this.ManageMoves.AutoSize = true;
            this.ManageMoves.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ManageMoves.Location = new System.Drawing.Point(17, 19);
            this.ManageMoves.Name = "ManageMoves";
            this.ManageMoves.Size = new System.Drawing.Size(71, 23);
            this.ManageMoves.TabIndex = 0;
            this.ManageMoves.TabStop = false;
            this.ManageMoves.Text = "Moves";
            this.ManageMoves.UseVisualStyleBackColor = true;
            this.ManageMoves.Click += new System.EventHandler(this.ManageMoves_Click);
            // 
            // OtherBox
            // 
            this.OtherBox.Controls.Add(this.GrowthRateInput);
            this.OtherBox.Controls.Add(this.HappinessInput);
            this.OtherBox.Controls.Add(this.StepsToHatchInput);
            this.OtherBox.Controls.Add(this.RarenessInput);
            this.OtherBox.Controls.Add(this.StepsToHatchLabel);
            this.OtherBox.Controls.Add(this.HappinessLabel);
            this.OtherBox.Controls.Add(this.RarenessLabel);
            this.OtherBox.Controls.Add(this.GrowthRateLabel);
            this.OtherBox.Location = new System.Drawing.Point(355, 233);
            this.OtherBox.Name = "OtherBox";
            this.OtherBox.Size = new System.Drawing.Size(383, 74);
            this.OtherBox.TabIndex = 23;
            this.OtherBox.TabStop = false;
            this.OtherBox.Text = "Other";
            // 
            // GrowthRateInput
            // 
            this.GrowthRateInput.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.GrowthRateInput.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.GrowthRateInput.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.GrowthRateInput.FormattingEnabled = true;
            this.GrowthRateInput.Location = new System.Drawing.Point(88, 20);
            this.GrowthRateInput.Name = "GrowthRateInput";
            this.GrowthRateInput.Size = new System.Drawing.Size(108, 21);
            this.GrowthRateInput.TabIndex = 10;
            this.GrowthRateInput.TabStop = false;
            // 
            // HappinessInput
            // 
            this.HappinessInput.Location = new System.Drawing.Point(286, 45);
            this.HappinessInput.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.HappinessInput.Name = "HappinessInput";
            this.HappinessInput.Size = new System.Drawing.Size(80, 20);
            this.HappinessInput.TabIndex = 9;
            this.HappinessInput.TabStop = false;
            // 
            // StepsToHatchInput
            // 
            this.StepsToHatchInput.Location = new System.Drawing.Point(286, 20);
            this.StepsToHatchInput.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.StepsToHatchInput.Name = "StepsToHatchInput";
            this.StepsToHatchInput.Size = new System.Drawing.Size(80, 20);
            this.StepsToHatchInput.TabIndex = 8;
            this.StepsToHatchInput.TabStop = false;
            // 
            // RarenessInput
            // 
            this.RarenessInput.Location = new System.Drawing.Point(89, 45);
            this.RarenessInput.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.RarenessInput.Name = "RarenessInput";
            this.RarenessInput.Size = new System.Drawing.Size(80, 20);
            this.RarenessInput.TabIndex = 7;
            this.RarenessInput.TabStop = false;
            // 
            // StepsToHatchLabel
            // 
            this.StepsToHatchLabel.AutoSize = true;
            this.StepsToHatchLabel.Location = new System.Drawing.Point(202, 23);
            this.StepsToHatchLabel.Name = "StepsToHatchLabel";
            this.StepsToHatchLabel.Size = new System.Drawing.Size(81, 13);
            this.StepsToHatchLabel.TabIndex = 3;
            this.StepsToHatchLabel.Text = "Steps to Hatch:";
            // 
            // HappinessLabel
            // 
            this.HappinessLabel.AutoSize = true;
            this.HappinessLabel.Location = new System.Drawing.Point(223, 47);
            this.HappinessLabel.Name = "HappinessLabel";
            this.HappinessLabel.Size = new System.Drawing.Size(60, 13);
            this.HappinessLabel.TabIndex = 2;
            this.HappinessLabel.Text = "Happiness:";
            // 
            // RarenessLabel
            // 
            this.RarenessLabel.AutoSize = true;
            this.RarenessLabel.Location = new System.Drawing.Point(27, 47);
            this.RarenessLabel.Name = "RarenessLabel";
            this.RarenessLabel.Size = new System.Drawing.Size(55, 13);
            this.RarenessLabel.TabIndex = 1;
            this.RarenessLabel.Text = "Rareness:";
            // 
            // GrowthRateLabel
            // 
            this.GrowthRateLabel.AutoSize = true;
            this.GrowthRateLabel.Location = new System.Drawing.Point(12, 23);
            this.GrowthRateLabel.Name = "GrowthRateLabel";
            this.GrowthRateLabel.Size = new System.Drawing.Size(70, 13);
            this.GrowthRateLabel.TabIndex = 0;
            this.GrowthRateLabel.Text = "Growth Rate:";
            // 
            // BattlePosBox
            // 
            this.BattlePosBox.Controls.Add(this.APosInput);
            this.BattlePosBox.Controls.Add(this.APosLabel);
            this.BattlePosBox.Controls.Add(this.YPosInput);
            this.BattlePosBox.Controls.Add(this.eYPosInput);
            this.BattlePosBox.Controls.Add(this.pYLabel);
            this.BattlePosBox.Controls.Add(this.eYLabel);
            this.BattlePosBox.Location = new System.Drawing.Point(168, 233);
            this.BattlePosBox.Name = "BattlePosBox";
            this.BattlePosBox.Size = new System.Drawing.Size(181, 74);
            this.BattlePosBox.TabIndex = 24;
            this.BattlePosBox.TabStop = false;
            this.BattlePosBox.Text = "Battle Positions";
            // 
            // APosInput
            // 
            this.APosInput.Location = new System.Drawing.Point(106, 23);
            this.APosInput.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.APosInput.Minimum = new decimal(new int[] {
            65535,
            0,
            0,
            -2147483648});
            this.APosInput.Name = "APosInput";
            this.APosInput.Size = new System.Drawing.Size(55, 20);
            this.APosInput.TabIndex = 5;
            this.APosInput.TabStop = false;
            this.ToolTip.SetToolTip(this.APosInput, "Altitude Value.");
            // 
            // APosLabel
            // 
            this.APosLabel.AutoSize = true;
            this.APosLabel.Location = new System.Drawing.Point(89, 25);
            this.APosLabel.Name = "APosLabel";
            this.APosLabel.Size = new System.Drawing.Size(17, 13);
            this.APosLabel.TabIndex = 4;
            this.APosLabel.Text = "A:";
            // 
            // YPosInput
            // 
            this.YPosInput.Location = new System.Drawing.Point(29, 47);
            this.YPosInput.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.YPosInput.Minimum = new decimal(new int[] {
            65535,
            0,
            0,
            -2147483648});
            this.YPosInput.Name = "YPosInput";
            this.YPosInput.Size = new System.Drawing.Size(59, 20);
            this.YPosInput.TabIndex = 3;
            this.YPosInput.TabStop = false;
            this.ToolTip.SetToolTip(this.YPosInput, "Player\'s Y Position.");
            // 
            // eYPosInput
            // 
            this.eYPosInput.Location = new System.Drawing.Point(29, 23);
            this.eYPosInput.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.eYPosInput.Minimum = new decimal(new int[] {
            65535,
            0,
            0,
            -2147483648});
            this.eYPosInput.Name = "eYPosInput";
            this.eYPosInput.Size = new System.Drawing.Size(59, 20);
            this.eYPosInput.TabIndex = 2;
            this.eYPosInput.TabStop = false;
            this.ToolTip.SetToolTip(this.eYPosInput, "Enemy\'s Y Position.");
            // 
            // pYLabel
            // 
            this.pYLabel.AutoSize = true;
            this.pYLabel.Location = new System.Drawing.Point(6, 47);
            this.pYLabel.Name = "pYLabel";
            this.pYLabel.Size = new System.Drawing.Size(23, 13);
            this.pYLabel.TabIndex = 1;
            this.pYLabel.Text = "pY:";
            // 
            // eYLabel
            // 
            this.eYLabel.AutoSize = true;
            this.eYLabel.Location = new System.Drawing.Point(6, 25);
            this.eYLabel.Name = "eYLabel";
            this.eYLabel.Size = new System.Drawing.Size(23, 13);
            this.eYLabel.TabIndex = 0;
            this.eYLabel.Text = "eY:";
            // 
            // IdentityBox
            // 
            this.IdentityBox.Controls.Add(this.ColorInput);
            this.IdentityBox.Controls.Add(this.WeightInput);
            this.IdentityBox.Controls.Add(this.HeightInput);
            this.IdentityBox.Controls.Add(this.WeightLabel);
            this.IdentityBox.Controls.Add(this.HeightLabel);
            this.IdentityBox.Controls.Add(this.ColorLabel);
            this.IdentityBox.Location = new System.Drawing.Point(390, 129);
            this.IdentityBox.Name = "IdentityBox";
            this.IdentityBox.Size = new System.Drawing.Size(181, 98);
            this.IdentityBox.TabIndex = 25;
            this.IdentityBox.TabStop = false;
            this.IdentityBox.Text = "Identity";
            // 
            // ColorInput
            // 
            this.ColorInput.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ColorInput.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ColorInput.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ColorInput.FormattingEnabled = true;
            this.ColorInput.Location = new System.Drawing.Point(61, 17);
            this.ColorInput.Name = "ColorInput";
            this.ColorInput.Size = new System.Drawing.Size(100, 21);
            this.ColorInput.TabIndex = 6;
            this.ColorInput.TabStop = false;
            // 
            // WeightInput
            // 
            this.WeightInput.DecimalPlaces = 2;
            this.WeightInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WeightInput.Location = new System.Drawing.Point(61, 66);
            this.WeightInput.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.WeightInput.Name = "WeightInput";
            this.WeightInput.Size = new System.Drawing.Size(100, 20);
            this.WeightInput.TabIndex = 5;
            this.WeightInput.TabStop = false;
            // 
            // HeightInput
            // 
            this.HeightInput.DecimalPlaces = 2;
            this.HeightInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HeightInput.Location = new System.Drawing.Point(61, 42);
            this.HeightInput.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.HeightInput.Name = "HeightInput";
            this.HeightInput.Size = new System.Drawing.Size(100, 20);
            this.HeightInput.TabIndex = 4;
            this.HeightInput.TabStop = false;
            // 
            // WeightLabel
            // 
            this.WeightLabel.AutoSize = true;
            this.WeightLabel.Location = new System.Drawing.Point(11, 68);
            this.WeightLabel.Name = "WeightLabel";
            this.WeightLabel.Size = new System.Drawing.Size(44, 13);
            this.WeightLabel.TabIndex = 2;
            this.WeightLabel.Text = "Weight:";
            // 
            // HeightLabel
            // 
            this.HeightLabel.AutoSize = true;
            this.HeightLabel.Location = new System.Drawing.Point(14, 44);
            this.HeightLabel.Name = "HeightLabel";
            this.HeightLabel.Size = new System.Drawing.Size(41, 13);
            this.HeightLabel.TabIndex = 1;
            this.HeightLabel.Text = "Height:";
            // 
            // ColorLabel
            // 
            this.ColorLabel.AutoSize = true;
            this.ColorLabel.Location = new System.Drawing.Point(21, 20);
            this.ColorLabel.Name = "ColorLabel";
            this.ColorLabel.Size = new System.Drawing.Size(34, 13);
            this.ColorLabel.TabIndex = 0;
            this.ColorLabel.Text = "Color:";
            // 
            // BaseValuesBox
            // 
            this.BaseValuesBox.Controls.Add(this.BaseStatsInput);
            this.BaseValuesBox.Controls.Add(this.EffortPointsInput);
            this.BaseValuesBox.Controls.Add(this.BaseExpInput);
            this.BaseValuesBox.Controls.Add(this.BaseExpLabel);
            this.BaseValuesBox.Controls.Add(this.BaseStatsLabel);
            this.BaseValuesBox.Controls.Add(this.EPLabel);
            this.BaseValuesBox.Location = new System.Drawing.Point(577, 132);
            this.BaseValuesBox.Name = "BaseValuesBox";
            this.BaseValuesBox.Size = new System.Drawing.Size(235, 95);
            this.BaseValuesBox.TabIndex = 26;
            this.BaseValuesBox.TabStop = false;
            this.BaseValuesBox.Text = "Base Values";
            // 
            // BaseStatsInput
            // 
            this.BaseStatsInput.Location = new System.Drawing.Point(87, 17);
            this.BaseStatsInput.Mask = "000,000,000,000,000,000";
            this.BaseStatsInput.Name = "BaseStatsInput";
            this.BaseStatsInput.Size = new System.Drawing.Size(136, 20);
            this.BaseStatsInput.TabIndex = 9;
            this.BaseStatsInput.TabStop = false;
            // 
            // EffortPointsInput
            // 
            this.EffortPointsInput.Location = new System.Drawing.Point(87, 41);
            this.EffortPointsInput.Mask = "0,0,0,0,0,0";
            this.EffortPointsInput.Name = "EffortPointsInput";
            this.EffortPointsInput.Size = new System.Drawing.Size(74, 20);
            this.EffortPointsInput.TabIndex = 8;
            this.EffortPointsInput.TabStop = false;
            // 
            // BaseExpInput
            // 
            this.BaseExpInput.Location = new System.Drawing.Point(87, 65);
            this.BaseExpInput.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.BaseExpInput.Name = "BaseExpInput";
            this.BaseExpInput.Size = new System.Drawing.Size(74, 20);
            this.BaseExpInput.TabIndex = 7;
            this.BaseExpInput.TabStop = false;
            // 
            // BaseExpLabel
            // 
            this.BaseExpLabel.AutoSize = true;
            this.BaseExpLabel.Location = new System.Drawing.Point(15, 67);
            this.BaseExpLabel.Name = "BaseExpLabel";
            this.BaseExpLabel.Size = new System.Drawing.Size(55, 13);
            this.BaseExpLabel.TabIndex = 5;
            this.BaseExpLabel.Text = "Base Exp:";
            // 
            // BaseStatsLabel
            // 
            this.BaseStatsLabel.AutoSize = true;
            this.BaseStatsLabel.Location = new System.Drawing.Point(15, 20);
            this.BaseStatsLabel.Name = "BaseStatsLabel";
            this.BaseStatsLabel.Size = new System.Drawing.Size(61, 13);
            this.BaseStatsLabel.TabIndex = 4;
            this.BaseStatsLabel.Text = "Base Stats:";
            // 
            // EPLabel
            // 
            this.EPLabel.AutoSize = true;
            this.EPLabel.Location = new System.Drawing.Point(14, 44);
            this.EPLabel.Name = "EPLabel";
            this.EPLabel.Size = new System.Drawing.Size(67, 13);
            this.EPLabel.TabIndex = 1;
            this.EPLabel.Text = "Effort Points:";
            // 
            // IconBox
            // 
            this.IconBox.Controls.Add(this.IconPicture);
            this.IconBox.Location = new System.Drawing.Point(744, 233);
            this.IconBox.Name = "IconBox";
            this.IconBox.Size = new System.Drawing.Size(68, 74);
            this.IconBox.TabIndex = 27;
            this.IconBox.TabStop = false;
            this.IconBox.Text = "Icon";
            // 
            // IconPicture
            // 
            this.IconPicture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.IconPicture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.IconPicture.Location = new System.Drawing.Point(3, 16);
            this.IconPicture.Name = "IconPicture";
            this.IconPicture.Size = new System.Drawing.Size(62, 55);
            this.IconPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.IconPicture.TabIndex = 0;
            this.IconPicture.TabStop = false;
            this.ToolTip.SetToolTip(this.IconPicture, "Change the Pokémon\'s Icon.");
            this.IconPicture.Click += new System.EventHandler(this.IconPicture_Click);
            // 
            // OpenIconFileDialog
            // 
            this.OpenIconFileDialog.Filter = "PNG Image|*.png";
            this.OpenIconFileDialog.RestoreDirectory = true;
            this.OpenIconFileDialog.Title = "Load Icon File";
            // 
            // SaveIconFileDialog
            // 
            this.SaveIconFileDialog.Filter = "PNG Image|*.png";
            this.SaveIconFileDialog.RestoreDirectory = true;
            // 
            // PokemonEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 419);
            this.Controls.Add(this.IconBox);
            this.Controls.Add(this.BaseValuesBox);
            this.Controls.Add(this.IdentityBox);
            this.Controls.Add(this.BattlePosBox);
            this.Controls.Add(this.OtherBox);
            this.Controls.Add(this.MovesBox);
            this.Controls.Add(this.PokedexnEvoBox);
            this.Controls.Add(this.BasicBox);
            this.Controls.Add(this.AbilitiesBox);
            this.Controls.Add(this.ListPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PokemonEditor";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Pokémon Editor";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.PokemonEditor_HelpButtonClicked);
            this.Load += new System.EventHandler(this.PokemonEditor_Load);
            this.ListPanel.ResumeLayout(false);
            this.ListPanel.PerformLayout();
            this.ListTools.ResumeLayout(false);
            this.ListTools.PerformLayout();
            this.AbilitiesBox.ResumeLayout(false);
            this.AbilitiesBox.PerformLayout();
            this.BasicBox.ResumeLayout(false);
            this.BasicBox.PerformLayout();
            this.PokedexnEvoBox.ResumeLayout(false);
            this.PokedexnEvoBox.PerformLayout();
            this.MovesBox.ResumeLayout(false);
            this.MovesBox.PerformLayout();
            this.OtherBox.ResumeLayout(false);
            this.OtherBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HappinessInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StepsToHatchInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RarenessInput)).EndInit();
            this.BattlePosBox.ResumeLayout(false);
            this.BattlePosBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.APosInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YPosInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eYPosInput)).EndInit();
            this.IdentityBox.ResumeLayout(false);
            this.IdentityBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WeightInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeightInput)).EndInit();
            this.BaseValuesBox.ResumeLayout(false);
            this.BaseValuesBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BaseExpInput)).EndInit();
            this.IconBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.IconPicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel ListPanel;
        private System.Windows.Forms.ToolStrip ListTools;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Label InternalNameLabel;
        private System.Windows.Forms.Label Type1Label;
        private System.Windows.Forms.Label Type2Label;
        private System.Windows.Forms.TextBox NameInput;
        private System.Windows.Forms.TextBox InternalNameInput;
        private System.Windows.Forms.ListBox Type1Selector;
        private System.Windows.Forms.ListBox Type2Selector;
        private System.Windows.Forms.CheckBox SecondaryType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label SecondaryAbilityLabel;
        private System.Windows.Forms.CheckBox SecondaryAbility;
        private System.Windows.Forms.Label HiddenAbilityLabel;
        private System.Windows.Forms.CheckBox HiddenAbility;
        private System.Windows.Forms.GroupBox AbilitiesBox;
        private System.Windows.Forms.GroupBox BasicBox;
        private System.Windows.Forms.ToolStripButton AddItemButton;
        private System.Windows.Forms.ToolStripButton DeleteItem;
        private System.Windows.Forms.ToolStripButton MoveUpButton;
        private System.Windows.Forms.ToolStripButton MoveDownButton;
        private System.Windows.Forms.ToolStripButton SaveItemButton;
        private System.Windows.Forms.GroupBox PokedexnEvoBox;
        private System.Windows.Forms.TextBox PokedexEntryInput;
        private System.Windows.Forms.Label PokedexEntryLabel;
        private System.Windows.Forms.GroupBox MovesBox;
        private System.Windows.Forms.GroupBox OtherBox;
        private System.Windows.Forms.Button ManageEggMoves;
        private System.Windows.Forms.Button ManageMoves;
        private System.Windows.Forms.GroupBox BattlePosBox;
        private System.Windows.Forms.NumericUpDown YPosInput;
        private System.Windows.Forms.NumericUpDown eYPosInput;
        private System.Windows.Forms.Label pYLabel;
        private System.Windows.Forms.Label eYLabel;
        private System.Windows.Forms.NumericUpDown APosInput;
        private System.Windows.Forms.Label APosLabel;
        private System.Windows.Forms.GroupBox IdentityBox;
        private System.Windows.Forms.Label GenderRateLabel;
        private System.Windows.Forms.Label WeightLabel;
        private System.Windows.Forms.Label HeightLabel;
        private System.Windows.Forms.Label ColorLabel;
        private System.Windows.Forms.NumericUpDown WeightInput;
        private System.Windows.Forms.NumericUpDown HeightInput;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox CompatibilitySelector;
        private System.Windows.Forms.Label CompatibilityLabel;
        private System.Windows.Forms.ListBox HabitatSelector;
        private System.Windows.Forms.Label EPLabel;
        private System.Windows.Forms.GroupBox BaseValuesBox;
        private System.Windows.Forms.Label BaseExpLabel;
        private System.Windows.Forms.Label BaseStatsLabel;
        private System.Windows.Forms.Label GrowthRateLabel;
        private System.Windows.Forms.Label HappinessLabel;
        private System.Windows.Forms.Label RarenessLabel;
        private System.Windows.Forms.Label StepsToHatchLabel;
        private System.Windows.Forms.Button ManageItems;
        private System.Windows.Forms.NumericUpDown HappinessInput;
        private System.Windows.Forms.NumericUpDown StepsToHatchInput;
        private System.Windows.Forms.NumericUpDown RarenessInput;
        private System.Windows.Forms.NumericUpDown BaseExpInput;
        private System.Windows.Forms.TextBox KindInput;
        private System.Windows.Forms.Label KindLabel;
        private System.Windows.Forms.MaskedTextBox BaseStatsInput;
        private System.Windows.Forms.MaskedTextBox EffortPointsInput;
        private System.Windows.Forms.ListBox PokemonList;
        private System.Windows.Forms.GroupBox IconBox;
        private System.Windows.Forms.PictureBox IconPicture;
        private System.Windows.Forms.Button ManageEvolutions;
        private System.Windows.Forms.Button RegionalNumbersButton;
        private System.Windows.Forms.ComboBox GenderRateInput;
        private System.Windows.Forms.Button FormNamesButton;
        private System.Windows.Forms.ComboBox GrowthRateInput;
        private System.Windows.Forms.ComboBox ColorInput;
        private System.Windows.Forms.ComboBox MainAbilityInput;
        private System.Windows.Forms.ComboBox SecondaryAbilityInput;
        private System.Windows.Forms.ComboBox HiddenAbilityInput;
        private System.Windows.Forms.ToolStripButton PreviewButton;
        private System.Windows.Forms.OpenFileDialog OpenIconFileDialog;
        private System.Windows.Forms.SaveFileDialog SaveIconFileDialog;
        private System.Windows.Forms.ToolTip ToolTip;
    }
}