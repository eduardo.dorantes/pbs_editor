﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Utilities;
using Utilities.Structures;

namespace App.Forms
{
    public partial class MovesEditor : Form
    {
        public MovesEditor()
        {
            InitializeComponent();
        }

        private void MovesEditor_Load(object sender, EventArgs e)
        {
            MovesList.DataSource = Global.Moves;
            MovesList.SelectedIndex = 0;
            var DamageCategories = Enum.GetValues(typeof(Damage)).Cast<Damage>().ToArray();
            TypeSelector.Items.AddRange(Global.Types.GetAll());
            DamageCategorySelector.Items.AddRange((from dm in DamageCategories select dm.ToString()).ToArray<string>());
        }

        private void MovesList_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = String.Format("{0:D3}", MovesList.Items.IndexOf(e.ListItem) + 1) + " - " + e.Value;
        }

        private void MovesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateFields();
        }

        private void UpdateFields()
        {
            if (MovesList.SelectedItem == null)
            {
                return;
            }
            
            Move m = (Move)MovesList.SelectedItem;
            m.Description = Regex.Replace(m.Description, "[\\\\]+\'", "'");
            m.Description = Regex.Replace(m.Description, "[\\\\]+\"", "\"");
            InternalNameInput.Text = m.InternalName;
            DisplayNameInput.Text = m.Name;
            FunctionCodeInput.Text = m.FunctionCode;
            BasePowerInput.Value = m.BasePower;
            TypeSelector.SelectedItem = m.Type.ToString();
            DamageCategorySelector.SelectedItem = m.Damage.ToString();
            AccuracyInput.Value = m.Accuracy;
            TotalPPInput.Value = m.TotalPP;
            AdditionalEffectInput.Value = m.AdditionalEffectChance;
            PriorityInput.Value = m.Priority;
            TargetSelector.SelectedIndex = Global.Target.IndexOf(m.Target);
            FlagsSelector.SelectedIndices.Clear();
            foreach (char flag in m.Flags)
            {
                int index = Global.Flags.IndexOf(flag);
                if(index >= 0)
                {
                    FlagsSelector.SelectedIndices.Add(index);
                }
            }
            DescriptionInput.Text = m.Description;
        }
    }
}
