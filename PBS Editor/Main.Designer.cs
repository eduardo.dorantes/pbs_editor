﻿namespace PBS_Editor
{
    partial class Main
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.OpenGameButton = new System.Windows.Forms.Button();
            this.GameNameLabel = new System.Windows.Forms.Label();
            this.ToolsPanel = new System.Windows.Forms.Panel();
            this.TrainerTypesButton = new System.Windows.Forms.Button();
            this.TrainersButton = new System.Windows.Forms.Button();
            this.MovesButton = new System.Windows.Forms.Button();
            this.AbilitiesButton = new System.Windows.Forms.Button();
            this.ItemsButton = new System.Windows.Forms.Button();
            this.PKMNButton = new System.Windows.Forms.Button();
            this.SavePBSFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.SaveButton = new System.Windows.Forms.Button();
            this.AboutButton = new System.Windows.Forms.Button();
            this.ToolsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // OpenFileDialog
            // 
            this.OpenFileDialog.Filter = "RMXP Project File|*.rxproj";
            this.OpenFileDialog.Title = "Select Project";
            // 
            // OpenGameButton
            // 
            this.OpenGameButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.OpenGameButton.Location = new System.Drawing.Point(233, 12);
            this.OpenGameButton.Name = "OpenGameButton";
            this.OpenGameButton.Size = new System.Drawing.Size(75, 23);
            this.OpenGameButton.TabIndex = 0;
            this.OpenGameButton.Text = "Open";
            this.OpenGameButton.UseVisualStyleBackColor = true;
            this.OpenGameButton.Click += new System.EventHandler(this.OpenGameButton_Click);
            // 
            // GameNameLabel
            // 
            this.GameNameLabel.AutoSize = true;
            this.GameNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GameNameLabel.Location = new System.Drawing.Point(25, 17);
            this.GameNameLabel.Name = "GameNameLabel";
            this.GameNameLabel.Size = new System.Drawing.Size(0, 13);
            this.GameNameLabel.TabIndex = 1;
            // 
            // ToolsPanel
            // 
            this.ToolsPanel.BackColor = System.Drawing.Color.White;
            this.ToolsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ToolsPanel.Controls.Add(this.TrainerTypesButton);
            this.ToolsPanel.Controls.Add(this.TrainersButton);
            this.ToolsPanel.Controls.Add(this.MovesButton);
            this.ToolsPanel.Controls.Add(this.AbilitiesButton);
            this.ToolsPanel.Controls.Add(this.ItemsButton);
            this.ToolsPanel.Controls.Add(this.PKMNButton);
            this.ToolsPanel.Enabled = false;
            this.ToolsPanel.Location = new System.Drawing.Point(15, 41);
            this.ToolsPanel.Name = "ToolsPanel";
            this.ToolsPanel.Size = new System.Drawing.Size(293, 208);
            this.ToolsPanel.TabIndex = 3;
            // 
            // TrainerTypesButton
            // 
            this.TrainerTypesButton.BackColor = System.Drawing.Color.White;
            this.TrainerTypesButton.Enabled = false;
            this.TrainerTypesButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TrainerTypesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TrainerTypesButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrainerTypesButton.Image = global::PBS_Editor.Properties.Resources.trainertypes;
            this.TrainerTypesButton.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.TrainerTypesButton.Location = new System.Drawing.Point(80, 112);
            this.TrainerTypesButton.Name = "TrainerTypesButton";
            this.TrainerTypesButton.Size = new System.Drawing.Size(62, 79);
            this.TrainerTypesButton.TabIndex = 5;
            this.TrainerTypesButton.Text = "T-Types";
            this.TrainerTypesButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.TrainerTypesButton.UseVisualStyleBackColor = true;
            // 
            // TrainersButton
            // 
            this.TrainersButton.BackColor = System.Drawing.Color.White;
            this.TrainersButton.Enabled = false;
            this.TrainersButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TrainersButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TrainersButton.Image = global::PBS_Editor.Properties.Resources.trainers;
            this.TrainersButton.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.TrainersButton.Location = new System.Drawing.Point(12, 112);
            this.TrainersButton.Name = "TrainersButton";
            this.TrainersButton.Size = new System.Drawing.Size(62, 79);
            this.TrainersButton.TabIndex = 4;
            this.TrainersButton.Text = "Trainers";
            this.TrainersButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.TrainersButton.UseVisualStyleBackColor = true;
            // 
            // MovesButton
            // 
            this.MovesButton.BackColor = System.Drawing.Color.White;
            this.MovesButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.MovesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MovesButton.Image = global::PBS_Editor.Properties.Resources.moves;
            this.MovesButton.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.MovesButton.Location = new System.Drawing.Point(216, 15);
            this.MovesButton.Name = "MovesButton";
            this.MovesButton.Size = new System.Drawing.Size(62, 79);
            this.MovesButton.TabIndex = 3;
            this.MovesButton.Text = "Moves";
            this.MovesButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.MovesButton.UseVisualStyleBackColor = true;
            this.MovesButton.Click += new System.EventHandler(this.MovesButton_Click);
            // 
            // AbilitiesButton
            // 
            this.AbilitiesButton.BackColor = System.Drawing.Color.White;
            this.AbilitiesButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.AbilitiesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AbilitiesButton.Image = global::PBS_Editor.Properties.Resources.abilities;
            this.AbilitiesButton.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.AbilitiesButton.Location = new System.Drawing.Point(148, 15);
            this.AbilitiesButton.Name = "AbilitiesButton";
            this.AbilitiesButton.Size = new System.Drawing.Size(62, 79);
            this.AbilitiesButton.TabIndex = 2;
            this.AbilitiesButton.Text = "Abilities";
            this.AbilitiesButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.AbilitiesButton.UseVisualStyleBackColor = true;
            this.AbilitiesButton.Click += new System.EventHandler(this.AbilitiesButton_Click);
            // 
            // ItemsButton
            // 
            this.ItemsButton.BackColor = System.Drawing.Color.White;
            this.ItemsButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ItemsButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ItemsButton.Image = global::PBS_Editor.Properties.Resources.items1;
            this.ItemsButton.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.ItemsButton.Location = new System.Drawing.Point(80, 15);
            this.ItemsButton.Name = "ItemsButton";
            this.ItemsButton.Size = new System.Drawing.Size(62, 79);
            this.ItemsButton.TabIndex = 1;
            this.ItemsButton.Text = "Items";
            this.ItemsButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ItemsButton.UseVisualStyleBackColor = true;
            this.ItemsButton.Click += new System.EventHandler(this.ItemsButton_Click);
            // 
            // PKMNButton
            // 
            this.PKMNButton.BackColor = System.Drawing.Color.White;
            this.PKMNButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.PKMNButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PKMNButton.Image = global::PBS_Editor.Properties.Resources.Pokemon;
            this.PKMNButton.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.PKMNButton.Location = new System.Drawing.Point(12, 15);
            this.PKMNButton.Name = "PKMNButton";
            this.PKMNButton.Size = new System.Drawing.Size(62, 79);
            this.PKMNButton.TabIndex = 0;
            this.PKMNButton.Text = "Pokémon";
            this.PKMNButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.PKMNButton.UseVisualStyleBackColor = true;
            this.PKMNButton.Click += new System.EventHandler(this.PKMNButton_Click);
            // 
            // SavePBSFileDialog
            // 
            this.SavePBSFileDialog.Filter = "PBS File|*.txt";
            this.SavePBSFileDialog.RestoreDirectory = true;
            this.SavePBSFileDialog.Title = "Save PBS File";
            // 
            // SaveButton
            // 
            this.SaveButton.Enabled = false;
            this.SaveButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.SaveButton.Location = new System.Drawing.Point(15, 255);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 4;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // AboutButton
            // 
            this.AboutButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.AboutButton.Location = new System.Drawing.Point(233, 255);
            this.AboutButton.Name = "AboutButton";
            this.AboutButton.Size = new System.Drawing.Size(75, 23);
            this.AboutButton.TabIndex = 5;
            this.AboutButton.Text = "About";
            this.AboutButton.UseVisualStyleBackColor = true;
            this.AboutButton.Click += new System.EventHandler(this.AboutButton_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 284);
            this.Controls.Add(this.ToolsPanel);
            this.Controls.Add(this.AboutButton);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.GameNameLabel);
            this.Controls.Add(this.OpenGameButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.Text = "PBS Editor";
            this.Load += new System.EventHandler(this.Main_Load);
            this.ToolsPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog OpenFileDialog;
        private System.Windows.Forms.Button OpenGameButton;
        private System.Windows.Forms.Label GameNameLabel;
        private System.Windows.Forms.Panel ToolsPanel;
        private System.Windows.Forms.Button PKMNButton;
        private System.Windows.Forms.Button ItemsButton;
        private System.Windows.Forms.SaveFileDialog SavePBSFileDialog;
        private System.Windows.Forms.Button AbilitiesButton;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button AboutButton;
        private System.Windows.Forms.Button MovesButton;
        private System.Windows.Forms.Button TrainerTypesButton;
        private System.Windows.Forms.Button TrainersButton;
    }
}

