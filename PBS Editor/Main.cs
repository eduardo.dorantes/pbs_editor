﻿using App;
using App.Forms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Utilities;
using Utilities.Structures;

namespace PBS_Editor
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
#if DEBUG
            this.Text = "PBS Editor ( DEBUG )";
#endif
        }

        private void OpenGameButton_Click(object sender, EventArgs e)
        {
            if(OpenFileDialog.ShowDialog() == DialogResult.OK)
            {

                Cursor.Current = Cursors.WaitCursor;
                Global.FullFileName = OpenFileDialog.FileName;
                Global.Directory = Path.GetDirectoryName(Global.FullFileName);
                Global.FileName = Path.GetFileName(Global.FullFileName);
                byte[] bytes = Encoding.Default.GetBytes(Helpers.GetGameName());
                string GameName = Encoding.UTF8.GetString(bytes);
                ItemsButton.Enabled = true;
                PKMNButton.Enabled = true;
                AbilitiesButton.Enabled = true;
                MovesButton.Enabled = true;
                SaveButton.Enabled = false;
                if (!Directory.Exists(Global.Directory + "/" + "PBS"))
                {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show("The PBS folder does not exists!");
                    ToolsPanel.Enabled = false;
                    return;
                }

                if (!Helpers.CheckAllFiles())
                {
                    MessageBox.Show(@"I cannot find some of the required PBS files.
The needed files are:
            abilities.txt,
            items.txt,
            moves.txt,
            pokemon.txt,
            trainers.txt,
            trainertypes.txt,
            types.txt");
                    Cursor.Current = Cursors.Default;
                    ToolsPanel.Enabled = false;
                    return;
                }
                bool update = true;
                Global.InitializeTypes();
                Global.GetVersion();
                if (!Global.v16Detected)
                {
                    if (MessageBox.Show(
                        "I have detected a non-updated version of Essentials, would you like to upgrade the format to the latest one?", "Confirmation",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        update = true;
                    }
                    else
                    {
                        update = false;
                    }
                }
                try
                {
                    Global.InitializePokemon(update);
                }
                catch(Exception ex)
                {
                    ToolsPanel.Enabled = false;
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show("The pokemon.txt has an unexpected format.");
#if DEBUG
                    MessageBox.Show(ex.ToString());
#endif
                    PKMNButton.Enabled = false;
                }
                try
                {
                    Global.InitializeItems(update);
                }
                catch(Exception ex)
                {
                    ToolsPanel.Enabled = false;
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show("The items.txt has an unexpected format.");
#if DEBUG
                    MessageBox.Show(ex.ToString());
#endif
                    ItemsButton.Enabled = false;
                }
                try
                {
                    Global.InitializeAbilities();
                }
                catch (Exception ex)
                {
                    ToolsPanel.Enabled = false;
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show("The abilities.txt has an unexpected format.");
#if DEBUG
                    MessageBox.Show(ex.ToString());
#endif
                    AbilitiesButton.Enabled = false;
                }
#if DEBUG
                try
                {
                    Global.InitializeMoves();
                }
                catch (Exception ex)
                {
                    ToolsPanel.Enabled = false;
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show("The moves.txt has an unexpected format.");
#if DEBUG
                    MessageBox.Show(ex.ToString());
#endif
                    MovesButton.Enabled = false;
                }
#else
                MovesButton.Enabled = false;
#endif
                Global.v16Detected = update;
                GameNameLabel.Text = GameName;
                string sufix = Global.v16Detected ? "(v16+)" : "";
                this.Text = "PBS Editor " + sufix;
                ToolsPanel.Enabled = true;
                SaveButton.Enabled = true;
            }
        }

        private void PKMNButton_Click(object sender, EventArgs e)
        {
            PokemonEditor form = new PokemonEditor();
            form.ShowDialog();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            SavePBSFileDialog.InitialDirectory = Global.Directory;
        }

        private void ItemsButton_Click(object sender, EventArgs e)
        {
            ItemEditor ited_form = new ItemEditor();
            ited_form.ShowDialog();
        }

        private void AbilitiesButton_Click(object sender, EventArgs e)
        {
            AbilitiesEditor abi_form = new AbilitiesEditor();
            abi_form.ShowDialog();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(
"Do you want to save the changes?", "Confirmation",
MessageBoxButtons.YesNo, MessageBoxIcon.Question,
MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
            {

                List<string> contents = new List<string>();
                Cursor.Current = Cursors.WaitCursor;
                int count;

                if (PKMNButton.Enabled)
                {
                    count = 1;
                    foreach (Pokemon pokemon in Global.Pokemon)
                    {
                        contents.Add(String.Format("[{0}]", count));
                        foreach (string line in pokemon.ToArray())
                        {
                            contents.Add(line);
                        }
                        ++count;
                    }
                    if (File.Exists(Global.Directory + "/PBS/pokemon.txt"))
                    {
                        File.Delete(Global.Directory + "/PBS/pokemon.txt");
                    }
                    File.WriteAllLines(Global.Directory + "/PBS/pokemon.txt", contents);
                }

                if (ItemsButton.Enabled)
                {
                    contents.Clear();
                    count = 1;
                    foreach (Item item in Global.Items)
                    {
                        contents.Add(count + "," + item);
                        ++count;
                    }
                    if (File.Exists(Global.Directory + "/PBS/items.txt"))
                    {
                        File.Delete(Global.Directory + "/PBS/items.txt");
                    }
                    File.WriteAllLines(Global.Directory + "/PBS/items.txt", contents);
                }

                if (AbilitiesButton.Enabled)
                {
                    contents.Clear();
                    count = 1;
                    foreach (Ability ability in Global.Abilities)
                    {
                        contents.Add(count + "," + ability);
                        ++count;
                    }
                    if (File.Exists(Global.Directory + "/PBS/abilities.txt"))
                    {
                        File.Delete(Global.Directory + "/PBS/abilities.txt");
                    }
                    File.WriteAllLines(Global.Directory + "/PBS/abilities.txt", contents);
                    contents.Clear();

                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void AboutButton_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.ShowDialog();
        }

        private void MovesButton_Click(object sender, EventArgs e)
        {
            MovesEditor moves_dialog = new MovesEditor();
            moves_dialog.ShowDialog();
        }
    }
}
