﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Utilities
{
    namespace Structures
    {
        /// <summary>
        /// Pokémon Types Class, used as a structure.
        /// </summary>
        public class Types
        {
            private List<string> List = new List<string>();
            public string this[int index]
            {
                get
                {
                    return List[index];
                }
            }

            public int Length()
            {
                return List.Count;
            }

            public string[] GetAll()
            {
                return List.ToArray();
            }

            public Types()
            {
                IEnumerable<string> lines = File.ReadAllLines(Global.Directory + "/PBS/types.txt");
                foreach(string line in lines)
                {
                    if(Regex.IsMatch(line, "^InternalName="))
                    {
                        List.Add(line.Split('=')[1]);
                    }
                }
            }
        }

        /// <summary>
        /// Habitat Enumerator.
        /// </summary>
        public enum Habitat
        {
            Unknown,
            Cave,
            Forest,
            Grassland,
            Mountain,
            Rare,
            RoughTerrain,
            Sea,
            Urban,
            WatersEdge
        }

        /// <summary>
        /// Color Enumerator.
        /// </summary>
        public enum Color
        {
            Black,
            Blue,
            Brown,
            Gray,
            Green,
            Pink,
            Purple,
            Red,
            White,
            Yellow
        }

        /// <summary>
        /// Egg Groups in Pokémon Essentials v15
        /// </summary>
        public enum EggGroupsv15
        {
            Undefined,
            Monster,
            Water1,
            Bug,
            Flying,
            Ground,
            Fairy,
            Plant,
            Humanshape,
            Water3,
            Mineral,
            Indeterminate,
            Water2,
            Ditto,
            Dragon,
            NoEggs
        }

        /// <summary>
        /// Egg Groups in Pokémon Essentials v16
        /// </summary>
        public enum EggGroupsv16
        {
            Monster,
            Water1,
            Bug,
            Flying,
            Field,
            Fairy,
            Grass,
            Humanlike,
            Water3,
            Mineral,
            Amorphous,
            Water2,
            Ditto,
            Dragon,
            Undiscovered
        }

        /// <summary>
        /// Pokémon's Growth Rate Enumerator.
        /// </summary>
        public enum GrowthRate
        {
            Fast,
            Medium,
            Slow,
            Parabolic,
            Erratic,
            Fluctuating
        }

        /// <summary>
        /// Pokémon's Gender Rate Enumerator.
        /// </summary>
        public enum GenderRate
        {
            AlwaysMale,
            FemaleOneEighth,
            Female25Percent,
            Female50Percent,
            Female75Percent,
            FemaleSevenEighths,
            AlwaysFemale,
            Genderless
        }

        /// <summary>
        /// Pokémon's Move Structure.
        /// </summary>
        public struct MoveField
        {
            public byte Level { get; set; }
            public string Name { get; set; }

            public MoveField(byte lvl, string name)
            {
                this.Level = lvl;
                this.Name = name;
            }

            public override string ToString()
            {
                return "" + this.Level + "," + this.Name;
            }
        }

        /// <summary>
        /// Pokémon's Evolution Method.
        /// </summary>
        public enum EvolutionMethods
        {
            Unknown,
            Happiness,
            HappinessDay,
            HappinessNight,
            Level,
            Trade,
            TradeItem,
            Item,
            AttackGreater,
            AtkDefEqual,
            DefenseGreater,
            Silcoon,
            Cascoon,
            Ninjask,
            Shedinja,
            Beauty,
            ItemMale,
            ItemFemale,
            DayHoldItem,
            NightHoldItem,
            HasMove,
            HasInParty,
            LevelMale,
            LevelFemale,
            Location,
            TradeSpecies,
            LevelDay,
            LevelNight,
            LevelDarkInParty,
            LevelRain,
            HappinessMoveType,
            Custom1,
            Custom2,
            Custom3,
            Custom4,
            Custom5,
            Custom6,
            Custom7
        }

        /// <summary>
        /// Pokémon's Evolutions Structure.
        /// </summary>
        public struct Evolutions
        {
            public string Pokemon;
            public EvolutionMethods Method;
            public string Value;

            public Evolutions(string pkmn, EvolutionMethods method, string value)
            {
                this.Pokemon = pkmn;
                this.Method = method;
                this.Value = value;
            }

            public override string ToString()
            {
                return "" + this.Pokemon + "," + this.Method + "," + this.Value;
            }
        }

        /// <summary>
        /// Moves's Damage Category.
        /// </summary>
        public enum Damage
        {
            Physical,
            Special,
            Status
        }

        /// <summary>
        /// Targets Indexer.
        /// </summary>
        public class Target
        {
            private int[] targets = new int[13] {0,1,2,4,8,10,20,40,80,100,200,400,800};

            public int this[int index]
            {
                get
                {
                    return targets[index];
                }
            }

            public int IndexOf(int value)
            {
                return Array.IndexOf(targets, value);
            }

        }

        /// <summary>
        /// Flags Indexer.
        /// </summary>
        public class Flags
        {
            private char[] flags = new char[14] {'a','b','c','d','e','f','g','h','i','j','k','l','m','n'};

            public int this[int index]
            {
                get
                {
                    return flags[index];
                }
            }

            public int IndexOf(char value)
            {
                return Array.IndexOf(flags, value);
            }

        }

        /// <summary>
        /// Gender Enumerator.
        /// </summary>
        public enum Gender
        {
            Male, Female, Mixed
        }

        /// <summary>
        /// Skill Codes Enumerator.
        /// </summary>
        public enum SkillCodes
        {

        }

        /// <summary>
        /// Natures Enumerator.
        /// </summary>
        public enum Natures
        {
            HARDY,
            LONELY,
            BRAVE,
            ADAMANT,
            NAUGHTY,
            BOLD,
            DOCILE,
            RELAXED,
            IMPISH,
            LAX,
            TIMID,
            HASTY,
            SERIOUS,
            JOLLY,
            NAIVE,
            MODEST,
            MILD,
            QUIET,
            BASHFUL,
            RASH,
            CALM,
            GENTLE,
            SASSY,
            CAREFUL,
            QUIRKY
        }

        /// <summary>
        /// Pokémon Structure.
        /// </summary>
        public struct Pokemon
        {
            static private int unique_id { get; set; }
            private int unique_identifier { get; set; }
            public string Name { get; set; }
            public string InternalName { get; set; }
            public string Type1 { get; set; }
            public string Type2 { get; set; }
            public byte[] BaseStats { get; set; }
            public GenderRate GenderRate { get; set; }
            public GrowthRate GrowthRate { get; set; }
            public ushort BaseExp { get; set; }
            public byte[] EffortPoints { get; set; }
            public byte Rareness { get; set; }
            public byte Happiness { get; set; }
            public List<MoveField> Moves { get; set; }
            public List<EggGroupsv16> Compatibilityv16 { get; set; }
            public List<EggGroupsv15> Compatibilityv15 { get; set; }
            public ushort StepsToHatch { get; set; }
            public decimal Height { get; set; }
            public decimal Weight { get; set; }
            public Color Color { get; set; }
            public string Kind { get; set; }
            public string Pokedex { get; set; }
            public List<Evolutions> Evolutions { get; set; }
            public string[] Abilities { get; set; }
            public string HiddenAbility { get; set; }
            public List<string> EggMoves { get; set; }
            public Habitat Habitat { get; set; }
            public List<ushort> RegionalNumbers { get; set; }
            public string WildItemCommon { get; set; }
            public string WildItemUncommon { get; set; }
            public string WildItemRare { get; set; }
            public byte BattlerEnemyY { get; set; }
            public byte BattlerPlayerY { get; set; }
            public byte BattlerAltitude { get; set; }
            public string[] FormNames { get; set; }
            public string Incense { get; set; }

            public Pokemon(bool someboolean = true)
            {
                ++Pokemon.unique_id;
                this.unique_identifier = Pokemon.unique_id;
                this.Name = "Unknown";
                this.InternalName = "UNKNOWN";
                this.Type1 = "NORMAL";
                this.Type2 = null;
                this.BaseStats = new byte[6] { 0, 0, 0, 0, 0, 0 };
                this.GenderRate = GenderRate.Genderless;
                this.GrowthRate = GrowthRate.Fast;
                this.BaseExp = 0;
                this.EffortPoints = new byte[6] { 0, 0, 0, 0, 0, 0 };
                this.Rareness = 0;
                this.Happiness = 0;
                this.Moves = new List<MoveField>() {new MoveField(1, "TACKLE")};
                this.Compatibilityv16 = new List<EggGroupsv16>();
                this.Compatibilityv15 = new List<EggGroupsv15>();
                this.StepsToHatch = 0;
                this.Height = 0.1m;
                this.Weight = 0.1m;
                this.Color = Color.Black;
                this.Kind = "Unknown";
                this.Pokedex = "No Data";
                this.Evolutions = new List<Evolutions>();
                this.Abilities = new string[1] {""};
                this.HiddenAbility = null;
                this.EggMoves = new List<string>();
                this.Habitat = Habitat.Unknown;
                this.RegionalNumbers = new List<ushort>();
                this.WildItemCommon = "";
                this.WildItemUncommon = "";
                this.WildItemRare = "";
                this.BattlerEnemyY = 0;
                this.BattlerPlayerY = 0;
                this.BattlerAltitude = 0;
                this.FormNames = new string[0];
                this.Incense = "";
            }

            public override string ToString()
            {
                return String.Join("\n", this.ToArray());
            }

            public string[] ToArray()
            {
                List<string> result = new List<string>();
                result.Add(String.Format("Name={0}", this.Name));
                result.Add(String.Format("InternalName={0}", this.InternalName));
                result.Add(String.Format("Type1={0}", this.Type1));
                if (this.Type2 != null)
                {
                    result.Add(String.Format("Type2={0}", this.Type2));
                }
                result.Add(String.Format("BaseStats={0}", String.Join(",", this.BaseStats.Cast<byte>())));
                result.Add(String.Format("GenderRate={0}", this.GenderRate));
                result.Add(String.Format("GrowthRate={0}", this.GrowthRate));
                result.Add(String.Format("BaseEXP={0}", this.BaseExp));
                result.Add(String.Format("EffortPoints={0}", String.Join(",", this.EffortPoints.Cast<byte>())));
                result.Add(String.Format("Rareness={0}", this.Rareness));
                result.Add(String.Format("Happiness={0}", this.Happiness));
                result.Add(String.Format("Abilities={0}", String.Join(",", this.Abilities)));
                if (this.HiddenAbility != null)
                {
                    result.Add(String.Format("HiddenAbility={0}", this.HiddenAbility));
                }
                List<string> moves = new List<string>();
                foreach (MoveField move in this.Moves)
                {
                    moves.Add(move.ToString());
                }
                result.Add(String.Format("Moves={0}", String.Join(",", moves.ToArray())));
                if (this.EggMoves != null && this.EggMoves.Count > 0)
                {
                    result.Add(String.Format("EggMoves={0}", String.Join(",", this.EggMoves.ToArray())));
                }
                if (Global.v16Detected)
                {
                    result.Add(String.Format("Compatibility={0}", String.Join(",", this.Compatibilityv16)));
                }
                else
                {
                    result.Add(String.Format("Compatibility={0}", String.Join(",", this.Compatibilityv15.Cast<int>())));
                }
                result.Add(String.Format("StepsToHatch={0}", this.StepsToHatch));
                result.Add(String.Format("Height={0}", this.Height.ToString().Replace(",",".")));
                result.Add(String.Format("Weight={0}", this.Weight.ToString().Replace(",", ".")));
                result.Add(String.Format("Color={0}", this.Color));
                if (this.Habitat != Habitat.Unknown)
                {
                    result.Add(String.Format("Habitat={0}", this.Habitat));
                }
                result.Add(String.Format("Kind={0}", this.Kind));
                result.Add(String.Format("Pokedex={0}", Regex.Replace(this.Pokedex, "[\"]+", "\\\"")));
                if (this.RegionalNumbers != null)
                {
                    result.Add(String.Format("RegionalNumbers={0}", String.Join(",", this.RegionalNumbers)));
                }
                if (this.WildItemCommon != null && !String.IsNullOrWhiteSpace(this.WildItemCommon))
                {
                    result.Add(String.Format("WildItemCommon={0}", this.WildItemCommon));
                }
                if (this.WildItemUncommon != null && !String.IsNullOrWhiteSpace(this.WildItemUncommon))
                {
                    result.Add(String.Format("WildItemUncommon={0}", this.WildItemUncommon));
                }
                if (this.WildItemRare != null && !String.IsNullOrWhiteSpace(this.WildItemRare))
                {
                    result.Add(String.Format("WildItemRare={0}", this.WildItemRare));
                }
                result.Add(String.Format("BattlerPlayerY={0}", this.BattlerPlayerY));
                result.Add(String.Format("BattlerEnemy={0}", this.BattlerEnemyY));
                result.Add(String.Format("BattlerAltitude={0}", this.BattlerAltitude));
                if (this.Evolutions != null && this.Evolutions.Count > 0)
                {
                    List<string> evolutions = new List<string>();
                    foreach (Evolutions evo in this.Evolutions)
                    {
                        evolutions.Add(evo.ToString());
                    }
                    result.Add(String.Format("Evolutions={0}", String.Join(",", evolutions.ToArray())));
                }
                if (this.FormNames != null && this.FormNames.Length > 0)
                {
                    result.Add(String.Format("FormNames={0}", String.Join(",", this.FormNames)));
                }
                if (this.Incense != null && !String.IsNullOrWhiteSpace(this.Incense))
                {
                    result.Add(String.Format("Incense={0}", this.Incense));
                }
                return result.ToArray();
            }
        }

        /// <summary>
        /// Item Structure.
        /// </summary>
        public struct Item
        {
            static private int unique_id { get; set; }
            private int unique_identifier { get; set; }
            public string InternalName { get; set; }
            public string Name { get; set; }
            public string PluralName { get; set; }
            public byte Pocket { get; set; }
            public int Price { get; set; }
            public string Description { get; set; }
            public byte UsabilityOutOfBattle { get; set; }
            public byte UsabilityInBattle { get; set; }
            public byte SpecialItems { get; set; }
            public string Move { get; set; }

            public Item(bool somebool)
            {
                ++Item.unique_id;
                this.unique_identifier = Item.unique_id;
                this.InternalName = null;
                this.Name = null;
                this.PluralName = null;
                this.Pocket = 1;
                this.Price = 0;
                this.Description = null;
                this.UsabilityOutOfBattle = 0;
                this.UsabilityInBattle = 0;
                this.SpecialItems = 0;
                this.Move = null;
            }

            public override string ToString()
            {
                string result = "";
                result += this.InternalName + ",";
                result += this.Name + ",";
                if (Global.v16Detected)
                {
                    result += this.PluralName + ",";
                }
                result += this.Pocket + ",";
                result += this.Price + ",";
                result += "\"" + this.Description.Replace("\"", "\\\"").Replace("\'", "\\\'") + "\",";
                result += this.UsabilityOutOfBattle + ",";
                result += this.UsabilityInBattle + ",";
                result += this.SpecialItems + ",";
                if(this.Pocket == 4)
                {
                    result += this.Move;
                }
                return result;
            }
        }

        /// <summary>
        /// Ability Structure.
        /// </summary>
        public struct Ability
        {
            public string InternalName { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }

            public Ability(string i, string n, string d)
            {
                this.InternalName = i;
                this.Name = n;
                this.Description = d;
            }

            public override string ToString()
            {
                return "" + this.InternalName + "," + this.Name + "," + "\"" + this.Description.Replace("\'", "\'").Replace("\"", "\"") + "\"";
            }
        }

        /// <summary>
        /// Move Structure
        /// </summary>
        public struct Move
        {
            static private int unique_id { get; set; }
            private int unique_identifier { get; set; }
            public string InternalName { get; set; }
            public string Name { get; set; }
            public string FunctionCode { get; set; }
            public int BasePower { get; set; }
            public string Type { get; set; }
            public Damage Damage { get; set; }
            public int Accuracy { get; set; }
            public int TotalPP { get; set; }
            public int AdditionalEffectChance { get; set; }
            public int Target { get; set; }
            public int Priority { get; set; } //-6 to 6
            public char[] Flags { get; set; }
            public string Description { get; set; }

            public override string ToString()
            {
                return base.ToString();
            }

            public Move(bool somebool = true)
            {
                ++unique_id;
                this.unique_identifier = unique_id;
                this.InternalName = null;
                this.Name = null;
                this.FunctionCode = null;
                this.BasePower = 0;
                this.Type = null;
                this.Damage = Damage.Physical;
                this.Accuracy = 0;
                this.TotalPP = 0;
                this.AdditionalEffectChance = 0;
                this.Target = 0;
                this.Priority = 0;
                this.Flags = new char[0];
                this.Description = null;
            }
        }

        /// <summary>
        /// Trainer's Type Structure.
        /// </summary>
        public struct TrainerType
        {
            public string InternalName { get; set; }
            public string Name { get; set; }
            public int BaseMoney { get; set; } //255
            public string BattleBGM { get; set; }
            public string VictoryBGM { get; set; }
            public string IntroME { get; set; }
            public Gender Gender { get; set; }
            public int SkillLevel { get; set; }
            public SkillCodes SkillCodes { get; set; }

            public override string ToString()
            {
                return base.ToString();
            }
        }

        /// <summary>
        /// Trainer's Pokémon Structure.
        /// </summary>
        public struct TrainerPokemon
        {
            public string Species { get; set; }
            public byte Level { get; set; }
            public string HeldItem { get; set; }
            public string[] Moves { get; set; }
            public byte Ability { get; set; }
            public char Gender { get; set; }
            public byte Form { get; set; }
            public bool Shiny { get; set; }
            public Natures Nature { get; set; }
            public byte[] IVs { get; set; }
            public byte Happiness { get; set; }
            public string Nickname { get; set; }
            public byte BallType { get; set; }

            public override string ToString()
            {
                return base.ToString();
            }
        }

        /// <summary>
        /// Trainer's Structure.
        /// </summary>
        public struct Trainer
        {
            public string TrainerType { get; set; }
            public string Name { get; set; }
            public ushort TrainerID { get; set; }
            public byte PokemonCount { get; set; }
            public TrainerPokemon[] Pokemon { get; set; }

            public override string ToString()
            {
                return base.ToString();
            }
        }
    }
}
