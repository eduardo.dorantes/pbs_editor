﻿using System.ComponentModel;
using System.IO;
using System.Text.RegularExpressions;
using Utilities.Structures;

namespace Utilities
{
    static public class Global
    {
        static public Flags Flags = new Flags();
        static public Target Target = new Target();
        static public BindingList<Pokemon> Pokemon { get; set; }
        static public BindingList<Item> Items { get; set; }
        static public string FullFileName { get; set; }
        static public string Directory { get; set; }
        static public string FileName { get; set; }
        static public Types Types { get; set; }
        static public BindingList<Ability> Abilities { get; set; }
        static public BindingList<Move> Moves { get; set; }
        static public bool v16Detected { get; set; }

        /// <summary>
        /// Loads all types.
        /// </summary>
        /// <returns></returns>
        static public bool InitializeTypes()
        {
            try
            {
                Types = new Types();
                return true;
            }
            catch
            {
                return false;
            }
            
        }

        /// <summary>
        /// Loads all Pokémon.
        /// </summary>
        /// <param name="update"></param>
        static public void InitializePokemon(bool update = false)
        {
            Pokemon = Helpers.LoadAllPokemon(update);
        }

        /// <summary>
        /// Loads all items.
        /// </summary>
        /// <param name="update"></param>
        static public void InitializeItems(bool update = false)
        {
            Items = Helpers.LoadAllItems(update);
        }

        /// <summary>
        /// Loads all abilities.
        /// </summary>
        /// <returns></returns>
        static public void InitializeAbilities()
        {
            Abilities = Helpers.LoadAllAbilities();
        }

        /// <summary>
        /// Loads all moves.
        /// </summary>
        static public void InitializeMoves()
        {
            Moves = Helpers.LoadAllMoves();
        }

        /// <summary>
        /// Detects the current Pokémon Essentials version in use.
        /// </summary>
        static public void GetVersion()
        {
            string line = File.ReadAllLines(Global.Directory + "/PBS/items.txt")[0];
            if(Regex.IsMatch(line, "^(\\d+),([A-Za-z0-9]+),"))
            {
                if(Regex.IsMatch(line, "^(\\d+),(.*),(.*),(.*),(\\d+),(\\d+),(.*),(\\d+),(\\d+),(\\d+),(.*)?"))
                {
                    Global.v16Detected = true;
                }
                else
                {
                    Global.v16Detected = false;
                }
            }
        }
    }
}
