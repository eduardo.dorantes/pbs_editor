﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text.RegularExpressions;
using Utilities.Structures;

namespace Utilities
{
    static public class Helpers
    {

        /// <summary>
        /// Gets the v16 Egg Groups equivalent for the v15 index.
        /// </summary>
        /// <param name="og"></param>
        /// <returns></returns>
        static public string EggGroupUpgrade(int og)
        {
            string[] Table = new string[] { "Undiscovered","Monster","Water1","Bug","Flying","Field","Fairy","Grass","Humanlike","Water3","Mineral","Amorphous","Water2","Ditto","Dragon","Undiscovered"};
            return Table[og];
        }

        /// <summary>
        /// Checks if all the needed files exist.
        /// </summary>
        /// <returns></returns>
        static public bool CheckAllFiles()
        {
            bool result = true;
            string[] Files = new String[] {"abilities", "items", "moves", "pokemon", "trainers", "trainertypes", "types"};
            foreach(string file in Files)
            {
                if(!File.Exists(Global.Directory + "/PBS/" + file + ".txt"))
                {
                    result = false;
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// Gets the current game's name.
        /// </summary>
        /// <returns></returns>
        static public string GetGameName()
        {
            try
            {
                Match match;
                string name = "";
                IEnumerable<string> lines = File.ReadLines(Global.Directory + "/" + "Game.ini");
                foreach (string line in lines){
                    if (line.Contains("Title=")){
                        match = Regex.Match(line, "^Title=(.*)");
                        name = match.Groups[1].ToString();
                        break;
                    }
                }
                return name;
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// Returns a List containing all defined pokémon's name.
        /// </summary>
        /// <returns></returns>
        static public List<string> GetAllPokemonNames()
        {
            List<string> names = new List<string>();
            foreach(Pokemon pkmn in Global.Pokemon)
            {
                names.Add(pkmn.Name);
            }
            return names;
        }

        /// <summary>
        /// Returns a List containing all defined pokémon's constant (internal) name.
        /// </summary>
        /// <returns></returns>
        static public List<string> GetAllPokemonConstantNames()
        {
            List<string> names = new List<string>();
            foreach (Pokemon pkmn in Global.Pokemon)
            {
                if(pkmn.InternalName != null)
                {
                    names.Add(pkmn.InternalName);
                }
            }
            return names;
        }

        /// <summary>
        /// Returns a List containing all defined moves's internal name.
        /// </summary>
        /// <returns></returns>
        static public List<string> GetAllMoveNames()
        {
            IEnumerable<string> data = File.ReadAllLines(Global.Directory + "/PBS/moves.txt");
            List<string> Moves = new List<string>();
            foreach(string line in data)
            {
                if(Regex.IsMatch(line, "^\\d+\\,([A-Z0-9]+)"))
                {
                    Moves.Add(line.Split(',')[1]);
                }
            }
            return Moves;
        }

        /// <summary>
        /// Returns a list containing all defined moves's internal name.
        /// </summary>
        /// <returns></returns>
        static public List<string> GetAllItemConstantNames()
        {
            List<string> Items = new List<string>();
            foreach(Item i in Global.Items)
            {
                if(i.InternalName != null)
                {
                    Items.Add(i.InternalName);
                }
            }
            return Items;
        }

        /// <summary>
        /// Returns a list containing all defined abilities's internal name.
        /// </summary>
        /// <returns></returns>
        static public List<string> GetAllAbilityConstantNames()
        {
            List<string> List = new List<string>();
            foreach (Ability ab in Global.Abilities)
            {
                if(ab.InternalName != null)
                {
                    List.Add(ab.InternalName);
                }
            }
            return List;
        }

        /// <summary>
        /// Returns a list containing all defined abilities's object representation.
        /// </summary>
        /// <returns></returns>
        static public BindingList<Ability> LoadAllAbilities()
        {
            BindingList<Ability> Abilities = new BindingList<Ability>();
            IEnumerable<string> lines = File.ReadAllLines(Global.Directory + "/PBS/abilities.txt");
            foreach(string line in lines)
            {
                Match match = Regex.Match(line, "^(\\d+),(.*),(.*),(\\\".*\\\")");
                if (match.Success)
                {
                    Abilities.Add(new Ability(match.Groups[2].Value, match.Groups[3].Value, match.Groups[4].Value.Trim('"')));
                }
            }
            return Abilities;
        }

        /// <summary>
        /// Returns a list containing all defined moves's object representation.
        /// </summary>
        /// <returns></returns>
        static public BindingList<Move> LoadAllMoves()
        {
            BindingList<Move> Moves = new BindingList<Move>();
            List<string> Error = new List<string>();
            IEnumerable<string> lines = File.ReadAllLines(Global.Directory + "/PBS/moves.txt");
            foreach(string line in lines)
            {
                Match match = Regex.Match(line, "(\\d+),(.*),(.*),([0-9a-fA-F]+),(\\d+),(.*),(.*),(\\d+),(\\d+),(\\d+),(\\d+),([\\-]?\\d+),([a-n]+)?,(\\\".*\\\"),?");
                if (match.Success)
                {
                    Move move = new Structures.Move(true);
                    move.InternalName = match.Groups[2].Value;
                    move.Name = match.Groups[3].Value;
                    move.FunctionCode = match.Groups[4].Value;
                    move.BasePower = Convert.ToInt32(match.Groups[5].Value);
                    move.Type = match.Groups[6].Value;
                    Damage damage;
                    Enum.TryParse(match.Groups[7].Value, out damage);
                    move.Damage = damage;
                    move.Accuracy = Convert.ToInt32(match.Groups[8].Value);
                    move.TotalPP = Convert.ToInt32(match.Groups[9].Value);
                    move.AdditionalEffectChance = Convert.ToInt32(match.Groups[10].Value);
                    move.Target = Convert.ToInt32(match.Groups[11].Value);
                    move.Priority = Convert.ToInt32(match.Groups[12].Value);
                    string allowedFlags = "abcdefghijklmn";
                    List<char> flags = new List<char>();
                    foreach(char c in match.Groups[13].Value)
                    {
                        if (allowedFlags.Contains(c.ToString()))
                        {
                            flags.Add(c);
                        }
                    }
                    move.Flags = flags.ToArray();
                    move.Description = match.Groups[14].Value.Trim('"'); ;
                    Moves.Add(move);
                }
                else
                {
                    Error.Add(line);
                }
            }
            return Moves;
        }

        /// <summary>
        /// Returns a List containing all defined pokémon's object representation.
        /// </summary>
        /// <param name="convert"></param>
        /// <returns></returns>
        static public BindingList<Pokemon> LoadAllPokemon(bool convert = false)
        {
            string lines = "|";
            string[] tmp;
            BindingList<Pokemon> Pokemon = new BindingList<Pokemon>();
            Pokemon pokemon;
            string[] data;

            lines += String.Join("|", File.ReadAllLines(Global.Directory + "/PBS/pokemon.txt"));
            tmp = Regex.Split(lines, "\\|\\[\\d+\\]\\|");
            for(int index = 1; index < tmp.Length; index++)
            {
                pokemon = new Pokemon(true);
                data = tmp[index].Split('|');
                foreach(string line in data)
                {
                    #region "values"
                    if(Regex.IsMatch(line, "^Name=(.*)"))
                    {
                        string name = line.Split('=')[1].Trim(' ');
                        if (!String.IsNullOrWhiteSpace(name))
                        {
                            pokemon.Name = name;
                        }
                    }
                    else if (Regex.IsMatch(line, "^InternalName=(.*)"))
                    {
                        string name = line.Split('=')[1].Trim(' ');
                        if (!String.IsNullOrWhiteSpace(name))
                        {
                            pokemon.InternalName = name;
                        }
                    }
                    else if (Regex.IsMatch(line, "^Type1=(.*)"))
                    {
                        string type = line.Split('=')[1];
                        if (!String.IsNullOrWhiteSpace(type))
                        {
                            pokemon.Type1 = type;
                        }
                    }
                    else if (Regex.IsMatch(line, "^Type2=(.*)"))
                    {
                        pokemon.Type2 = line.Split('=')[1];
                    }
                    else if (Regex.IsMatch(line, "^BaseStats=(.*)"))
                    {
                        pokemon.BaseStats = new byte[6];
                        string[] stats = line.Split('=')[1].Split(',');
                        for(int i = 0; i < stats.Length; i++)
                        {
                            pokemon.BaseStats[i] = (byte)Convert.ToInt32(stats[i]);
                        }
                    }
                    else if (Regex.IsMatch(line, "^GrowthRate=(.*)"))
                    {
                        GrowthRate temp;
                        Enum.TryParse(line.Split('=')[1].Trim(' '), true, out temp);
                        pokemon.GrowthRate = temp;
                    }
                    else if (Regex.IsMatch(line, "^GenderRate=(.*)"))
                    {
                        GenderRate temp;
                        Enum.TryParse(line.Split('=')[1].Trim(' '), true, out temp);
                        pokemon.GenderRate = temp;
                    }
                    else if (Regex.IsMatch(line, "^EffortPoints=(.*)"))
                    {
                        pokemon.EffortPoints = new byte[6];
                        string[] points = line.Split('=')[1].Split(',');
                        for (int i = 0; i < points.Length; i++)
                        {
                            pokemon.EffortPoints[i] = (byte)Convert.ToInt32(points[i]);
                        }
                    }
                    else if (Regex.IsMatch(line, "^Rareness=(.*)"))
                    {
                        pokemon.Rareness = (byte)Convert.ToInt32(line.Split('=')[1]);
                    }
                    else if (Regex.IsMatch(line, "^Happiness=(.*)"))
                    {
                        pokemon.Happiness = (byte)Convert.ToInt32(line.Split('=')[1]);
                    }
                    else if (Regex.IsMatch(line, "^Moves=(.*)"))
                    {
                        List<MoveField> Moves = new List<MoveField>();
                        string[] moves = line.Split('=')[1].Split(',');
                        MoveField move;
                        for(int i = 0; i < moves.Length; i += 2)
                        {
                            if (String.IsNullOrWhiteSpace(moves[i]))
                            {
                                continue;
                            }
                            move = new MoveField((byte)Convert.ToInt32(moves[i]), moves[i + 1]);
                            Moves.Add(move);
                        }
                        if(Moves.Count > 0)
                        {
                            pokemon.Moves.Clear();
                            pokemon.Moves.AddRange(Moves);
                        }
                    }
                    else if (Regex.IsMatch(line, "^Compatibility=(.*)"))
                    {
                        if (Global.v16Detected)
                        {
                            pokemon.Compatibilityv16 = new List<EggGroupsv16>();
                            string[] groups = line.Split('=')[1].Split(',');
                            foreach (string group in groups)
                            {
                                EggGroupsv16 t;
                                Enum.TryParse(group, true, out t);
                                pokemon.Compatibilityv16.Add(t);
                            }
                        }
                        else
                        {
                            pokemon.Compatibilityv15 = new List<EggGroupsv15>();
                            pokemon.Compatibilityv16 = new List<EggGroupsv16>();

                            string[] groups = line.Split('=')[1].Split(',');
                            foreach (string group in groups)
                            {
                                if (convert)
                                {
                                    string g = EggGroupUpgrade(Convert.ToInt32(group));
                                    EggGroupsv16 t;
                                    Enum.TryParse(g, true, out t);
                                    pokemon.Compatibilityv16.Add(t);
                                }
                                else
                                {
                                    EggGroupsv15 t;
                                    Enum.TryParse(group, true, out t);
                                    pokemon.Compatibilityv15.Add(t);
                                }
                            }
                        }
                    }
                    else if (Regex.IsMatch(line, "^StepsToHatch=(.*)"))
                    {
                        pokemon.StepsToHatch = (ushort)Convert.ToInt32(line.Split('=')[1]);
                    }
                    else if (Regex.IsMatch(line, "^BaseEXP=(.*)"))
                    {
                        pokemon.BaseExp = (ushort)Convert.ToInt32(line.Split('=')[1]);
                    }
                    else if (Regex.IsMatch(line, "^Height=(.*)"))
                    {
                        pokemon.Height = Convert.ToDecimal(line.Split('=')[1].Replace(",","."));
                        if(pokemon.Height == 0)
                        {
                            pokemon.Height = 0.1m;
                        }
                    }
                    else if (Regex.IsMatch(line, "^Weight=(.*)"))
                    {
                        pokemon.Weight = Convert.ToDecimal(line.Split('=')[1].Replace(",", "."));
                        if(pokemon.Weight == 0)
                        {
                            pokemon.Weight = 0.1m;
                        }
                    }
                    else if (Regex.IsMatch(line, "^Color=(.*)"))
                    {
                        Color temp;
                        Enum.TryParse(line.Split('=')[1], true, out temp);
                        pokemon.Color = temp;
                    }
                    else if (Regex.IsMatch(line, "^Kind=(.*)"))
                    {
                        string kind = line.Split('=')[1].Trim(' ');
                        if (!String.IsNullOrWhiteSpace(kind))
                        {
                            pokemon.Kind = kind;
                        }
                    }
                    else if (Regex.IsMatch(line, "^Pokedex=(.*)"))
                    {
                        string pokedex = line.Split('=')[1].Trim('"');
                        if (!String.IsNullOrWhiteSpace(pokedex))
                        {
                            pokemon.Pokedex = pokedex;
                        }
                    }
                    else if (Regex.IsMatch(line, "^Evolutions=(.*)"))
                    {
                        pokemon.Evolutions = new List<Evolutions>();
                        if (String.IsNullOrWhiteSpace(line.Split('=')[1]))
                        {
                            continue;
                        }
                        string[] evols = line.Split('=')[1].Split(',');
                        Evolutions evo;

                        for(int i = 0; i < evols.Length; i += 3)
                        {
                            if (String.IsNullOrWhiteSpace(evols[i]))
                            {
                                continue;
                            }
                            EvolutionMethods evo_meth;
                            Enum.TryParse(evols[i + 1], true, out evo_meth);
                            string m = "";
                            if(evols.Length >= i + 3)
                            {
                                m = evols[i + 2];
                            }
                            evo = new Evolutions(evols[i], evo_meth, m);
                            pokemon.Evolutions.Add(evo);
                        }
                    }
                    else if (Regex.IsMatch(line, "^Abilities=(.*)"))
                    {
                        pokemon.Abilities = line.Split('=')[1].Trim(' ').Split(',');
                    }
                    else if (Regex.IsMatch(line, "^HiddenAbility=(.*)"))
                    {
                        pokemon.HiddenAbility = line.Split('=')[1].Trim(' ');
                    }
                    else if (Regex.IsMatch(line, "^EggMoves=(.*)"))
                    {
                        List<string> egg_moves = new List<string>();
                        egg_moves.AddRange(line.Split('=')[1].Split(','));
                        pokemon.EggMoves = egg_moves;
                    }
                    else if (Regex.IsMatch(line, "^Habitat=(.*)"))
                    {
                        Habitat temp;
                        Enum.TryParse(line.Split('=')[1], true, out temp);
                        pokemon.Habitat = temp;
                    }
                    else if (Regex.IsMatch(line, "^RegionalNumbers=(.*)"))
                    {
                        pokemon.RegionalNumbers = new List<ushort>();
                        foreach(string num in line.Split('=')[1].Split(','))
                        {
                            if(num == null || num == "")
                            {
                                continue;
                            }
                            pokemon.RegionalNumbers.Add((ushort)Convert.ToInt32(num));
                        }
                    }
                    else if (Regex.IsMatch(line, "^WildItemCommon=(.*)"))
                    {
                        pokemon.WildItemCommon = line.Split('=')[1];
                    }
                    else if (Regex.IsMatch(line, "^WildItemUncommon=(.*)"))
                    {
                        pokemon.WildItemUncommon = line.Split('=')[1];
                    }
                    else if (Regex.IsMatch(line, "^WildItemRare=(.*)"))
                    {
                        pokemon.WildItemRare = line.Split('=')[1];
                    }
                    else if (Regex.IsMatch(line, "^BattlerEnemyY=(.*)"))
                    {
                        pokemon.BattlerEnemyY = (byte)Convert.ToInt32(line.Split('=')[1]);
                    }
                    else if (Regex.IsMatch(line, "^BattlerPlayerY=(.*)"))
                    {
                        pokemon.BattlerPlayerY = (byte)Convert.ToInt32(line.Split('=')[1]);
                    }
                    else if (Regex.IsMatch(line, "^BattlerAltitude=(.*)"))
                    {
                        pokemon.BattlerAltitude = (byte)Convert.ToInt32(line.Split('=')[1]);
                    }
                    else if (Regex.IsMatch(line, "^FormNames=(.*)"))
                    {
                        pokemon.FormNames = line.Split('=')[1].Split(',');
                    }
                    else if (Regex.IsMatch(line, "^Incense=(.*)"))
                    {
                        pokemon.Incense = line.Split('=')[1];
                    }
                    #endregion
                }
                Pokemon.Add(pokemon);
            }
            return Pokemon;
        }

        /// <summary>
        /// Returns a List containing all defined items's object representation.
        /// </summary>
        /// <param name="convert"></param>
        /// <returns></returns>
        static public BindingList<Item> LoadAllItems(bool convert = false)
        {
            IEnumerable<string> lines = File.ReadAllLines(Global.Directory + "/PBS/items.txt");
            BindingList<Item> Items = new BindingList<Item>();
            string v16 = "^(\\d+),(.*),(.*),(.*),(\\d+),(\\d+),(\\\".*\\\"),(\\d+),(\\d+),(\\d+),?(.*)?";
            string v15 = "^(\\d+),(.*),(.*),(\\d+),(\\d+),(\\\".*\\\"),(\\d+),(\\d+),(\\d+),?(.*)?";
            string pattern = Global.v16Detected ? v16 : v15;
            foreach (string line in lines)
            {
                if(Regex.IsMatch(line, pattern))
                {
                    Match match = Regex.Match(line, pattern);
                    Item item = new Item();
                    item.InternalName = match.Groups[2].Value;
                    item.Name = match.Groups[3].Value;
                    if (Global.v16Detected)
                    {
                        item.PluralName = match.Groups[4].Value;
                        item.Pocket = (byte)Convert.ToInt32(match.Groups[5].Value);
                        item.Price = Convert.ToInt32(match.Groups[6].Value);
                        item.Description = match.Groups[7].Value.Trim('"');
                        item.Description = Regex.Replace(item.Description, "\\\\'", "'");
                        item.Description = Regex.Replace(item.Description, "\\\\\"", "\"");
                        item.UsabilityOutOfBattle = (byte)Convert.ToInt32(match.Groups[8].Value);
                        item.UsabilityInBattle = (byte)Convert.ToInt32(match.Groups[9].Value);
                        item.SpecialItems = (byte)Convert.ToInt32(match.Groups[10].Value);

                        if (match.Groups[11].Value != null)
                        {
                            item.Move = match.Groups[11].Value;
                        }
                    }
                    else
                    {
                        item.Pocket = (byte)Convert.ToInt32(match.Groups[4].Value);
                        item.Price = Convert.ToInt32(match.Groups[5].Value);
                        item.Description = match.Groups[7].Value.Trim('"');
                        item.Description = Regex.Replace(item.Description, "\\\\'", "'");
                        item.Description = Regex.Replace(item.Description, "\\\\\"", "\"");
                        item.UsabilityOutOfBattle = (byte)Convert.ToInt32(match.Groups[7].Value);
                        item.UsabilityInBattle = (byte)Convert.ToInt32(match.Groups[8].Value);
                        item.SpecialItems = (byte)Convert.ToInt32(match.Groups[9].Value);

                        if (match.Groups[10].Value != null)
                        {
                            item.Move = match.Groups[10].Value;
                        }
                        if (convert)
                        {
                            item.PluralName = "";
                        }
                    }
                    Items.Add(item);
                }
            }
            return Items;
        }
    }
}
